import createDataContext from "./createDataContext";
import { signUp } from "../Api/signUp";
import { signIn } from "../Api/signIn";
import AxiosConf from "../Api/AxiosConfigue";
import AsyncStorage from "@react-native-community/async-storage";
import { navigate } from "../../navigationRef";
const authReducer = (state, action) => {
  switch (action.type) {
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "signin":
      return { errorMessage: "", token: action.payload };
    case "clear_error_message":
      return { ...state, errorMessage: "" };
    case "signout":
      return { token: null, errorMessage: "" };
    case "loading":
      return { ...state, loading: action.payload };
    case "edit_number":
      return { ...state, loginType: "Login" };
    case "token":
      return { ...state, token: action.payload };
    case "change_type":
      return { ...state, loginType: action.payload };
    default:
      return state;
  }
};
// const add = (a, b) => a + b;
const clearErrorMessage = (dispatch) => () => dispatch({ type: "clear_error_message" });

const tryLoacalSignIn = (dispatch) => async () => {
  const token = await AsyncStorage.getItem("token");
  if (token) {
    dispatch({ type: "signin", payload: token });
    navigate("TrackList");
  } else {
    navigate("Singup");
  }
};
//================================ SIGNUP ==============================
const signup = (dispatch) => async (phoneNumber, name) => {
  console.log(phoneNumber, name);

  dispatch({
    type: "add_error",
    payload: "",
  });
  dispatch({ type: "loading", payload: true });
  if (phoneNumber.length > 10) {
    const response = await signUp({ phoneNumber, name });
    console.log({ response });
    if (response === 2019) {
      dispatch({ type: "change_type", payload: "Code" }),
        dispatch({ type: "loading", payload: false });
    } else if (response === 1012) {
      dispatch({
        type: "add_error",
        payload: "شما قبلا ثبت نام کرده اید",
      });
      dispatch({ type: "loading", payload: false });
    } else if (response === 1099) {
      dispatch({
        type: "add_error",
        payload: "نام خود را وارد کنید",
      });
      dispatch({ type: "loading", payload: false });
    } else if (response === 1021) {
      dispatch({
        type: "add_error",
        payload: "دریافت مجدد پیامک بعد از ۲ دقیقه",
      });
      dispatch({ type: "loading", payload: false });
    } else {
      dispatch({
        type: "add_error",
        payload: "لطفا دوباره تلاش کنید",
      });
      dispatch({ type: "loading", payload: false });
    }
  } else {
    dispatch({
      type: "add_error",
      payload: "شماره  همراه ۱۱ رقمی خود را وارد کنید",
    });
    dispatch({ type: "loading", payload: false });
  }
};
//================================ SIGNIN ==============================

const signin = (dispatch) => async (phoneNumber) => {
  console.log({ phoneNumber });

  //do something
  dispatch({
    type: "add_error",
    payload: "",
  });
  dispatch({ type: "loading", payload: true });
  if (phoneNumber.length > 10) {
    const response = await signIn({ phoneNumber });
    console.log({ login: response });
    if (response === 2019) {
      console.log("alijoon");

      dispatch({ type: "change_type", payload: "Code" }),
        dispatch({ type: "loading", payload: false });
    } else if (response === 1015) {
      dispatch({
        type: "add_error",
        payload: "شما ثبت نام نکرده اید",
      });
      dispatch({
        type: "change_type",
        payload: "SignUp",
      });
      dispatch({ type: "loading", payload: false });
    } else {
      dispatch({
        type: "add_error",
        payload: "لطفا دوباره تلاش کنید",
      });
      dispatch({ type: "loading", payload: false });
    }
  } else {
    dispatch({
      type: "add_error",
      payload: "شماره  همراه ۱۱ رقمی خود را وارد کنید",
    });
    dispatch({ type: "loading", payload: false });
  }
};

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem("token");
  dispatch({ type: "signout" });
  dispatch({ type: "change_type", payload: "Login" });

  navigate("LoginFlow");
};
//================================ TOKEN ==============================

const enterToken = (dispatch) => async (token, phoneNumber, callBack) => {
  dispatch({ type: "add_error", payload: "" });
  console.log("eter");

  dispatch({ type: "loading", payload: true });
  if (token.length >= 6) {
    const response = await AxiosConf.put("/register", {
      verifyCode: token,
      phoneNumber: phoneNumber,
    })
      .then(async (res) => {
        console.log(res);

        await AsyncStorage.setItem("token", res.data.token),
          dispatch({ type: "token", payload: res.data.token });
        dispatch({ type: "loading", payload: false });
        if (callBack) callBack();
      })
      .catch((e) => {
        if (e.response.data.CODE === 1013) {
          dispatch({ type: "add_error", payload: "کد وارد شده اشتباه است" });
        }
        console.log({ e }), dispatch({ type: "loading", payload: false });
      });
    console.log(response);
  } else {
    dispatch({ type: "add_error", payload: "لطفا کد ۶ رقمی را کامل وارد کنید" });
    dispatch({ type: "loading", payload: false });
  }
};

const editNumber = (dispatch) => () => {
  dispatch({ type: "edit_number", payload: false });
};
const changeTypeToSignUp = (dispatch) => (type) => {
  dispatch({ type: "change_type", payload: type });
  dispatch({ type: "add_error", payload: "" });
};
export const { Provider, Context } = createDataContext(
  authReducer,
  {
    signin,
    signout,
    signup,
    clearErrorMessage,
    tryLoacalSignIn,
    enterToken,
    editNumber,
    changeTypeToSignUp,
  },
  { token: null, errorMessage: "", showCode: false, loginType: "Login" }
);
