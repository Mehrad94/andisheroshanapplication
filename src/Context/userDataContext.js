import createDtateContext from "./createDataContext";
import Axios from "../Api/AxiosConfigue";
const UserDataContext = (state = inisialState, action) => {
  switch (action.type) {
    case "user_data":
      return { ...state, userDataState: action.payload };
    case "add_eroor":
      return { ...state, error: "مشکل در دریافت اطلاعات" };
    case "delete_error":
      return { ...state, error: null };
    default:
      return state;
  }
};
const inisialState = {
  userDataState: null,
  error: null
};
const getFreshUserData = (dispatch) => async () => {
  console.log("fresh");

  dispatch({ type: "delete_error" });
  const userData = await Axios.get("/splashscreen")
    .then((res) => {
      console.log({ kl: res.data.userInformation });

      return res.data.userInformation;
    })
    .catch((e) => {
      console.log({ e });

      dispatch({ type: "add_eroor" });

      /// code 1098 user token is expired
      // console.log({ e.response.data.CODE });
    });
  dispatch({ type: "user_data", payload: userData });
};
export const { Provider, Context } = createDtateContext(
  UserDataContext,
  { getFreshUserData },
  inisialState
);
