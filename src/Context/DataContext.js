import createDtateContext from "./createDataContext";
import { getAllCourses } from "../Api/getCourses";
import { getBlogs } from "../Api/getBlogs";
import { likeApi } from "../Api/likeApi";
import { toggleBookMark } from "../Api/toggleBookMark";
import { getAudioBook } from "../Api/getAudioBooks";
import { getAudioBookById } from "../Api/getAudioBookById";
import { getSeminar } from "../Api/getSeminars";
import { getSeminarById } from "../Api/getSeminarById";
import { getIntroDuctionJob } from "../Api/getIntroducingJobs";
import { getIntroducingJobById } from "../Api/getIntroducingJobById";
const ALL_COURSE = "all_courses";
const DataContext = (state = inisialState, action) => {
  switch (action.type) {
    case ALL_COURSE:
      return { ...state, allCorses: action.payload };
    case "splash_data":
      return { ...state, splashData: action.payload };
    case "push_cat":
      return { ...state, ...state.splashData.categories.push({ ـid: 123, title: "همه" }) };
    case "all_blog":
      return { ...state, allBlogs: action.payload };
    case "like_submitted":
      if (state.likeState.includes(action.payload)) {
        let filtered = state.likeState.filter((item) => {
          return item !== action.payload;
        });
        return { ...state, likeState: filtered };
      } else {
        return { ...state, ...state.likeState.push(action.payload) };
      }
    case "add_error":
      return { ...state, errrState: action.payload };
    case "spared_Data":
      return { ...state, likeState: state.splashData.userInformation.likedBlog };
    case "filter_cat":
      if (action.payload === 123) {
        return { ...state, filterCat: null };
      }
      return { ...state, filterCat: action.payload };
    case "music_player":
      return { ...state, musicPlayer: action.payload };

    case "change_current_music":
      return { ...state, currentMusic: action.payload };
    case "download_url":
      return { ...state, voiceUrl: action.payload };
    case "clear_url":
      if (action.payload === "voice") {
        return { ...state, voiceUrl: null };
      } else {
        return { ...state, videoUrl: null };
      }
    case "video_download":
      return { ...state, videoUrl: action.payload };
    case "app_version":
      return { ...state, appVersion: action.payload };
    case "book_submitted":
      if (state.bookState.includes(action.payload)) {
        let BookFilter = state.bookState.filter((item) => {
          return item !== action.payload;
        });
        return { ...state, bookState: BookFilter };
      } else {
        return { ...state, ...state.bookState.push(action.payload) };
      }
    case "book_array":
      return { ...state, bookState: state.splashData.userInformation.blogBookmark };
    case "current_music_null":
      return { ...state, currentMusic: null };
    case "audio_book":
      return { ...state, audioBook: action.payload };
    case "audio_book_byid":
      return { ...state, audioSection: action.payload };
    case "seminar":
      return { ...state, seminar: action.payload };
    case "Seminar_byid":
      return { ...state, seminarSection: action.payload };
    case "introducing_job":
      return { ...state, introducingjob: action.payload };
    case "introducing_job_byid":
      return { ...state, introducingSection: action.payload };
    case "connection":
      return { ...state, isConnected: action.payload };
    default:
      return state;
  }
};
const inisialState = {
  allCorses: null,
  splashData: null,
  allBlogs: null,
  blogs: null,
  likeState: [],
  filterCat: null,
  globalPage: 1,
  musicPlayer: false,
  currentMusic: "",
  voiceUrl: null,
  videoUrl: null,
  appVersion: null,
  bookState: [],
  audioBook: [],
  audioSection: null,
  seminar: [],
  seminarSection: null,
  introducingjob: [],
  introducingSection: null,
  isConnected: null,
};
const addDataToState = (dispatch) => (data) => {
  dispatch({ type: "add_data", payload: data });
};
const pushNewAddress = (dispatch) => (data) => {
  dispatch({ type: "push-address", payload: data });
};
const fetchAllCourses = (dispatch) => async (courseId) => {
  const ress = await getAllCourses();
  dispatch({ type: ALL_COURSE, payload: ress });
};
const fetchSplashData = (dispatch) => (res) => {
  console.log({ res });

  dispatch({ type: "splash_data", payload: res });
  dispatch({ type: "spared_Data" });
  dispatch({ type: "push_cat" });
  dispatch({ type: "book_array" });
};
const fetchAllBlogs = (dispatch) => async (page) => {
  const resBlog = await getBlogs(page);
  dispatch({ type: "all_blog", payload: resBlog });
};
const fetchMoreBlogs = (dispatch) => async (page) => {
  const resBlog = await getBlogs(page);
  dispatch({ type: "load_more", payload: resBlog.docs });
};
const likeMechanism = (dispatch) => async (likeId) => {
  const res = await likeApi(likeId);
  if (res.CODE === 2014) {
    dispatch({ type: "like_submitted", payload: likeId });
  } else {
    dispatch({ type: "add_error", payload: "مشکل در ارتباط با سرور" });
  }
};
const musicPlayerAction = (dispatch) => (bool) => {
  dispatch({ type: "music_player", payload: bool });
};
const filterCategoryAction = (dispatch) => (catId) => {
  dispatch({ type: "filter_cat", payload: catId });
};
const changeCurrentMusic = (dispatch) => (music, type) => {
  if (type === "download") {
    dispatch({ type: "download_url", payload: music });
  } else {
    dispatch({ type: "change_current_music", payload: music });
  }
};
const downloadEnd = (dispatch) => (type) => {
  dispatch({ type: "clear_url", payload: type });
};
const onDownloadVideoAction = (dispatch) => (url) => {
  dispatch({ type: "video_download", payload: url });
};
const setAppVersion = (dispatch) => (version) => {
  dispatch({ type: "app_version", payload: version });
};
const toggleBookmarkAction = (dispatch) => async (bookId) => {
  const bookRes = await toggleBookMark(bookId);
  console.log({ bookRes });
  if (bookRes.MSG === 2016) {
    dispatch({ type: "book_submitted", payload: bookId });
  } else {
    dispatch({ type: "add_error", payload: "مشکل در ارتباط با سرور" });
  }
};
const setCurrentMusicNull = (dispatch) => () => {
  dispatch({ type: "current_music_null" });
};
const fetchIntroducingJobs = (dispatch) => async () => {
  console.log("aliiiidfgdfknkjdfnk");

  const resIntroducingJob = await getIntroDuctionJob();
  console.log({ resIntroducingJob });
  if (typeof resIntroducingJob === "object") {
    dispatch({ type: "introducing_job", payload: resIntroducingJob });
  }
};
const getIntroducingJobItem = (dispatch) => async (id, callBack) => {
  const resIntroducingJobById = await getIntroducingJobById(id);
  console.log({ resIntroducingJobById });
  if (typeof resIntroducingJobById === "object") {
    dispatch({ type: "introducing_job_byid", payload: resIntroducingJobById });
    if (callBack) {
      callBack();
    }
  }
};
const fetchSeminars = (dispatch) => async () => {
  const resSeminar = await getSeminar();
  console.log({ resSeminar });
  if (typeof resSeminar === "object") {
    dispatch({ type: "seminar", payload: resSeminar });
  }
};
const getSeminarItem = (dispatch) => async (id, callBack) => {
  const resSeminarById = await getSeminarById(id);
  console.log({ resSeminarById });
  if (typeof resSeminarById === "object") {
    dispatch({ type: "Seminar_byid", payload: resSeminarById });
    if (callBack) {
      callBack();
    }
  }
};
const fetchAudioBooks = (dispatch) => async () => {
  const resAudioBook = await getAudioBook();
  console.log({ resAudioBook });
  if (typeof resAudioBook === "object") {
    dispatch({ type: "audio_book", payload: resAudioBook });
  }
};
const getAudioBookItem = (dispatch) => async (id, callBack) => {
  const resAudioBookById = await getAudioBookById(id);
  console.log({ resAudioBookById });
  if (typeof resAudioBookById === "object") {
    dispatch({ type: "audio_book_byid", payload: resAudioBookById });
    if (callBack) {
      callBack();
    }
  }
};
const checkConection = (dispatch) => (connected) => {
  dispatch({ type: "connection", payload: connected });
};
const onSliderPresed = (dispatch) => (data, callBack) => {
  const { type } = data;
  if (type === "AUDIO_BOOK") {
    dispatch({
      type: "audio_book_byid",
      payload: { audioBooks: data.audioBook, isPaid: data.isPaid },
    });

    callBack();
  } else if (type === "SEMINAR") {
    dispatch({ type: "Seminar_byid", payload: { seminars: data.seminar, isPaid: data.isPaid } });
    callBack();
  }
};
export const { Provider, Context } = createDtateContext(
  DataContext,
  {
    addDataToState,
    pushNewAddress,
    fetchAllCourses,
    fetchSplashData,
    fetchAllBlogs,
    fetchMoreBlogs,
    likeMechanism,
    filterCategoryAction,
    musicPlayerAction,
    changeCurrentMusic,
    downloadEnd,
    onDownloadVideoAction,
    setAppVersion,
    toggleBookmarkAction,
    setCurrentMusicNull,
    fetchAudioBooks,
    getAudioBookItem,
    fetchSeminars,
    getSeminarItem,
    fetchIntroducingJobs,
    getIntroducingJobItem,
    checkConection,
    onSliderPresed,
  },
  inisialState
);
