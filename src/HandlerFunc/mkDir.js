import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";

export const mkDir = async (type) => {
  return check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
    .then((result) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          break;
        case RESULTS.DENIED:
          request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
            if (result === "granted") {
              return true;
            }
          });
          break;
        case RESULTS.GRANTED:
          return true;
        case RESULTS.BLOCKED:
          return false;
      }
    })
    .catch((error) => {
      // …
      return false;
    });
};
