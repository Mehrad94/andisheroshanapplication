import axios from "./AxiosConfigue";

export const getSeminar = async () => {
  return axios
    .get("/seminar")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
