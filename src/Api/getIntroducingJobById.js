import axios from "./AxiosConfigue";

export const getIntroDuctionById = async (id) => {
  return axios
    .get(`/jobs/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
