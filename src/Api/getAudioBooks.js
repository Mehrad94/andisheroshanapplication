import axios from "./AxiosConfigue";

export const getAudioBook = async () => {
  return axios
    .get("/audioBook")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
