import axios from "./AxiosConfigue";

export const getAudioBookById = async (id) => {
  return axios
    .get(`/audioBook/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
