import AxiosComfig from "./AxiosConfigue";
export const unLikeApi = async (likeId) => {
  return AxiosComfig.post(`blog/unlike/${likeId}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
