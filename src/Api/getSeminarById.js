import axios from "./AxiosConfigue";

export const getSeminarById = async (id) => {
  return axios
    .get(`/seminar/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
