import axios from "./AxiosConfigue";
export const onPayment = async (type, id) => {
  return axios
    .get(`/payment/${type}/${id}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
