import AxiosComfig from "./AxiosConfigue";
export const getAllCourses = async (courseId) => {
  return AxiosComfig.get("/course", { courseId })
    .then((res) => res.data)
    .catch((e) => e);
};
