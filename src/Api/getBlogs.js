import AxiosComfig from "./AxiosConfigue";
export const getBlogs = async (page = 1) => {
  return AxiosComfig.get(`/blog/${page}`)
    .then((res) => res.data)
    .catch((e) => e);
};
