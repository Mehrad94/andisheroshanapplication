import AxiosComfig from "./AxiosConfigue";
export const signIn = async ({ phoneNumber, name }) => {
  return AxiosComfig.post("/login", { phoneNumber })
    .then((res) => res.data.CODE)
    .catch((e) => e.response.data.CODE);
};
