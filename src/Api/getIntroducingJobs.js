import axios from "./AxiosConfigue";

export const getIntroDuctionJob = async () => {
  return axios
    .get("/jobs")
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
