import AxiosComfig from "./AxiosConfigue";
export const signUp = async ({ phoneNumber, name }) => {
  return AxiosComfig.post("/register", { phoneNumber, fullName: name })
    .then((res) => res.data.CODE)
    .catch((e) => e.response.data.CODE);
};
