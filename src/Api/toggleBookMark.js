import AxiosComfig from "./AxiosConfigue";
export const toggleBookMark = async (bookId) => {
  return AxiosComfig.post(`bookmark/${bookId}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
