import axios from "./AxiosConfigue";
export const postFinishSec = async (type, courseId, sectionId) => {
  return axios
    .post("/user", { type, courseId, sectionId })
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
