import AxiosComfig from "./AxiosConfigue";
export const likeApi = async (likeId) => {
  return AxiosComfig.post(`blog/like/${likeId}`)
    .then((res) => {
      return res.data;
    })
    .catch((e) => {
      return e;
    });
};
