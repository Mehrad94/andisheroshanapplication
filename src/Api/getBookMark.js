import AxiosComfig from "./AxiosConfigue";
export const getBookmark = async (bookId) => {
  return AxiosComfig.get(`/blog/b/${bookId}`)
    .then((res) => res.data)
    .catch((e) => e);
};
