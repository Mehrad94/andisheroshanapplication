import React, { useState,useEffect} from 'react';
import { StatusBar, StyleSheet, Text, View ,Dimensions} from 'react-native';
import Video from 'react-native-video';
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import Orientation from 'react-native-orientation-locker';

const VideoPlayer = () => {

  let videoPlayer = null
  const [currentTime,setCurrentTime] = useState(0)
  const [duration,setDuration] = useState(0)
  const [isFullScreen,setIsFullScreen] = useState(false)
  const [isLoading,setIsLoading] = useState(true)
  const [paused,setPaused] = useState(false)
  const [playerState,setPlayerState] = useState(PLAYER_STATES.PLAYING)
  const [screenType,setScreenType] = useState('content')

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);

    return () => {
      Orientation.removeOrientationListener(handleOrientation);
    };
  }, []);
 
  const onSeek = seek => {
    //Handler for change in seekbar
    videoPlayer.seek(seek);
  };
  const onPaused = playerState => {
    //Handler for Video Pause
   setPaused(!paused)
   setPlayerState(playerState)
  };
  const onReplay = () => {
    //Handler for Replay
    setPlayerState(PLAYER_STATES.PLAYING)
    videoPlayer.seek(0)
  };
  const onProgress = data => {
    // Video Player will continue progress even if the video already ended
    if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
      setCurrentTime(data.currentTime);
    }
  };

  const handleOrientation= (orientation) => {
    orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
      ? (setIsFullScreen(true), StatusBar.setHidden(true))
      : (setIsFullScreen(false),
        StatusBar.setHidden(false));
  }


  const onLoad = data => {
    setDuration(data.duration)
    setIsLoading(false)}

  const onLoadStart = data => {setIsLoading(true)}
  const onEnd = () => {setPlayerState(PLAYER_STATES.ENDED)}
  const onError = () => alert('Oh! ', error);
  const exitFullScreen = () => {
    alert('Exit full screen');
  };
  const enterFullScreen = () => {};
  const onFullScreen = () => {
    isFullScreen
    ? Orientation.unlockAllOrientations()
    : Orientation.lockToLandscapeLeft();
  };
  const renderToolbar = () => (
    <View>
      <Text> toolbar </Text>
    </View>
  );
  const onSeeking = currentTime => {setCurrentTime(currentTime)};

  return (
    <View style={styles.container}>
        <Video
          onEnd={onEnd}
          onLoad={onLoad}
          onLoadStart={onLoadStart}
          onProgress={onProgress}
          paused={paused}
          // controls={true}
          // fullscreen={true}
          
          ref={ref => (videoPlayer = ref)}
          playInBackground={true}
          resizeMode= 'contain'
          onFullScreen={isFullScreen}
          source={{ uri: 'https://www.w3schools.com/html/mov_bbb.mp4' }}
          style={isFullScreen ? styles.fullscreenVideo : styles.video}
          volume={10}
        />
        <MediaControls
          duration={duration}
          isLoading={isLoading}
          mainColor="#333"
          onFullScreen={onFullScreen}
          onPaused={onPaused}
          onReplay={onReplay}
          onSeek={onSeek}
          onSeeking={onSeeking}
          playerState={playerState}
          progress={currentTime}
          toolbar={renderToolbar()}
          style={{height:100}}
        />
      </View>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  toolbar: {
    marginTop: 30,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  video: {
    height: Dimensions.get('window').width * (9 / 16),
    width: Dimensions.get('window').width,
    backgroundColor: 'black',
  },
  fullscreenVideo: {
    height: Dimensions.get('window').width,
    width: Dimensions.get('window').height,
    backgroundColor: 'black',
  },
  
});


export default VideoPlayer ;