import { StyleSheet, Dimensions } from "react-native";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import { iranSans } from "../../common/Theme";
import { screenHeight } from "../../common/Constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e8e8e8"
  },
  video: {
    height: Dimensions.get("window").width * (9 / 14),
    width: Dimensions.get("window").width
  },
  fullscreenVideo: {
    height: Dimensions.get("window").width,
    width: Dimensions.get("window").height,
    backgroundColor: "#e8e8e8"
  },
  text: {
    marginTop: 10,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: "right",
    ...iranSans
  },
  fullscreenButton: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "flex-end",
    alignItems: "center",
    paddingRight: 10
  },
  controlOverlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#00000040",
    justifyContent: "space-between"
  },
  title: {
    textAlign: "right",
    ...iranSans,
    fontSize: 18,
    color: GOLDEN_COLOR,
    borderRadius: 5
  },
  scroll: {
 
    

  },
  likeHolder: {
    backgroundColor: MAIN_COLOR,
    paddingRight:15,
    height: screenHeight /13,
   
    
  },
  date: {
    color: GOLDEN_COLOR,
    textAlign: "right",
  ...iranSans,
  fontSize:12,
  },
  box:{
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor:MAIN_COLOR,
    height: screenHeight /13,
 
  },
  view:{
    marginLeft:5,
    justifyContent: 'center',
    alignSelf: 'center',
    width: Dimensions.get("window").width/3.5
    
  },
  comment:{
   alignSelf:'center',
  },
  textComment:{
    ...iranSans,
    color:GOLDEN_COLOR,
  },
  icon:{
    alignSelf:'center'
  }
});

export default styles;
