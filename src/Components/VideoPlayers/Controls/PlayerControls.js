import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";

import Icon from "react-native-vector-icons/FontAwesome5";
import { Indicator } from "../../Indicator";

// interface Props {
//   playing: boolean;
//   showPreviousAndNext: boolean;
//   showSkip: boolean;
//   previousDisabled?: boolean;
//   nextDisabled?: boolean;
//   onPlay: () => void;
//   onPause: () => void;
//   skipForwards?: () => void;
//   skipBackwards?: () => void;
//   onNext?: () => void;
//   onPrevious?: () => void;
// }

const PlayerControls = ({
  playing,
  showPreviousAndNext,
  showSkip,
  previousDisabled,
  nextDisabled,
  onPlay,
  onPause,
  skipForwards,
  skipBackwards,
  onNext,
  onPrevious
}) => {
  return (
    <View style={styles.wrapper}>
      {showPreviousAndNext && (
        <TouchableOpacity
          style={[styles.touchable, previousDisabled && styles.touchableDisabled]}
          onPress={onPrevious}
          disabled={previousDisabled}
        >
          <Icon name="edit" color="red" size={25} />
        </TouchableOpacity>
      )}

      {showSkip && (
        <TouchableOpacity style={styles.touchable} onPress={skipBackwards}>
          <Icon name="backward" color="red" size={25} />
        </TouchableOpacity>
      )}

      <TouchableOpacity style={styles.touchable} onPress={playing ? onPause : onPlay}>
        {playing ? (
          <Icon name="pause" color="red" size={25} />
        ) : (
          <Icon name="play" color="red" size={25} />
        )}
      </TouchableOpacity>

      {showSkip && (
        <TouchableOpacity style={styles.touchable} onPress={skipForwards}>
          <Icon name="forward" color="red" size={25} />
        </TouchableOpacity>
      )}

      {showPreviousAndNext && (
        <TouchableOpacity
          style={[styles.touchable, nextDisabled && styles.touchableDisabled]}
          onPress={onNext}
          disabled={nextDisabled}
        >
          <Icon name="home" size={25} />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    flex: 3
  },
  touchable: {
    padding: 5
  },
  touchableDisabled: {
    opacity: 0.3
  }
});

export default PlayerControls;
