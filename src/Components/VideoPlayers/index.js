import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  ScrollView,
  BackHandler,
  Alert,
  ToastAndroid,
} from "react-native";
import Video, { OnLoadData } from "react-native-video";
import Orientation from "react-native-orientation-locker";

import PlayerControls from "./Controls/PlayerControls";
import ProgressBar from "./Controls/ProgressBar";
import Icon from "react-native-vector-icons/AntDesign";
import Icon1 from "react-native-vector-icons/FontAwesome5";
import styles from "./styles";
import { Indicator } from "../Indicator";
import { Context as DataContext } from "../../Context/DataContext";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import { GOLDEN_COLOR } from "../../common/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import BlogMusicPlayer from "../BlogMusicPlayer";
import { IS_EXIST_TOAST } from "../../common/Strings";

const LIKE_TEXT = "پسندیدم";
const VideoPlayers = ({ navigation }) => {
  //==============================CONTEXT =========================
  const context = useContext(DataContext);
  const { state, likeMechanism, toggleBookmarkAction } = context;
  //==============================CONTEXT =========================

  const videoRef = React.createRef();

  const [fullScreen, setFullScreen] = useState(false);
  const [play, setPlay] = useState(false);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [showControls, setShowControls] = useState(true);
  const [parameter, setParameter] = useState(null);
  const [liked, setLiked] = useState(false);
  const [videoUrl, setVideoUrl] = useState(null);
  const [downloading, setDownloading] = useState(false);
  const [isBooked, setBooked] = useState(false);
  const [musicStop, setMusicStop] = useState(false);
  const [loading, setLoading] = useState(false);
  const [voiceUrl, setVoiceUrl] = useState(null);
  const [isDownloaded, setDownloaded] = useState(false);
  //=============================== CHECK PARAMS AND FOCUS IN SCREEEN ==================
  useEffect(() => {
    navigation.addListener("didBlur", (path) => {
      if (path.type === "didBlur") {
        setMusicStop(true);
      }
    });
    navigation.addListener("didFocus", (path) => {
      if (path.type === "didFocus") {
        setMusicStop(false);
      }
    });
    const params = navigation.state.params.blog;
    setParameter(params);
  }, [navigation]);
  //=============================== CHECK PARAMS AND FOCUS IN SCREEEN ==================
  console.log({ parameter });

  //=============================== BACK HANDLER  ==================
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackress", backButtonHandler);

    return () => {
      BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
    };
  }, [fullScreen]);
  const backButtonHandler = () => {
    if (fullScreen) {
      return true;
    } else {
      return false;
    }
  };

  // useEffect(() => {
  //   Orientation.addOrientationListener(handleOrientation);

  //   return () => {
  //     Orientation.removeOrientationListener(handleOrientation);
  //   };
  // }, []);
  //====================== CHECK EXIST IN FIRST ====================================
  const isExistFree = () => {
    if (parameter) {
      const folderName = parameter.title;
      let directory = "";
      if (parameter.type === "VIDEO") {
        directory = `/storage/emulated/0//AndisheRoshan/Free/Videos/${folderName}`;
      }
      if (parameter.type === "VOICE") {
        directory = `/storage/emulated/0//AndisheRoshan/Free/Voices/${folderName}`;
      }

      RNFS.exists(directory).then((exist) => {
        if (exist) {
          setDownloaded(true);
        } else {
          setDownloaded(false);
        }
      });
    }
  };
  //====================== CHECK EXIST IN FIRST ====================================

  // const handleOrientation = (orientation) => {
  //   orientation === "LANDSCAPE-LEFT" || orientation === "LANDSCAPE-RIGHT"
  //     ? (setFullScreen(true), StatusBar.setHidden(true))
  //     : (setFullScreen(false), StatusBar.setHidden(false));
  // };

  // const handleFullscreen = () => {
  //   fullScreen ? Orientation.unlockAllOrientations() : Orientation.lockToLandscapeLeft();
  // };
  //=============================== BACK HANDLER  ==================

  //=============================== LIKE AND BOOKMARK  HANDLER   ==================
  useEffect(() => {
    let likedd = false;
    if (parameter) {
      likedd = state.likeState.includes(parameter._id);
      setLiked(likedd);
    }

    setLoading(false);
  }, [state, parameter]);
  useEffect(() => {
    isExistFree();
  }, [parameter, videoUrl, voiceUrl]);
  useEffect(() => {
    let booked = false;
    if (parameter) {
      booked = state.bookState.includes(parameter._id);
      setBooked(booked);
    }
    setLoading(false);
  }, [state, parameter]);
  //=============================== LIKE AND BOOKMARK  HANDLER   ==================

  //=============================== VIDEO PLAYER ACTIONS   ==================
  const handlePlayPause = () => {
    if (play) {
      setPlay(false);
      setShowControls(true);
      return;
    }

    setPlay(true);
    setTimeout(() => setShowControls(false), 1000);
  };

  const skipBackward = () => {
    videoRef.current.seek(currentTime - 15);
    setCurrentTime(currentTime - 15);
  };

  const skipForward = () => {
    videoRef.current.seek(currentTime + 15);
    setCurrentTime(currentTime + 15);
  };

  const onSeek = (data) => {
    videoRef.current.seek(data.seekTime);
    setCurrentTime(data.seekTime);
  };

  const onLoadEnd = (data) => {
    setDuration(data.duration);
    setCurrentTime(data.currentTime);
  };

  const onProgress = (data) => {
    setCurrentTime(data.currentTime);
  };
  const onEnd = () => {
    setPlay(false);
    videoRef.current.seek(0);
  };

  const showControlsF = () => {
    setShowControls(!showControls);
  };
  //=============================== VIDEO PLAYER ACTIONS   ==================

  const onLikePress = () => {
    setLoading(true);

    likeMechanism(parameter._id);
  };

  //=============================== DOWNLOADIN ACTIONS   ==================
  const onDowloadPressed = (item) => {
    setDownloading(true);
    if (item.type === "VIDEO") {
      setVideoUrl(item.dataUrl);
    } else {
      setVoiceUrl(item.dataUrl);
    }
  };
  const makeVideoDirectory = () => {
    const videoDirectory = "/AndisheRoshan/Free/Videos";
    const videoPath = `/storage/emulated/0/${videoDirectory}`;
    const voiceDirectory = "/AndisheRoshan/Free/Voices";
    const voicePath = `/storage/emulated/0/${voiceDirectory}`;
    RNFS.exists(videoPath)
      .then((exist) => {
        if (exist) {
          return null;
        } else {
          RNFetchBlob.fs
            .mkdir(videoPath)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
    RNFS.exists(voicePath)
      .then((exist) => {
        if (exist) {
          return null;
        } else {
          RNFetchBlob.fs
            .mkdir(voicePath)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  useEffect(() => {
    mkDir();
  }, [videoUrl, voiceUrl]);
  const mkDir = (type) => {
    check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            break;
          case RESULTS.DENIED:
            request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
              if (result === "granted") {
                makeVideoDirectory();

                videoDownload();
                voiceDownload();
              }
            });
            break;
          case RESULTS.GRANTED:
            makeVideoDirectory();

            videoDownload();
            voiceDownload();

            break;
          case RESULTS.BLOCKED:
            break;
        }
      })
      .catch((error) => {
        // …
      });
  };
  const voiceDownload = () => {
    if (voiceUrl) {
      setLoading(true);
      const { config, fs } = RNFetchBlob;
      const url = voiceUrl.toString();
      const imageUrl = parameter.cover.toString();
      const voiceName = `${parameter.title}.mp3`;
      const folderName = parameter.title;
      const imageName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.length);
      let voiceDirectory = `/storage/emulated/0//AndisheRoshan/Free/Voices/${folderName}`;
      RNFS.exists(voiceDirectory)
        .then((exist) => {
          if (exist) {
            setLoading(false);
            setDownloaded(true);
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.LONG);
            setVoiceUrl(null);
            setDownloading(false);
          } else {
            let voiceDownloadOption = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: voiceDirectory + `/${voiceName}`,
                description: "Downloading",
              },
            };
            let imageDownloadOption = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: false,
                path: voiceDirectory + `/${imageName}`,
                description: "Downloading",
              },
            };

            config(voiceDownloadOption)
              .fetch("GET", url)
              .then((res) => {
                config(imageDownloadOption)
                  .fetch("GET", imageUrl)
                  .then((res) => {
                    const textObject = {
                      title: parameter.title,
                      desc: parameter.description,
                    };
                    AsyncStorage.setItem(`${voiceName}`, JSON.stringify(textObject))
                      .then((res) => {
                        setDownloading(false);
                      })
                      .catch((e) => console.log({ e }));
                  })
                  .catch((e) => {})
                  .catch((err) => {
                    console.log(err.message);
                  });
                setVoiceUrl(null);
              })
              .catch((r) => {
                Alert.alert("erroein  download");
                setVoiceUrl(null);
              });
          }
        })
        .catch((e) => {
          console.log({ e });
          setLoading(false);

          return Alert.alert("error in check in exist ");
        });
      setLoading(false);
    }
  };
  const videoDownload = async () => {
    if (videoUrl) {
      setLoading(true);
      const { config, fs } = RNFetchBlob;
      const url = videoUrl.toString();
      const imageUrl = parameter.cover.toString();
      const videoName = `${parameter.title}.mp4`;
      const folderName = parameter.title;
      const imageName = imageUrl.substring(imageUrl.lastIndexOf("/") + 1, imageUrl.length);
      let videoDirectory = `/storage/emulated/0//AndisheRoshan/Free/Videos/${folderName}`;

      RNFS.exists(videoDirectory)
        .then((exist) => {
          if (exist) {
            setLoading(false);

            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
            setVideoUrl(null);
            setDownloading(false);
          } else {
            let videoDownloadoption = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: videoDirectory + `/${videoName}`,
                description: "Downloading",
              },
            };
            let imageDownloadOption = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: false,
                path: videoDirectory + `/${imageName}`,
                description: "Downloading",
              },
            };

            config(videoDownloadoption)
              .fetch("GET", url)
              .then((res) => {
                config(imageDownloadOption)
                  .fetch("GET", imageUrl)
                  .then((res) => {
                    const textObject = {
                      title: parameter.title,
                      desc: parameter.description,
                    };
                    AsyncStorage.setItem(`${videoName}`, JSON.stringify(textObject))
                      .then((res) => {
                        setDownloading(false);
                        console.log({ res });
                      })
                      .catch((e) => console.log({ e }));
                  })
                  .catch((e) => {
                    console.log("errror in image");
                  })
                  .catch((err) => {
                    console.log(err.message);
                  });
                setVideoUrl(null);
              })
              .catch((r) => {
                Alert.alert("erroein  download");
                setVideoUrl(null);
              });
          }
        })
        .catch((e) => {
          console.log({ e });

          return Alert.alert("error in check in exist ");
        });
      setLoading(false);
    }
  };
  //=============================== DOWNLOADIN ACTIONS   ==================

  const toggleBookmarkPress = async (id) => {
    if (state.bookState.includes(id)) {
      await AsyncStorage.removeItem(id);
    }

    setLoading(true);
    toggleBookmarkAction(id);
  };
  if (!parameter) {
    return <Indicator />;
  }
  return (
    <View style={styles.container}>
      {parameter.type === "VOICE" ? (
        <View style={{ height: 200 }}>
          <BlogMusicPlayer parameter={parameter} musicStop={musicStop} />
        </View>
      ) : (
        <TouchableWithoutFeedback onPress={showControlsF}>
          <View>
            <Video
              poster={parameter.cover}
              posterResizeMode={"stretch"}
              ref={videoRef}
              source={{
                uri: parameter.dataUrl,
              }}
              style={fullScreen ? styles.fullscreenVideo : styles.video}
              controls={false}
              resizeMode={"contain"}
              onLoad={onLoadEnd}
              onProgress={onProgress}
              onEnd={onEnd}
              paused={!play}
            />
            {showControls && (
              <View style={styles.controlOverlay}>
                {/* <TouchableOpacity
                  onPress={handleFullscreen}
                  hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                  style={styles.fullscreenButton}
                >
                  {fullScreen ? (
                    <Icon name="shrink" size={25} color="red" />
                  ) : (
                    <Icon name="arrowsalt" color="red" size={25} />
                  )}
                </TouchableOpacity> */}
                <PlayerControls
                  onPlay={handlePlayPause}
                  onPause={handlePlayPause}
                  playing={play}
                  showPreviousAndNext={false}
                  showSkip={true}
                  skipBackwards={skipBackward}
                  skipForwards={skipForward}
                />
                <ProgressBar
                  currentTime={currentTime}
                  duration={duration > 0 ? duration : 0}
                  onSlideStart={handlePlayPause}
                  onSlideComplete={handlePlayPause}
                  onSlideCapture={onSeek}
                />
              </View>
            )}
          </View>
        </TouchableWithoutFeedback>
      )}

      <View style={styles.likeHolder}>
        <Text style={styles.title}>{parameter.title}</Text>
        <Text style={styles.date}>{parameter.createdTime}</Text>
      </View>
      <ScrollView style={styles.scroll}>
        <Text style={styles.text}>{parameter.aboutBlog}</Text>
      </ScrollView>
      <View style={styles.box}>
        <View style={styles.comment}>
          {downloading ? (
            <Indicator color="red" size={20} />
          ) : (
            <Icon
              name="download"
              size={25}
              color={isDownloaded ? "gray" : GOLDEN_COLOR}
              style={styles.icon}
              onPress={() => onDowloadPressed(parameter)}
            />
          )}
        </View>
        <View style={styles.comment}>
          <Icon1
            name="heart"
            color="red"
            size={25}
            solid={liked}
            onPress={onLikePress}
            style={styles.icon}
          />
        </View>
        <View style={styles.comment}>
          <Icon1
            name="bookmark"
            solid={isBooked}
            color={GOLDEN_COLOR}
            size={25}
            style={styles.icon}
            onPress={() => toggleBookmarkPress(parameter._id)}
          />
        </View>
      </View>
      {loading ? (
        <View
          style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(255,255,255,0.3)",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Indicator size={30} />
        </View>
      ) : null}
    </View>
  );
};

export default VideoPlayers;
