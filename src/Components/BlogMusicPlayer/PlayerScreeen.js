import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import TrackPlayer from "react-native-track-player";
import { Image, StyleSheet, Text, TouchableOpacity, View, ViewPropTypes } from "react-native";
import { iranSans, centerAll } from "../../common/Theme";
import Icon from "react-native-vector-icons/FontAwesome5";
import { MAIN_COLOR } from "../../common/Colors";
var moment = require("moment");
var momentDurationFormatSetup = require("moment-duration-format");

function ControlButton({ title, onPress }) {
  return (
    <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
      <Text style={styles.controlButtonText}>{title}</Text>
    </TouchableOpacity>
  );
}

ControlButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired
};

const Player = (props) => {
  const [visiable, setVisiable] = useState(false);
  useEffect(() => {
    mori();
  });

  async function mori() {
    const mm = await TrackPlayer.getPosition();
    if (mm > 0) {
      setVisiable(true);
    } else {
      if (visiable) setVisiable(false);
    }
  }
  const onProgress = TrackPlayer.useTrackPlayerProgress();
  const ProgressBar = () => {
    if (visiable) {
      return (
        <TouchableOpacity style={styles.progress}>
          <View style={{ flex: onProgress.position, backgroundColor: MAIN_COLOR }} />
          <View
            style={{
              flex: onProgress.duration - onProgress.position,
              backgroundColor: "red"
            }}
          />
        </TouchableOpacity>
      );
    } else {
      return <View></View>;
    }
  };
  const playbackState = TrackPlayer.usePlaybackState();

  const { style, onNext, onPrevious, onTogglePlayback, onStop, stateName } = props;

  var middleButtonText = "Play";

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    middleButtonText = "Pause";
  }
  const progress = TrackPlayer.useTrackPlayerProgress();

  function onForward() {
    return TrackPlayer.seekTo(progress.position + 15);
  }
  function onBackWard() {
    return TrackPlayer.seekTo(progress.position - 15);
  }
  const duration = Math.round(progress.duration);
  const position = Math.round(progress.position);

  momentDurationFormatSetup(moment);
  let positionII = moment.duration(position, "seconds").format("mm:ss", { trim: false });
  let durationII = moment.duration(duration, "seconds").format("mm:ss", { trim: false });
  return (
    <View style={[styles.card, style]}>
      {visiable ? (
        <View style={styles.propgress}>
          <Text>{positionII}</Text>
          <Text>{durationII}</Text>
        </View>
      ) : null}
      <ProgressBar />
      <View style={styles.controls}>
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="backward" size={25} color={MAIN_COLOR} onPress={onBackWard} />
        </TouchableOpacity>
        {playbackState === TrackPlayer.STATE_PLAYING ? (
          <TouchableOpacity style={{ width: 30, height: 30 }}>
            <Icon name="pause" size={25} color={MAIN_COLOR} onPress={onTogglePlayback} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity style={{ width: 30, height: 30 }}>
            <Icon name="play" size={25} color={MAIN_COLOR} onPress={onTogglePlayback} />
          </TouchableOpacity>
        )}
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="stop" size={25} color={MAIN_COLOR} onPress={onStop} />
        </TouchableOpacity>
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="forward" size={25} color={MAIN_COLOR} onPress={onForward} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Player;
Player.propTypes = {
  style: ViewPropTypes.style,
  onNext: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
  onTogglePlayback: PropTypes.func.isRequired
};

Player.defaultProps = {
  style: {}
};

const styles = StyleSheet.create({
  card: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },

  progress: {
    height: 3,
    width: "90%",
    marginTop: 8,
    marginBottom: 16,
    flexDirection: "row"
  },
  title: {
    ...iranSans,
    textAlign: "center"
  },
  artist: {
    fontWeight: "bold"
  },
  controls: {
    width: "100%",
    justifyContent: "space-around",
    flexDirection: "row"
  },
  controlButtonContainer: {
    // width: "100%",
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  controlButtonText: {
    fontSize: 18,
    textAlign: "center"
  },
  propgress: {
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "90%"
  }
});
