import React, { useEffect, useContext, useState, useRef } from "react";
import { StyleSheet, Text, View, Dimensions, Animated } from "react-native";
import TrackPlayer from "react-native-track-player";
import Player from "./PlayerScreeen";
import { Context as DataContext } from "../../Context/DataContext";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import { ARTIST } from "../../common/Strings";

export default function BlogMusicPlayer({ navigation, parameter, musicStop }) {
  const playbackState = TrackPlayer.usePlaybackState();

  useEffect(() => {
    stuff();
    onPlay();
  }, [parameter]);

  useEffect(() => {
    if (musicStop) {
      onStop();
    }
  }, [musicStop]);

  async function onPlay() {
    await TrackPlayer.reset();
    await TrackPlayer.add({
      id: "local-track",
      url: parameter.dataUrl,
      title: parameter.title,
      artist: ARTIST,
      artwork: parameter.cover,
    });
    await TrackPlayer.play();
  }
  const stuff = async () => {
    await TrackPlayer.setupPlayer();

    TrackPlayer.updateOptions({
      stopWithApp: false,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        // TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        // TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
        TrackPlayer.CAPABILITY_STOP,
      ],
      compactCapabilities: [TrackPlayer.CAPABILITY_PLAY, TrackPlayer.CAPABILITY_PAUSE],
    });
  };

  async function togglePlayback() {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack == null) {
      await TrackPlayer.add({
        id: "local-track",
        url: parameter.dataUrl,
        title: parameter.title,
        artist: ARTIST,
        artwork: parameter.cover,
      });
      await TrackPlayer.play();
    } else {
      if (playbackState === TrackPlayer.STATE_PAUSED) {
        await TrackPlayer.play();
      } else {
        await TrackPlayer.pause();
      }
    }
  }
  async function onStop() {
    await TrackPlayer.reset();
  }

  return (
    <View style={styles.container}>
      <Player
        onNext={skipToNext}
        style={styles.player}
        onPrevious={skipToPrevious}
        onTogglePlayback={togglePlayback}
        onStop={onStop}
        stateName={getStateName}
      />
    </View>
  );
}

BlogMusicPlayer.navigationOptions = {
  title: "Playlist Example",
};

function getStateName(state) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return "None";
    case TrackPlayer.STATE_PLAYING:
      return "Playing";
    case TrackPlayer.STATE_PAUSED:
      return "Paused";
    case TrackPlayer.STATE_STOPPED:
      return "Stopped";
    case TrackPlayer.STATE_BUFFERING:
      return "Buffering";
  }
}

async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}

const styles = StyleSheet.create({
  container: {
    padding: 16,
    height: Dimensions.get("screen").height / 4,
    alignItems: "center",
    backgroundColor: GOLDEN_COLOR,
    flex: 1,
  },

  player: {},
  state: {},
});
