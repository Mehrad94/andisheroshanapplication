import React from "react";
import { Text, View } from "react-native";
import { DotIndicator, BallIndicator } from "react-native-indicators";
import { centerAll, Flex, h2, iranSans, posAbs } from "../../common/Theme";
import { MAIN_COLOR } from "../../common/Colors";

export const Indicator = ({ color = MAIN_COLOR, size = 30, count = 8 }) => {
  return (
    <View style={[centerAll, Flex]}>
      <BallIndicator color={color} size={size} count={count} />
    </View>
  );
};
