import { StyleSheet, Dimensions } from "react-native";
import { posAbs, iranSans } from "../../common/Theme";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../common/Colors";
const { height, width } = Dimensions.get("screen");
const Styles = StyleSheet.create({
  Container: {
    width: "100%",
    backgroundColor: MAIN_COLOR,
    height: height / 14,
    flexDirection: "row-reverse",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10
  },

  view:{
width:width/1.99,
alignItems:'center'
  },

  Text: {
    ...iranSans,
    color: GOLDEN_COLOR,
    fontSize: 20,
    
  },
  img: {
    height: 30,
    width: 30
  }
});
export default Styles;
