import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Styles from "./styles";
import { GOLDEN_COLOR } from "../../common/Colors";
import Logo from "./../../Assets/2.png";
import { Context as DataContext } from "../../Context/DataContext";

const Header = ({ navigation, title = "عنوان مطالب" }) => {
  const context = useContext(DataContext);
  const { isConnected } = context.state;

  const onPressed = () => {
    if (!isConnected) {
      return null;
    } else {
      navigation.toggleDrawer();
    }
  };
  return (
    <View style={Styles.Container}>
      <TouchableOpacity activeOpacity={0.9} onPress={onPressed}>
        <Icon name="align-right" size={30} color={GOLDEN_COLOR} />
      </TouchableOpacity>
      <View style={Styles.view}>
        <Text style={Styles.Text} numberOfLines={1}>
          {title}
        </Text>
      </View>
      <Image source={Logo} style={Styles.img} />
    </View>
  );
};

export default Header;
