import React, { useEffect, useContext, useState } from "react";
import {
  View,
  Animated,
  Dimensions,
  Platform,
  UIManager,
  LayoutAnimation,
} from "react-native";
import { Context as DataContext } from "../../Context/DataContext";
import LandingScreen from "../../Screens/PlayerScreen.js";
const FloatinMusicPlayer = ({ data, type }) => {
  const scrollY = new Animated.Value(200);
  const fade = new Animated.Value(0);
  const { width } = Dimensions.get("screen");
  const context = useContext(DataContext);
  const { state, musicPlayerAction } = context;
  const [mori, setMOri] = useState(0);
  useEffect(() => {
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental &&
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    if (state.musicPlayer) {
      setMOri(100);
      //   startAnimation();
    } else {
      setMOri(0);
      //   stopAnimation();
    }
  }, [state.musicPlayer]);

  const startAnimation = () => {
    Animated.timing(scrollY, {
      useNativeDriver: true,
      toValue: 0,
      duration: 1000,
    }).start();
  };
  const stopAnimation = () => {
    Animated.timing(scrollY, {
      useNativeDriver: true,
      duration: 2000,
      toValue: 200,
    }).start();
  };
  const transformStyle = {
    transform: [
      {
        translateY: scrollY,
      },
    ],
  };



  return (
    <View style={[{ width: width, height: mori, zIndex: 1000 }]}>
      {mori === 100 ? <LandingScreen data={data} type={type} /> : null}
    </View>
  );
};
export default FloatinMusicPlayer;
