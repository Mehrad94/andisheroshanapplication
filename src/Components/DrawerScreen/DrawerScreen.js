import React, { useContext } from "react";
import { Image, View, Text, TouchableOpacity } from "react-native";
import Styles from "./styles";
import Icon from "react-native-vector-icons/AntDesign";
import { GOLDEN_COLOR } from "../../common/Colors";
import logo from "../../Assets/1.png";
import { Context as DataContext } from "../../Context/DataContext";
import { Context as AuthContext } from "../../Context/AuthContext";
import {
  WELLCOME_DEAR,
  HOME,
  BOOKMARKS,
  ABOUT_US,
  CONTACT_US,
  EXIT,
  VERSION
} from "../../common/Strings";
const DrawerScreen = ({ navigation }) => {
  const authontext = useContext(AuthContext);
  const { signout } = authontext;
  const context = useContext(DataContext);
  const { state } = context;
  let fullName = "";
  if (state.splashData) {
    fullName = state.splashData.userInformation.fullName;
  }

  return (
    <View style={Styles.Container}>
      <View>
        <Image style={Styles.img} source={logo} />

        <View style={Styles.touchable}>
          <Icon name="user" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>
            {fullName} {WELLCOME_DEAR}
          </Text>
        </View>
        <TouchableOpacity
          TouchableOpacity={0.7}
          style={Styles.touchable}
          onPress={() => navigation.navigate("Home")}
        >
          <Icon name="home" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>{HOME}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          TouchableOpacity={0.7}
          style={Styles.touchable}
          onPress={() => navigation.navigate("Bookmarks")}
        >
          <Icon name="tagso" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>{BOOKMARKS}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          TouchableOpacity={0.7}
          style={Styles.touchable}
          onPress={() => navigation.navigate("AboutUs")}
        >
          <Icon name="team" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>{ABOUT_US} </Text>
        </TouchableOpacity>
        <TouchableOpacity
          TouchableOpacity={0.7}
          style={Styles.touchable}
          onPress={() => navigation.navigate("ContactUs")}
        >
          <Icon name="phone" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>{CONTACT_US}</Text>
        </TouchableOpacity>
        <TouchableOpacity TouchableOpacity={0.7} style={Styles.touchable} onPress={() => signout()}>
          <Icon name="logout" size={20} color={GOLDEN_COLOR} />
          <Text style={Styles.text}>{EXIT}</Text>
        </TouchableOpacity>
      </View>
      <Text style={Styles.textVersion}>
        {VERSION}
        {state.appVersion}
      </Text>
    </View>
  );
};

export default DrawerScreen;
