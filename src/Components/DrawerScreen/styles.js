import {StyleSheet, Dimensions} from 'react-native';
import {iranSans} from '../../common/Theme';
import {MAIN_COLOR, GOLDEN_COLOR} from '../../common/Colors';
const {height, width} = Dimensions.get('screen');

const Styles = StyleSheet.create({
  Container: {
    height: '100%',
    justifyContent: 'space-between',
    backgroundColor:MAIN_COLOR
  },
  img: {
    alignSelf: 'center',
    width: width / 2,
    height: width / 2,

    borderRadius: 20,
    marginVertical: 20,
  },
  text: {
    fontFamily: 'iran_sans',
    color: GOLDEN_COLOR,
    fontSize: 14,
    marginRight: 10,
  },
  icon: {
    color: GOLDEN_COLOR,
  },
  touchable: {
    flexDirection: 'row-reverse',
    marginLeft: 5,
    marginTop: 10,
  },
  textVersion: {
    ...iranSans,
    textAlign: 'center',
    color: GOLDEN_COLOR,
    borderTopWidth:0.5,
    borderColor:GOLDEN_COLOR,
    alignSelf: 'center',
    width: '60%',
    height: height / 16,
    padding: 10,
    fontSize: 13,
  },
});
export default Styles;
