import React, { useEffect } from "react";
import { View, Text } from "react-native";
import { Flex, centerAll, iranSans, h4 } from "../../common/Theme";
import { NOTHING_TO_SHOW } from "../../common/Strings";
const RenderEmptyList = () => {
  return (
    <View style={[Flex, centerAll, { marginTop: 62 }]}>
      <Text style={[iranSans, h4]}>{NOTHING_TO_SHOW}</Text>
    </View>
  );
};
export default RenderEmptyList;
