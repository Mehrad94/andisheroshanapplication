import React, { useContext } from "react";
import BackgroundCursor from "./Slider/BackgroundCursor";
import { navigate } from "../../../navigationRef";
import { Context as DataContext } from "../../Context/DataContext";
const SliderHomeContainer = ({ data }) => {
  const context = useContext(DataContext);
  const { onSliderPresed } = context;
  const containerStuff = (item) => {
    console.log({ item });
    const { type } = item;
    if (type === "BLOG") {
      //done
      navigate("Video", { blog: item.blog });
    } else if (type === "AUDIO_BOOK") {
      //done
      onSliderPresed(item, () => navigate("AudioPack"));
    } else if (type === "SEMINAR") {
      //done
      onSliderPresed(item, () => navigate("SeminarPack"));
    } else {
      //done
      navigate("Pack", { course: item.course });
    }
  };
  return <BackgroundCursor selectItems={containerStuff} images={data} />;
};

export default SliderHomeContainer;
