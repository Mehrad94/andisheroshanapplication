import {StyleSheet} from 'react-native';
import {
  mH16,
  mV8,
  posAbs,
  iranSans,
  mV16,
  padV64,
  r8,
} from '../../../common/Theme';
import { MAIN_COLOR } from '../../../common/Colors';

const Styles = StyleSheet.create({
  Container: {
   
    flex: 1,
  },
  Text: {
    fontSize: 25,
    fontFamily: 'iran_sans',
    marginRight: 25,
    marginVertical: 10,
  },
  Pack: {
    backgroundColor: 'white',
    borderRadius: 5,
    width: '45%',
    marginTop: 10,
    marginHorizontal: 9,
    borderWidth:2,
    borderColor:MAIN_COLOR
  },
  img: {
    paddingVertical: 85,
    margin: 5,
  },
  TextTitle: {
    fontFamily: 'iran_sans',
    fontSize: 18,
    borderTopWidth: 2,
    width: 100,
    textAlign: 'center',
    borderColor: '#e8e8e8',
    color: '#d4af37',
  },
  TextDescription: {
    ...iranSans,
    fontSize: 10,
    flexDirection: 'column',
  },
  
});
export default Styles;
