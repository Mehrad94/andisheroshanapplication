import React from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import Styles from './style';
import  Header  from '../../../Components/Header';

const CategoryScreen = ({navigation}) => {
  const Title = 'بازاریابی';
  const Category = [
    {
      list: 'https://picsum.photos/200/300.jpg',
      title: 'ویدیو ۱',
      description: 'توضیحات ویدیو اول می باشد',
    },
    {
      list: 'https://picsum.photos/id/237/200/300',
      title: 'ویدیو دوم',
      description: 'توضیحات ویدیو دوم می باشد',
    },
    {
      list: 'https://i.picsum.photos/id/1024/1920/1280.jpg',
      title: 'ویدیو سوم',
      description: 'توضیحات ویدیو سوم می باشد',
    },
    {
      list: 'https://i.picsum.photos/id/1024/1920/1280.jpg',
      title: 'ویدیو چهارم',
      description: 'توضیحات ویدیو چهارم می باشد',
    },
    {
      list: 'https://picsum.photos/200/300.jpg',
      title: 'ویدیو ۵',
      description: 'توضیحات ویدیو ۵ می باشد',
    },
  ]
  return (
    <View style={Styles.Container}>
      <Header navigation={navigation}/>
      <FlatList
      
      initialNumToRender={4}
      
        numColumns={2}
        keyExtractor={CategoryList => CategoryList.list}
        data={Category}
        renderItem={({item}) => {
          return (
            <TouchableOpacity
              style={Styles.Pack}
              onPress={() => navigation.navigate('Video')}>
              <Image style={Styles.img} source={{uri: item.list}} />
              <View style={{alignItems: 'center'}}>
                <Text style={Styles.TextTitle}>{item.title}</Text>
                <Text style={Styles.TextDescription}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default CategoryScreen;
