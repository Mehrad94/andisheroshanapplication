import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  ScrollView,
  Dimensions,
  Image,
  StyleSheet,
  I18nManager,
  TouchableOpacity,
} from "react-native";
import InvertibleScrollView from "react-native-invertible-scroll-view";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import { navigate } from "../../../../navigationRef";
const DEVICE_WIDTH = Dimensions.get("window").width;
class BackgroundCursor extends React.Component {
  scrollRef = React.createRef();
  constructor(props) {
    super(props);

    this.state = {
      selectedIndex: 0,
    };
    this.scrollRef = React.createRef();
  }

  componentDidMount = () => {
    setInterval(() => {
      this.setState(
        (prev) => ({
          selectedIndex:
            prev.selectedIndex === this.props.images.length - 1 ? 0 : prev.selectedIndex + 1,
        }),
        () => {
          this.scrollRef.current.scrollTo({
            animated: true,
            x: DEVICE_WIDTH * this.state.selectedIndex,
            y: 0,
          });
        }
      );
    }, 4000);
  };

  setSelectedIndex = (event) => {
    const contentOffset = event.nativeEvent.contentOffset;
    const viewSize = event.nativeEvent.layoutMeasurement;

    // Divide the horizontal offset by the width of the view to see which page is visible
    const selectedIndex = Math.floor(contentOffset.x / viewSize.width);
    this.setState({ selectedIndex });
  };
  onPressImage = (item) => {
    this.props.selectItems(item);
  };
  render() {
    const { images } = this.props;

    const { selectedIndex } = this.state;
    return (
      <View style={{ height: 200, width: "100%" }}>
        <InvertibleScrollView
          inverted
          style={{ flexDirection: I18nManager.isRTL ? "row-reverse" : "row" }}
          horizontal
          pagingEnabled
          onMomentumScrollEnd={this.setSelectedIndex}
          ref={this.scrollRef}
        >
          {images.map((image, index) => {
            return (
              <TouchableOpacity
                key={index * Math.random()}
                activeOpacity={0.1}
                onPress={() => this.onPressImage(image)}
              >
                <Image
                  resizeMode={"stretch"}
                  style={styles.backgroundImage}
                  source={{ uri: image.image }}
                  key={image}
                />
              </TouchableOpacity>
            );
          })}
        </InvertibleScrollView>
        <View style={styles.circleView}>
          {images
            .map((image, i) => (
              <View
                style={[styles.whiteCircle, { opacity: i === selectedIndex ? 1 : 0.5 }]}
                key={image + Math.random()}
                active={i === selectedIndex}
              />
            ))
            .reverse()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundImage: {
    height: 200,
    width: DEVICE_WIDTH,
  },
  whiteCircle: {
    width: 8,
    height: 8,
    borderRadius: 3,
    margin: 5,
    borderColor: GOLDEN_COLOR,
    borderWidth: 2,
  },
  circleView: {
    position: "absolute",
    bottom: 15,
    height: 10,

    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
  },
  contentContainerStyle: {
    flexDirection: "row-reverse",
  },
});
export default BackgroundCursor;
