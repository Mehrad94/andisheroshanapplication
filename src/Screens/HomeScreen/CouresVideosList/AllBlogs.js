import React, { useState, useEffect, useContext, useRef } from "react";
import { FlatList, Text, Dimensions } from "react-native";
import CouresVideoRow from "./CoursesVideoRow";
import { Context as DataContext } from "../../../Context/DataContext";
import Axios from "../../../Api/AxiosConfigue";
import { Indicator } from "../../../Components/Indicator";
import { View } from "native-base";
import { Flex, centerAll } from "../../../common/Theme";
import RenderEmptyList from "../../../Components/RenderEmptyLists";

const DiscountListContainer = ({ navigation, data, loadMore, resetData }) => {
  const onViewRef = useRef(({ viewableItems, changed }) => {});
  const viewConfigRef = useRef({ viewAreaCoveragePercentThreshold: 100, minimumViewTime: 500 });
  //========================CONETXT ==========================
  navigation.addListener("didFocus", () => {});
  const context = useContext(DataContext);
  const { state } = context;

  //=========================STATE ===========================
  const [localState, setState] = useState({
    blogs: [],
  });
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [refreshing, setRefreshing] = useState(false);
  const [totalpage, setTotalPage] = useState(5);
  const [loadData, setLoadData] = useState(false);
  //=========================STATE ===========================
  useEffect(() => {
    setState({ blogs: [] });
    setPage(1);
  }, [state.filterCat]);
  console.log({ Homestate: state });
  //========================LOGIC ============================

  const mergeBlogs = async () => {
    if (!state.filterCat) {
      setLoading(true);
      if (page <= totalpage) {
        await Axios.get(`/blog/${page}`)
          .then((res) => {
            let docs = res.data;
            setTotalPage(docs.pages);
            setState((prevState) => {
              return {
                blogs: page === 1 ? docs.docs : [...prevState.blogs, ...docs.docs],
              };
            });
            setRefreshing(false);
            setLoading(false);
          })
          .catch((e) => {
            console.log({ e });
          });
      } else {
        setRefreshing(false);
        setLoading(false);
      }
    } else {
      if (localState.blogs.length > 0) {
        if (localState.blogs[0].category !== state.filterCat) {
          setState({ blogs: [] });
          setPage(1);
        }
      }
      setLoading(true);
      if (page <= totalpage) {
        await Axios.get(`/blog/cat/${state.filterCat}/${page}`)
          .then((res) => {
            let docs = res.data;

            setTotalPage(docs.pages);
            setState((prevState) => {
              return {
                blogs: page === 1 ? docs.docs : [...prevState.blogs, ...docs.docs],
              };
            });
            setRefreshing(false);
            setLoading(false);
          })
          .catch((e) => {
            console.log({ e });
          });
      } else {
        setRefreshing(false);
        setLoading(false);
      }
    }
  };
  useEffect(() => {
    if (loadData) {
      if (page <= totalpage) {
        setLoading(true);
        setTimeout(() => {
          setPage((prevState) => prevState + 1);
        }, 2000);
      }
    }
    setLoadData(true);
  }, [loadMore]);

  console.log({ page, totalpage });

  useEffect(() => {
    mergeBlogs();
  }, [page, state.filterCat]);

  useEffect(() => {
    console.log("rese");

    if (resetData) {
      // setState({ blogs: [] });
      setPage(1);
      mergeBlogs();
    }
  }, [resetData]);

  const renderItem = ({ item }) => {
    return <CouresVideoRow items={item} />;
  };

  const renderFooter = () => {
    if (!loading) return null;

    return (
      <View style={[Flex, centerAll, { height: 100 }]}>
        <Indicator size={30} />
      </View>
    );
  };

  const handleRefresh = () => {};
  if (!loading && localState.blogs.length < 1) {
    return <RenderEmptyList />;
  }
  return (
    <View style={{ flex: 1 }}>
      <FlatList
        keyExtractor={(CategoryList, index) => Math.random(index) * 5.64}
        data={localState.blogs}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        // ListEmptyComponent={() => <Indicator />}
        ListFooterComponent={renderFooter}
        refreshing={refreshing}
        onRefresh={handleRefresh}
        // onEndReached={handleLoadMore}
        // onEndReachedThreshold={0.1}
        initialNumToRender={5}
        viewabilityConfig={viewConfigRef.current}
        onViewableItemsChanged={onViewRef.current}
        getItemLayout={(dat, index) => {
          return {
            length: Dimensions.get("window").height / 3,
            offset: (Dimensions.get("window").height / 3) * index,
            index,
          };
        }}
        removeClippedSubviews={true}
        snapToAlignment={"end"}
        // Defines here the interval between to item (basically the width of an item with margins)
        snapToInterval={Dimensions.get("window").height / 3}
      />
    </View>
  );
};

export default DiscountListContainer;
