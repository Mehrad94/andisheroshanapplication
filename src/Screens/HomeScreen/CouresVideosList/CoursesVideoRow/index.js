import React, { useEffect } from "react";
import { View, Text, TouchableOpacity, Image} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Styles from "./styles";

import { h3, h5, h4, h6 } from "../../../../common/Theme";
import { GOLDEN_COLOR } from "../../../../common/Colors";
import { navigate } from "../../../../../navigationRef";
const CouresVideoRow = (props) => {
  const { like, title, description, dataUrl, cover, type, createdTime } = props.items;

  return (
    <TouchableOpacity
      style={Styles.Container}
      onPress={() => {
        navigate("Video", { blog: props.items });
      }}
    >
      <View>
        <Image style={Styles.ImageVideo} source={{ uri: cover }} resizeMode={"stretch"} />

        <View style={Styles.CoverView}>
          <Icon name="playcircleo" size={50} color={GOLDEN_COLOR} />
        </View>

        <View style={Styles.AbsoluteView}>
          <Icon name="heart" size={20} color="red" />
          <Text style={Styles.AbsoluteViewText}>{like}</Text>
        </View>

        <View style={Styles.DiscriptionView}>
          <Text style={[Styles.DscriptionText, h4]}>{title}</Text>
          <Text style={[Styles.DscriptionText, h6]}>{description}</Text>
        </View>

        <View style={Styles.DateView}>
          <Text style={Styles.DateViewText}>{createdTime}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};
export default CouresVideoRow;
