import { StyleSheet } from "react-native";
import { mH16, mV8, posAbs, iranSans, h5, h6 } from "../../../../common/Theme";
const Styles = StyleSheet.create({
  Container: {
    ...mH16,
    ...mV8
  },
  ImageVideo: { height: 200, borderRadius: 5 },
  CoverView: {
    ...posAbs,
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },

  AbsoluteView: {
    zIndex: 400,
    ...posAbs,
    flexDirection: "row",
    left: 5,
    bottom: 20
  },
  AbsoluteViewText: {
    color: "white",
    ...h6,
    marginLeft: 4,
    fontFamily: "iran_sans"
  },
  DiscriptionView: {
    ...posAbs,
    bottom: 0,
    width: "100%",
    flexDirection: "column",
    borderBottomEndRadius: 5,
    borderBottomStartRadius: 5,
    backgroundColor: "rgba(52, 52, 52, 0.4)"
  },
  DscriptionText: {
    ...iranSans,
    ...h6,
    color: "white",
    right: 5
  },
  DateView: {
    ...posAbs,
    height: "100%",
    width: "100%",
    justifyContent: "flex-end",
    left: 5
  },
  DateViewText: {
    color: "white",
    ...h6,
    fontFamily: "iran_sans"
  }
});
export default Styles;
