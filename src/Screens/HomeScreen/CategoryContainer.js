import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image, ScrollView } from "react-native";
import {
  m4,
  m16,
  pad4,
  posAbs,
  mH16,
  padH16,
  iranSans,
  mV16,
  padV64,
  padV8,
  r8
} from "../../common/Theme";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../common/Colors";
import { Context as DataContext } from "../../Context/DataContext";

const CategoryContainer = ({ navigation, data }) => {
  //==============================++CONETXT====================
  const context = useContext(DataContext);
  const { filterCategoryAction } = context;
  //==============================++CONETXT====================
  const newItem = { ـid: 123, title: "همه" };
  let newArray = [newItem, ...data];

  return (
    <FlatList
      horizontal
      inverted
      showsHorizontalScrollIndicator={false}
      keyExtractor={(CategoryList, index) => Math.random(index * 16.3654).toString()}
      data={newArray}
      renderItem={({ item }) => {
        return (
          <TouchableOpacity
            style={[mH16, mV16, padV8, padH16, r8, { backgroundColor: MAIN_COLOR }]}
            onPress={() => filterCategoryAction(item._id)}
          >
            <Text style={[iranSans, { color: GOLDEN_COLOR }]}>{item.title}</Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

export default CategoryContainer;
