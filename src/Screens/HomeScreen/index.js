import React, { useContext, useState, useEffect } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
  Dimensions,
  StatusBar,
} from "react-native";
import SliderHomeContainer from "./SliderHomeContainer";
import CategoryContainer from "./CategoryContainer";
import AllBlogs from "./CouresVideosList/AllBlogs";
import Icon from "react-native-vector-icons/FontAwesome5";

import { m4, m16, tr0, posAbs, s48, m8 } from "../../common/Theme";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import { Context as DataContext } from "../../Context/DataContext";
import { Context as UserData } from "../../Context/userDataContext";
const HomeScreen = ({ navigation }) => {
  //==============================+CONETXT  ==========================
  const context = useContext(DataContext);
  const { state } = context;
  const blog = state.splashData.blog;
  const sliders = state.splashData.sliders;
  const categories = state.splashData.categories;
  const [loadMore, setLoadMore] = useState(false);
  const [loading, setLoading] = useState(false);
  const userContext = useContext(UserData);
  const { getFreshUserData } = userContext;

  //==============================+CONETXT  ==========================
  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 0.5;
    return layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingToBottom;
  };
  useEffect(() => {
    getFreshUserData();
  }, []);
  const onEndeReached = () => {
    setLoadMore(!loadMore);
  };

  const onRefresh = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 2000);
  };

  return (
    <ScrollView
      refreshControl={<RefreshControl refreshing={loading} onRefresh={onRefresh} />}
      onScroll={({ nativeEvent }) => {
        if (isCloseToBottom(nativeEvent)) {
          onEndeReached();
        }
      }}
      scrollEventThrottle={1000}
    >
      <StatusBar backgroundColor={MAIN_COLOR} barStyle="light-content" />

      <View style={{ flex: 1 }}>
        <SliderHomeContainer data={sliders} />
        <CategoryContainer navigation={navigation} data={categories} />

        <AllBlogs navigation={navigation} data={blog} loadMore={loadMore} resetData={loading} />

        <TouchableOpacity style={[posAbs, tr0, m4]} onPress={() => navigation.toggleDrawer()}>
          <Icon name="align-right" size={50} color={GOLDEN_COLOR} />
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default HomeScreen;
