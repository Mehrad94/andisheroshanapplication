import React, { useState, useContext } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity,
  ToastAndroid
} from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Styles from "./style";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import axios from "../../../Api/AxiosConfigue";
import { Context as userDataContext } from "../../../Context/userDataContext";
import { Indicator } from "../../../Components/Indicator";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import ImagePicker from "react-native-image-picker";
import {
  AVATAR_SELECT,
  FROM_CAMERA,
  FROM_STORAGE,
  UNDETERMINARED,
  DENIED,
  GRANTED,
  AUTORIZED,
  NAME_FAMILY,
  EMAIL,
  MOBILE_NUMBER,
  ADDRESS,
  JOB_TITLE,
  PROPBLEM_IN_UPLOAD
} from "../../../common/Strings";
import { useEffect } from "react";

const EditProfile = ({ navigation }) => {
  const context = useContext(userDataContext);
  const { state, getFreshUserData } = context;
  console.log({ Edit: state });
  let ProfilePng = "";
  let userName = "";
  let userEmail = "";
  let mobileNumber = "";
  let userAddress = "";
  let userPhoneNumber = "";
  let userJob = "";
  const item = state.userDataState;
  useEffect(() => {}, [state]);
  if (item) {
    userName = item.fullName;
    mobileNumber = item.phoneNumber;
    userEmail = item.email;
    userJob = item.job;
    userAddress = item.address;
    ProfilePng = item.avatar;
  }
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [address, setAddress] = useState("");
  const [job, setJob] = useState("");
  const [loading, setLoading] = useState(false);
  const [imageLoading, setImageLoading] = useState(false);
  const [newAvatar, setNewAvatar] = useState("");
  const onEmailChange = (text) => {
    setEmail(text);
  };
  const onNameChange = (text) => {
    setName(text);
  };
  const onPhoneNumberChange = (text) => {
    setMobile(text);
  };
  const onAddressChange = (text) => {
    setAddress(text);
  };
  const onJobChange = (text) => {
    setJob(text);
  };
  const updateUserInformation = async () => {
    //=================== NAME =======================
    setLoading(true);
    if (name.length > 0) {
      axios
        .put("/user", {
          fieldChange: "fullName",
          newValue: name
        })
        .then((res) => {
          console.log({ res });
          getFreshUserData();
        })
        .catch((e) => {
          console.log({ e });
        });
    }
    //=================== EMAIL =======================
    if (email.length > 0) {
      axios
        .put("/user", {
          fieldChange: "email",
          newValue: email
        })
        .then((res) => {
          console.log({ res });
          getFreshUserData();
        })
        .catch((e) => {
          console.log({ e });
        });
    }
    //=================== ADDRESS =======================

    if (address.length > 0) {
      axios
        .put("/user", {
          fieldChange: "address",
          newValue: address
        })
        .then((res) => {
          console.log({ res });
          getFreshUserData();
        })
        .catch((e) => {
          console.log({ e });
        });
    }
    //=================== JOB =======================

    if (job.length > 0) {
      axios
        .put("/user", {
          fieldChange: "job",
          newValue: job
        })
        .then((res) => {
          console.log({ res });
          getFreshUserData();
        })
        .catch((e) => {
          console.log({ e });
        });
    }
    //=================== MOBILE =======================

    if (mobile.length > 0) {
      axios
        .put("/user", {
          fieldChange: "phoneNumber",
          newValue: mobile
        })
        .then((res) => {
          console.log({ res });
          getFreshUserData();
        })
        .catch((e) => {
          console.log({ e });
        });
    }
    setTimeout(() => {
      navigation.pop();
    }, 2000);
  };
  const options = {
    title: AVATAR_SELECT,
    takePhotoButtonTitle: FROM_CAMERA,
    chooseFromLibraryButtonTitle: FROM_STORAGE,
    cancelButtonTitle: "لغو",
    quality: 0.1,
    storageOptions: {
      skipBackup: true,
      path: "images"
    }
  };
  const requestPermission = () => {
    check(PERMISSIONS.ANDROID.CAMERA).then((response) => {
      console.log({ response });

      if (response === DENIED || response === UNDETERMINARED) {
        request(PERMISSIONS.ANDROID.CAMERA).then((res) => {
          imagePick();
        });
      } else if (response === AUTORIZED || response === GRANTED) {
        imagePick();
      }
    });
  };
  const imagePick = () => {
    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        console.log(response.didCancel);
      } else if (response.error) {
      } else {
        setNewAvatar(response.uri);
        console.log({ ur: response.uri });
      }
    });
  };
  useEffect(() => {
    uploadImage();
  }, [newAvatar]);
  const uploadImage = async () => {
    if (newAvatar) {
      console.log({ newAvatar });

      setImageLoading(true);
      let formData = new FormData();
      formData.append("image", {
        uri: newAvatar,
        name: "image.jpeg",
        type: "image/jpeg"
      });
      console.log({ formData });
      axios
        .post("/upload", formData)
        .then((res) => {
          console.log({ upload: res });

          axios
            .put("/user", {
              fieldChange: "avatar",
              newValue: res.data.imageUrl
            })
            .then((res) => {
              setImageLoading(false);

              console.log({ res });
              getFreshUserData();
            })
            .catch((eImageUpload) => {
              ToastAndroid.show(PROPBLEM_IN_UPLOAD, ToastAndroid.SHORT);

              setImageLoading(false);

              console.log({ eImageUpload });
            });
        })
        .catch((eUpload) => {
          ToastAndroid.show(PROPBLEM_IN_UPLOAD, ToastAndroid.SHORT);

          setImageLoading(false);

          console.log({ eUpload });
        });
    }
  };
  const onImageChange = () => {
    requestPermission();
  };
  return (
    <View style={Styles.Container}>
      <View style={Styles.TopBack}></View>

      <ScrollView style={Styles.ViewCenter}>
        <Image source={{ uri: ProfilePng }} style={Styles.img} />
        <TouchableOpacity style={Styles.icon}>
          <Icon name="pluscircle" size={30} color={MAIN_COLOR} onPress={onImageChange} />
        </TouchableOpacity>
        <TouchableOpacity style={Styles.Edit} onPress={updateUserInformation}>
          {loading ? (
            <Indicator size={20} />
          ) : (
            <Icon name="checksquareo" size={30} color={MAIN_COLOR} />
          )}
        </TouchableOpacity>
        <View style={Styles.ViewInput}>
          <Text style={Styles.TextInput}>{NAME_FAMILY}:</Text>
          <TextInput
            style={Styles.Input}
            value={name}
            placeholder={userName}
            onChangeText={onNameChange}
          />
        </View>
        <View style={Styles.ViewInput}>
          <Text style={Styles.TextInput}>{EMAIL}:</Text>
          <TextInput
            style={Styles.Input}
            keyboardType="email-address"
            placeholder={userEmail}
            onChangeText={onEmailChange}
            value={email}
          />
        </View>
      
        <View style={Styles.ViewInput}>
          <Text style={Styles.TextInput}>{ADDRESS}:</Text>
          <TextInput
            numberOfLines={1}
            style={Styles.Input}
            value={address}
            placeholder={userAddress}
            onChangeText={onAddressChange}
          />
        </View>
        <View style={Styles.ViewInput}>
          <Text style={Styles.TextInput}>{JOB_TITLE}:</Text>
          <TextInput
            style={Styles.Input}
            value={job}
            placeholder={userJob}
            onChangeText={onJobChange}
          />
        </View>
      </ScrollView>
      {imageLoading ? (
        <View
          style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(255,255,255,0.3)",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Indicator size={30} />
        </View>
      ) : null}
    </View>
  );
};

export default EditProfile;
