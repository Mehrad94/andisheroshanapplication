import {StyleSheet, Dimensions} from 'react-native';
import {MAIN_COLOR} from '../../../common/Colors';
import { posAbs, iranSans } from '../../../common/Theme';
const { width} = Dimensions.get('screen');

const Styles = StyleSheet.create({
  Container: {
    flex: 1
  },
  TopBack: {
    height: width/1.5,
    backgroundColor: MAIN_COLOR,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50
  },

  ViewCenter: {
    position: 'absolute',
    width: '90%',
    height: '85%',
    backgroundColor: '#e8e8e8',
    borderRadius: 25,
    alignSelf: "center",
    top: width / 5
  },
  img: {
    // position: 'absolute',
    width: width / 3.2,
    height: width / 3.2,
    borderRadius: 60,
    alignSelf: "center",
    top: width / 60,
    marginBottom: 15
  },
  TextInput: {
    fontFamily: "iran_sans",
    marginRight: 20,
    marginTop: 5,

  },
  ViewInput: {},
  Input: {
    borderWidth:1.2,
    borderColor: MAIN_COLOR,
    width: '90%',
    alignSelf: 'center',
    borderRadius:5,
    ...iranSans,
    fontSize:13
  },
  icon: {
    ...posAbs,
    top:width / 4.5,
    right: width / 3.5
  },
  Edit: {
    position: "absolute",
    right: 15,
    top: 10
  },
 
});
export default Styles;
