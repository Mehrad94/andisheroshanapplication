import { StyleSheet, Dimensions } from "react-native";
const {  width } = Dimensions.get("screen");
import { MAIN_COLOR, GOLDEN_COLOR } from "../../common/Colors";
import { iranSans } from "../../common/Theme";

const Styles = StyleSheet.create({
  Container: {
    flex: 1
  },
  TopBack: {
    height: width / 1.45,
    backgroundColor: MAIN_COLOR,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50
  },
  Text: {
    fontFamily: "iran_sans",
    fontSize: 35,
    paddingVertical: 40,
    paddingHorizontal: 25,
    color: GOLDEN_COLOR
  },
  Edit: {
    position: "absolute",
    right: 15,
    top: 10
  },
  ViewCenter: {
    position: "absolute",
    width: width / 1.1,
    height: width * 1.1,
    backgroundColor: "#e8e8e8",
    borderRadius: 25,
    alignSelf: "center",
    top: width / 3
  },
  img: {
    position: "absolute",
    width: width / 3,
    height: width / 3,
    borderRadius: 60,
    alignSelf: "center",
    bottom: width/ 1.05
  },
  Name: {
    width: "100%",
    alignSelf: "center",
    top: width / 6
  },
  TextName: {
    textAlign: "center",
    fontFamily: "iran_sans",
    bottom: 5
  },
  TextEmail: {
    textAlign: "center",
    paddingBottom: 5,
    borderBottomWidth: 2,
    borderColor: "white",
    ...iranSans
  },

  Number: {
    flexDirection: "row-reverse",

    paddingVertical: 20,
    paddingHorizontal: 10
  },
  TextInput: {
    fontFamily: "iran_sans",
    marginRight: 10,
    width: width / 1.31,
    textAlign: "right"
  }
});
export default Styles;
