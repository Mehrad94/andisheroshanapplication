import React, { useState, useEffect, useContext } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Styles from "./style";

import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import { Context as userDataContext } from "../../Context/userDataContext";
import { Context as AuthContext } from "../../Context/AuthContext";
import { Indicator } from "../../Components/Indicator";
import {
  EXIT,
  TRY_AGAIN,
  YOUR_NAME,
  YOUR_EMAIL,
  YOUR_NUMBER,
  YOUR_ADDRESS,
  YOUR_JOB
} from "../../common/Strings";
import { centerAll, iranSans, fWhite } from "../../common/Theme";
const ProfileScreen = ({ navigation }) => {
  const authContext = useContext(AuthContext);
  const { signout } = authContext;
  const context = useContext(userDataContext);
  const { state, getFreshUserData } = context;
  const [loading, setLoading] = useState(false);
  const [tryAgain, setTryAgian] = useState(false);
  const [tryLoading, setTryLoading] = useState(false);
  console.log({ state });
  let ProfilePng = "";
  let userName = YOUR_NAME;
  let userEmail = YOUR_EMAIL;
  let mobileNumber = YOUR_NUMBER;
  let address = YOUR_ADDRESS;
  let job = YOUR_JOB;
  const item = state.userDataState;
  if (item) {
    if (item.fullName) {
      userName = item.fullName;
    }
    if (item.phoneNumber) {
      mobileNumber = item.phoneNumber;
    }
    if (item.email) {
      userEmail = item.email;
    }
    if (item.job) {
      job = item.job;
    }
    if (item.address) {
      address = item.address;
    }
    ProfilePng = item.avatar;
  }
  useEffect(() => {
    if (state.error) {
      setTryAgian(true);
      setTryLoading(false);
    } else {
      setTryAgian(false);
      setTryLoading(false);
    }
  }, [state]);
  useEffect(() => {
    if (item) {
      setLoading(false);
    }
  }, [item]);
  useEffect(() => {
    setLoading(true);
    getFreshUserData();
  }, []);
  const onTryAgainPress = () => {
    setTryLoading(true);
    getFreshUserData();
  };
  return (
    <View style={Styles.Container}>
      <View style={Styles.TopBack}></View>
      <View style={Styles.ViewCenter}>
        <Image source={{ uri: ProfilePng }} style={Styles.img} />
        <TouchableOpacity style={Styles.Edit} onPress={() => navigation.navigate("Edit")}>
          <Icon name="form" size={30} color={MAIN_COLOR} />
        </TouchableOpacity>

        <View style={Styles.Name}>
          <Text style={Styles.TextName}>{userName} </Text>
          <Text style={Styles.TextEmail}>{userEmail}</Text>

          <View style={Styles.Number}>
            <Icon name="mobile1" size={20} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{mobileNumber}</Text>
          </View>

          <View style={Styles.Number}>
            <Icon name="pushpino" size={20} color={MAIN_COLOR} />
            <Text style={Styles.TextInput} numberOfLines={1}>
              {address}
            </Text>
          </View>

          <View style={Styles.Number}>
            <Icon name="skin" size={20} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{job}</Text>
          </View>

          <TouchableOpacity style={Styles.Number} onPress={() => signout()}>
            <Icon name="logout" size={20} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{EXIT}</Text>
          </TouchableOpacity>
        </View>
      </View>
      {loading ? (
        <View
          style={{
            position: "absolute",
            width: "100%",
            height: "100%",
            backgroundColor: "rgba(255,255,255,0.3)",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          {tryAgain ? (
            <TouchableOpacity
              onPress={onTryAgainPress}
              style={[centerAll, { padding: 16, height: 50, backgroundColor: MAIN_COLOR }]}
            >
              {tryLoading ? (
                <Indicator color="white" />
              ) : (
                <Text style={[iranSans, fWhite]}>{TRY_AGAIN}</Text>
              )}
            </TouchableOpacity>
          ) : (
            <Indicator size={30} />
          )}
        </View>
      ) : null}
    </View>
  );
};

export default ProfileScreen;
