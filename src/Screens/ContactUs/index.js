import React, {useState, useEffect} from 'react';
import {View, Text, Linking, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/FontAwesome5';
import Styles from './styles';
import {GOLDEN_COLOR, MAIN_COLOR} from '../../common/Colors';
import Header from '../../Components/Header';

const ContactUs = ({navigation}) => {

  const _onWebsiteClicked = () => {
    Linking.openURL("https://www.andisheroshan.com");
  };

  const _onInstagramClicked = () => {
    Linking.openURL("https://www.instagram.com/andisheroshan_com");
  };

  const _onTelegramClicked = () => {
    Linking.openURL("https://t.me/Andisheroshant");
  };
  const Category = {
  
    site: 'www.andisheroshan.com',
    mobile: '09394018034',
    instagram: 'andisheroshan_com',
    telegram: 'Andisheroshant',
  };
  const title = 'تماس با ما'
  return (
    <View style={Styles.Container}>
       <Header navigation={navigation} title={title}/>
      <View style={Styles.TopBack}></View>
      <View style={Styles.ViewCenter}>

          <TouchableOpacity  style={Styles.Number} onPress={_onWebsiteClicked}>
            <Icon name="earth" size={25} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{Category.site}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={_onInstagramClicked} style={Styles.Number}>
            <Icon name="instagram" size={25} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{Category.instagram}</Text>
          </TouchableOpacity>

          <TouchableOpacity onPress={_onTelegramClicked } style={Styles.Number}>
            <Icon1 name="telegram-plane" size={30} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{Category.telegram}</Text>
          </TouchableOpacity>

          <View style={Styles.Number}>
            <Icon name="mobile1" size={25} color={MAIN_COLOR} />
            <Text style={Styles.TextInput}>{Category.mobile}</Text>
          </View>

       
      </View>
    </View>
  );
};

export default ContactUs;
