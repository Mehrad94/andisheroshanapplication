import {StyleSheet, Dimensions} from 'react-native';
const { width} = Dimensions.get('screen');
import {MAIN_COLOR, GOLDEN_COLOR} from '../../common/Colors';

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  TopBack: {
    height: width/1.5,
    backgroundColor: MAIN_COLOR,
    borderBottomStartRadius: 50,
    borderBottomEndRadius: 50,
  },
  Text: {
    fontFamily: 'iran_sans',
    fontSize: 35,
    paddingVertical: 40,
    paddingHorizontal: 25,
    color: GOLDEN_COLOR,
  },
  Edit: {
    position: 'absolute',
    right: 10,
    top: 10,
  },
  ViewCenter: {
    position: 'absolute',
    width: width/1.1,
    height: width/1.2,
    backgroundColor: '#e8e8e8',
    borderRadius: 25,
    alignSelf: 'center',
    top: width / 3,
  },


  Number: {
    flexDirection: 'row',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  TextInput: {
    fontFamily: 'iran_sans',
    marginLeft: 10,
    marginTop: 5,
  },
});
export default Styles;
