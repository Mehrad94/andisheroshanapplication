import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, Image, ToastAndroid } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import { iranSans } from "../../../common/Theme";
import Styles from "./style";
import { PLEASE_PAY } from "../../../common/Strings";
import { MAIN_COLOR } from "../../../common/Colors";
import { navigate } from "../../../../navigationRef";
import { Context as DataContext } from "../../../Context/DataContext";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
const VideoTab = ({ data, onVideoMakeDirectory, isPaid, type, idCourse }) => {
  console.log({ NEMIDUNAM: isPaid });

  //=========================CONTEXT =================
  const context = useContext(DataContext);
  const { onDownloadVideoAction } = context;
  //=========================CONTEXT =================
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    setLoading(false);
  });
  const onDownload = (data) => {
    if (isPaid) {
      onDownloadVideoAction(data.video);
      onVideoMakeDirectory(data);
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };

  const onPressPlay = (item) => {
    if (isPaid) {
      console.log({ dataPlay: item });

      navigate("SecVideo", { video: item, type: type, idCourse: idCourse });
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };
  if (data) {
    if (data.length < 1) {
      return <RenderEmptyList />;
    }
  }
  return (
    <FlatList
      style={Styles.flat}
      showsHorizontalScrollIndicator={false}
      keyExtractor={(index) => index._id}
      data={data}
      renderItem={({ item }) => {
        return (
          <View style={Styles.Container}>
            <View style={Styles.view}>
              <Icon
                name="playcircleo"
                color={MAIN_COLOR}
                size={40}
                style={Styles.playIcon}
                onPress={() => onPressPlay(item)}
              />
              <View style={Styles.TextView}>
                <Text style={Styles.Title} numberOfLines={1}>
                  {item.title}
                </Text>
                <Text style={[iranSans]} numberOfLines={1}>
                  {item.description}
                </Text>
              </View>
            </View>
            <Icon
              name="download"
              color={item.downloaded ? "#eee" : MAIN_COLOR}
              size={25}
              style={Styles.downloadIcon}
              onPress={() => onDownload(item)}
            />
          </View>
        );
      }}
    />
  );
};

export default VideoTab;
