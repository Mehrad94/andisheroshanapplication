import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Styles from "./styles";
import Header from "../../../../Components/Header";
import { MAIN_COLOR } from "../../../../common/Colors";
import { Context as DataContext } from "../../../../Context/DataContext";
import { SEMINAR_TITLE } from "../../../../common/Strings";
import { TOMAN } from "../../../../common/Strings";
import { navigate } from "../../../../../navigationRef";
import { Context as UserContext } from "../../../../Context/userDataContext";
const irAmount = require("iramount");

const SeminarList = ({ navigation }) => {
  //===========================CONTEXT =============================
  const context = useContext(DataContext);
  const userContext = useContext(UserContext);
  const { state, getSeminarItem } = context;
  const userState = userContext.state;

  //===========================CONTEXT =============================

  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} title={SEMINAR_TITLE} />
      <FlatList
        numColumns={2}
        keyExtractor={(CategoryList, index) => Math.random(index) * 1.62}
        data={state.seminar}
        renderItem={({ item }) => {
          let isPaid = false;
          if (userState.userDataState.seminars.includes(item._id)) {
            console.log("morijoooon");
            isPaid = true;
          }
          return (
            <TouchableOpacity
              activeOpacity={0.9}
              style={Styles.Pack}
              onPress={() => {
                getSeminarItem(item._id, () => navigate("SeminarPack"));
              }}
            >
              <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: item.cover }} />
              <Icon
                name="bookmark"
                size={70}
                solid
                color={MAIN_COLOR}
                style={Styles.bookmarkImg}
              />
              <View style={Styles.PriseView}>
                <Text style={Styles.priseText} numberOfLines={1}>
                  {isPaid ? "خریداری" : new irAmount(parseInt(item.price)).digitGrouped()}
                </Text>
                <Text style={Styles.priseText}>{isPaid ? "شده" : TOMAN}</Text>
              </View>
              <View style={{ alignItems: "center" }}>
                <Text style={Styles.TextTitle}>{item.title}</Text>
                <Text style={Styles.TextDescription}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default SeminarList;
