import React, { useState, useEffect, useContext } from "react";
import { TouchableOpacity, View, Text, Image, FlatList, Linking, ScrollView,ToastAndroid } from "react-native";
import { Accordion } from "native-base";
import Styles from "./styles";

import Header from "../../../Components/Header";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import { Indicator } from "../../../Components/Indicator";
import Icon1 from "react-native-vector-icons/FontAwesome5";
import Icon from "react-native-vector-icons/AntDesign";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import AsyncStorage from "@react-native-community/async-storage";
import { PLEASE_PAY, IS_EXIST_TOAST } from "../../../common/Strings";
import { iranSans } from "../../../common/Theme";
import { Context as DataContext } from "../../../Context/DataContext";
import { mkDir } from "../../../HandlerFunc/mkDir";
import { navigate } from "../../../../navigationRef";
import { Context as UserContext } from "../../../Context/userDataContext";
import { onPayment } from "../../../Api/onPayment";
const SeminarScreen = (props) => {
  //=========================== CONTEXT  ====================
  const context = useContext(DataContext);
  const { state } = context;
  const { seminarSection } = state;
  const { seminars} = seminarSection;
  console.log({ Ali: state.seminarSection });
  const userContext = useContext(UserContext);
  const userState = userContext.state;
  //=========================== CONTEXT  ====================
  const [data, setData] = useState(seminars.sections);
  const [imageDownload, setImageDownload] = useState(false);
  const [isPaid, setIsPaid] = useState(false);
  const [payloadin, setPayLoadin] = useState(false);
  const dataArray = [{ title: "توضیحات دوره", content: seminars ? seminars.aboutSeminar : "" }];
  let IconDown = <Icon1 style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon1 style={Styles.Icon} name="chevron-left" color={"black"} />;
  useEffect(() => {
    if (seminars) {
      if (userState.userDataState.seminars.includes(seminars._id)) {
        setIsPaid(true);
      }
    }
  }, [seminars]);
  const onDownload = async (item) => {
   if (isPaid) {
    const resPermission = await mkDir();
    if (resPermission) {
      makeVideoDirectory();
      setImageDownload(true);
      videoDownload(item);
    } 
   }else {
    ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
  }
  };
  useEffect(() => {
    checkIfDownLoaded();
  }, []);
  useEffect(() => {
    if (imageDownload) {
      makeImageDirectory();
    }
  }, [imageDownload]);
  const makeImageDirectory = () => {

    const { config, fs } = RNFetchBlob;
    const imageUrl = seminars.cover.toString();
    const imageDirectoryPath = `/AndisheRoshan/Seminar/${seminars.title}`;
    const path = `/storage/emulated/0/${imageDirectoryPath}`;
    const imageFileDirectory = path + `/image.png`;
    RNFS.exists(imageFileDirectory)
      .then((exist) => {
        if (exist) {
          
        } else {
          let imageDownloadoption = {
            fileCache: false,
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: false,
              path: imageFileDirectory,
              description: "Downloading"
            }
          };
          config(imageDownloadoption)
            .fetch("GET", imageUrl)
            .then((res) => {
             
              const textObject = {
                title: seminars.title,
                desc: seminars.description,
                about: seminars.aboutSeminar
              };
              AsyncStorage.setItem(`${seminars.title}`, JSON.stringify(textObject))
                .then((res) => {
                  console.log({ res });
                })
                .catch((e) => console.log({ e }));
            })
            .catch((e) => {
              console.log({ e });
            });
        }
      })
      .catch((e) => {
        console.log({ e });
      });
  };
  const videoDownload = async (data) => {
    if (data) {
      const voiceDirectoryPath = `/AndisheRoshan/Seminar/${seminars.title}/Videoes`;
      const { config, fs } = RNFetchBlob;
      const url = data.video.toString();
      const voiceDirectory = `/storage/emulated/0/${voiceDirectoryPath}`;
      const voiceName = `${data.title}.mp4`;

      RNFS.exists(voiceDirectory + `/${voiceName}`)
        .then((exist) => {
          if (exist) {
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
          } else {
            let options = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: voiceDirectory + `/${voiceName}`,
                description: "Downloading"
              }
            };

            config(options)
              .fetch("GET", url)
              .then((res) => checkIfDownLoaded())
              .catch((r) => {});
          }
        })
        .catch((e) => {
          return Alert.alert("error in check in exist ");
        });
    }
  };
  const makeVideoDirectory = () => {
    const voiceDirectoryPath = `/AndisheRoshan/Seminar/${seminars.title}/Videoes`;
    const path = `/storage/emulated/0/${voiceDirectoryPath}`;
    RNFS.exists(path)
      .then((exist) => {
        if (exist) {
          console.log("videoExist");
        } else {
          RNFetchBlob.fs
            .mkdir(path)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  const checkIfDownLoaded = () => {
    const checkDownloaded = seminars.sections.map(async (item) => {
      const directoryPath = `/AndisheRoshan/Seminar/${seminars.title}/Videoes`;
      const Path = `/storage/emulated/0/${directoryPath}`;
      const itemName = `${item.title}.mp4`;

      return RNFS.exists(Path + `/${itemName}`).then((exist) => {
        let newItem = {};
        if (exist) {
          newItem = { ...item, downloaded: true };
        } else {
          Object.assign(newItem, item);
        }
        return newItem;
      });
    });

    Promise.all(checkDownloaded).then((res) => {
      setData(res);
    });
  };
  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };
  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text style={Styles.textCotent}>{item.content}</Text>
      </View>
    );
  };
  const checkPayment = () => {
    return (
      isPaid ? null : (
        <TouchableOpacity TouchableOpacity={0.5} style={Styles.buttonBank} onPress={onPayments}>
          {payloadin ? (
            <Indicator size={20} color={GOLDEN_COLOR} />
          ) : (
            <View style={Styles.viewPrice}>
              <Text style={Styles.priceBank}>{seminars.price}</Text>
              <Text style={Styles.priceBank}> تومان</Text>
            </View>
          )}
          <Text style={Styles.priceBank}>پرداخت</Text>
        </TouchableOpacity>
      )
    );
  };
  const onPressPlay = (item) => {
    if (isPaid) {
      navigate("SecVideo", { video: item ,type: 'seminar'});
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };

  const onPayments = async () => {
    setPayLoadin(true);
    const resPayment = await onPayment("Seminar", seminars._id);
    console.log({ resPayment });
    if (typeof resPayment === "string") {
      Linking.canOpenURL(resPayment).then((supported) => {
        if (supported) {
          setPayLoadin(false);
          console.log({ supported });
          Linking.openURL(resPayment);
        } else {
          setPayLoadin(false);
          console.log("Don't know how to open URI: " + resPayment);
        }
      });
    }
  };
  const videoList = () => {
    return (
      <FlatList
        
        showsHorizontalScrollIndicator={false}
        keyExtractor={(index) => index._id}
        data={data}
        renderItem={({ item }) => {
          return (
            <View style={Styles.container}>
              <View style={Styles.view}>
                <Icon
                  name="playcircleo"
                  color={MAIN_COLOR}
                  size={40}
                  style={Styles.playIcon}
                  onPress={() => onPressPlay(item)}
                />
                <View style={Styles.TextView}>
                  <Text style={Styles.Title} numberOfLines={1}>{item.title}</Text>
                  <Text style={[iranSans]} numberOfLines={1}>{item.description}</Text>
                </View>
              </View>
              <Icon
                name="download"
                color={item.downloaded ? "#eee" : MAIN_COLOR}
                size={25}
                style={Styles.downloadIcon}
                onPress={() => onDownload(item)}
              />
            </View>
          );
        }}
      />
    );
  };
  return (
    <View style={Styles.Container}>
      <ScrollView>
      <View style={Styles.TopView}>
        <Header navigation={props.navigation} title={seminars.title} />
        <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: seminars.cover }} />
        {checkPayment()}
        <Accordion
          style={Styles.Accordion}
          headerStyle={Styles.Header}
          dataArray={dataArray}
          renderHeader={header}
          renderContent={renderContent}
        />
      </View>

      {videoList()}

      </ScrollView>
    </View>
  );
};

export default SeminarScreen;
