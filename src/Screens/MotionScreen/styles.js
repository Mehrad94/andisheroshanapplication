import {StyleSheet,Dimensions} from 'react-native';
import {
  mH16,
  mV8,

  r8,
} from '../../common/Theme';
const {height, width} = Dimensions.get('screen');
const Styles = StyleSheet.create({
  Container: {
    
  },
 
  ViewTouch: {
    ...mV8,
    ...mH16,
  },
  Img: {
  width: '100%',
  height:width/3.5,
  
    ...r8,
  },

});
export default Styles;
