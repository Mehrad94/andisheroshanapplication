import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, Image, ToastAndroid } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";
import Styles from "./style";
import { MAIN_COLOR } from "../../../common/Colors";
import { posAbs, mH16, mV8, iranSans, Flex, centerAll, bgMainColor } from "../../../common/Theme";
import {PLEASE_PAY} from '../../../common/Strings'
import { Context as DataContext } from "../../../Context/DataContext";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
import TrackPlayer from "react-native-track-player";
const VoiseTab = ({ data, onvoiceMakeDirectory, isPaid }) => {
  console.log({ data });

  console.log("VOICETAB");
  //========================== CONTEXT====================
  const context = useContext(DataContext);
  const { state, musicPlayerAction, changeCurrentMusic, setCurrentMusicNull } = context;
  //========================== CONTEXT====================

  const onVoiceIconPress = async (item) => {
    if (isPaid) {
      if (state.currentMusic === item) {
        return;
      } else {
        await TrackPlayer.reset();
        setCurrentMusicNull();
        changeCurrentMusic(item);
        musicPlayerAction(true);
      }
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }

    // navigate("Voice");
  };
  const onDownload = (itemUrl, data) => {
    if (isPaid) {
      const type = "download";
      changeCurrentMusic(itemUrl, type);
      onvoiceMakeDirectory(data);
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };
  if (data) {
    if (data.length < 1) {
      return <RenderEmptyList />;
    }
  }
  return (
    <FlatList
      style={Styles.flat}
      showsHorizontalScrollIndicator={false}
      keyExtractor={(index) => index._id}
      data={data}
      renderItem={({ item }) => {
        return (
          <View style={Styles.Container}>
            <View style={Styles.view}>
              <Icon
                name="playcircleo"
                color={MAIN_COLOR}
                size={40}
                style={Styles.playIcon}
                onPress={() => onVoiceIconPress(item)}
              />
              <View style={Styles.TextView}>
                <Text style={Styles.Title} numberOfLines={1}>{item.title}</Text>
                <Text style={[iranSans]} numberOfLines={1}>{item.description}</Text>
              </View>
            </View>
            <Icon
              name="download"
              color={item.downloaded ? "#eee" : MAIN_COLOR}
              size={25}
              style={Styles.downloadIcon}
              onPress={() => onDownload(item.voice, item)}
            />
          </View>
        );
      }}
    />
  );
};

export default VoiseTab;
