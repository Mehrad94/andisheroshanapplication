import { StyleSheet, Dimensions } from "react-native";
import { MAIN_COLOR } from "../../../common/Colors";
const { width } = Dimensions.get("screen");
const Styles = StyleSheet.create({
  Container: {
    backgroundColor: "white",
    flexDirection: "row-reverse",
    borderBottomWidth: 2,
    marginHorizontal: 5,
    borderColor: MAIN_COLOR,
    borderRadius: 5,
    justifyContent: "space-between"
  },
  TextView: {
    justifyContent: "center",
    marginRight: 10,
    width:width/1.5
  },
  Title: {
    fontFamily: "iran_sans",
    fontSize: 18
  },
  playIcon: {
    // justifyContent:'center',
    marginRight: 10,
    alignSelf: "center"
  },
  downloadIcon: {
    alignSelf: "center",
    marginLeft: 5
  },
  view: {
    flexDirection: "row-reverse"
  },
  flat: {
    marginBottom: width / 10
  }
});
export default Styles;
