import React, { useState, useEffect, useContext } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  Alert,
  ToastAndroid,
  ScrollView,
  Linking,
} from "react-native";
import { Accordion } from "native-base";
import Styles from "./style";
import VideoTab from "../VideoTab";
import VoiseTab from "../VoiseTab";
import Header from "sample/src/Components/Header";
import { MAIN_COLOR } from "sample/src/common/Colors";
import { Indicator } from "../../../Components/Indicator";
import Icon from "react-native-vector-icons/FontAwesome5";
import RNFS from "react-native-fs";
import { Context as DataContext } from "../../../Context/DataContext";
import RNFetchBlob from "rn-fetch-blob";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import AsyncStorage from "@react-native-community/async-storage";
import { TAB_VOICE, RIGHT, TAB_VIDEO, LEFT, IS_EXIST_TOAST } from "../../../common/Strings";
import FloatinMusicPlayer from "../../../Components/FloatinMusicPlaye";
import { screenWidth } from "../../../common/Constants";
import { Context as UserContext } from "../../../Context/userDataContext";
import { onPayment } from "../../../Api/onPayment";
import { GOLDEN_COLOR } from "../../../common/Colors";
const PackScreen = (props) => {
  //===================CONTEXT=================
  const context = useContext(DataContext);
  const userContext = useContext(UserContext);
  const { state, downloadEnd } = context;
  const userState = userContext.state;
  //===================CONTEXT=================
  const [underView, setUnderView] = useState(<VideoTab data={null} />);
  const [active, setActive] = useState("left");
  const [parameter, setParameter] = useState(null);
  const [sectionData, setSectionData] = useState(null);
  const [imageDownload, setImageDownload] = useState(false);
  const [isPaid, setIsPaid] = useState(false);
  const [payloadin, setPayLoadin] = useState(false);
  console.log({ isPaid });
  console.log({ ALI: parameter });

  useEffect(() => {
    let params = null;
    console.log({ LL: props.navigation });

    if (props.navigation.state.params.course) {
      params = props.navigation.state.params.course;
      setParameter(params);
    }

    if (parameter) {
      return onTabPressed(LEFT);
    }
  }, [props, parameter, isPaid]);
  useEffect(() => {
    console.log({ KK: props.navigation });

    if (parameter) {
      if (userState.userDataState) {
        if (userState.userDataState.courses.includes(parameter._id)) {
          setIsPaid(true);
        }
      }
    }
  }, [parameter]);

  const onTabPressed = (direction) => {
    if (direction === "left") {
      setActive(direction);
      const videos = parameter.sections.filter((item) => {
        return item.type === "VIDEO";
      });
      const checkDownloaded = videos.map(async (item) => {
        const directoryPath = `/AndisheRoshan/Courses/${parameter.title}/Videos`;
        const Path = `/storage/emulated/0/${directoryPath}`;
        const itemName = `${item.title}.mp4`;

        return RNFS.exists(Path + `/${itemName}`).then((exist) => {
          let newItem = {};
          if (exist) {
            newItem = { ...item, downloaded: true };
          } else {
            Object.assign(newItem, item);
          }
          return newItem;
        });
      });
      Promise.all(checkDownloaded).then((res) => {
        setUnderView(
          <VideoTab
            isPaid={isPaid}
            data={res}
            onVideoMakeDirectory={videoDownload}
            idCourse={parameter._id}
            type={"course"}
          />
        );
      });
    }
    if (direction === "right") {
      setActive(direction);
      const voices = parameter.sections.filter((item) => {
        return item.type === "VOICE";
      });
      //=============================== CHECK ITEMS IF DOWLOADED BEFORE ========================

      const checkDownloaded = voices.map(async (item) => {
        const directoryPath = `/AndisheRoshan/Courses/${parameter.title}/Voices`;
        const Path = `/storage/emulated/0/${directoryPath}`;
        const itemName = `${item.title}.mp3`;

        return RNFS.exists(Path + `/${itemName}`).then((exist) => {
          let newItem = {};
          if (exist) {
            newItem = { ...item, downloaded: true };
          } else {
            Object.assign(newItem, item);
          }
          return newItem;
        });
      });

      Promise.all(checkDownloaded).then((res) => {
        console.log({ res });
        setUnderView(<VoiseTab isPaid={isPaid} data={res} onvoiceMakeDirectory={voiceDownload} />);
      });
    }
  };
  const dataArray = [{ title: "توضیحات دوره", content: parameter ? parameter.aboutCourse : "" }];

  //=============================== CHECK ITEMS IF DOWLOADED BEFORE ========================
  const checkExistItemsInStorage = () => {};
  //=================================DOWNLOAD =============================
  const mkDir = (type) => {
    check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            break;
          case RESULTS.DENIED:
            request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
              if (result === "granted") {
                makeVideoDirectory();
                makeVoiceDirectory();
                setImageDownload(true);

                if (type === "video") {
                  videoDownload();
                }
                if (type === "voice") {
                  voiceDownload();
                }
              }
            });
            break;
          case RESULTS.GRANTED:
            makeVideoDirectory();
            makeVoiceDirectory();
            setImageDownload(true);

            if (type === "video") {
              videoDownload();
            }
            if (type === "voice") {
              voiceDownload();
            }
            break;
          case RESULTS.BLOCKED:
            break;
        }
      })
      .catch((error) => {
        // …
      });
  };
  useEffect(() => {
    mkDir("voice");
  }, [state.voiceUrl]);
  useEffect(() => {
    mkDir("video");
  }, [state.videoUrl]);
  useEffect(() => {
    mkDir();
  }, []);
  useEffect(() => {
    if (imageDownload) {
      makeImageDirectory();
    }
  }, [imageDownload]);
  const makeImageDirectory = () => {
    console.log("image");

    const { config, fs } = RNFetchBlob;
    const imageUrl = parameter.cover.toString();
    const imageDirectoryPath = `/AndisheRoshan/Courses/${parameter.title}`;
    const path = `/storage/emulated/0/${imageDirectoryPath}`;
    const imageFileDirectory = path + `/image.png`;
    RNFS.exists(imageFileDirectory)
      .then((exist) => {
        if (exist) {
        } else {
          let imageDownloadoption = {
            fileCache: false,
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: false,
              path: imageFileDirectory,
              description: "Downloading",
            },
          };
          config(imageDownloadoption)
            .fetch("GET", imageUrl)
            .then((res) => {
              console.log(res);
              const textObject = {
                title: parameter.title,
                desc: parameter.description,
                about: parameter.aboutCourse,
              };
              AsyncStorage.setItem(`${parameter.title}`, JSON.stringify(textObject))
                .then((res) => {
                  console.log({ res });
                })
                .catch((e) => console.log({ e }));
            })
            .catch((e) => {
              console.log({ e });
            });
        }
      })
      .catch((e) => {
        console.log({ e });
      });
  };
  const makeVideoDirectory = () => {
    const videoDirectoryPath = `/AndisheRoshan/Courses/${parameter.title}/Videos`;
    const path = `/storage/emulated/0/${videoDirectoryPath}`;
    RNFS.exists(path)
      .then((exist) => {
        if (exist) {
          console.log("videoExist");
        } else {
          RNFetchBlob.fs
            .mkdir(path)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  const makeVoiceDirectory = () => {
    const voiceDirectoryPath = `/AndisheRoshan/Courses/${parameter.title}/Voices`;
    const path = `/storage/emulated/0/${voiceDirectoryPath}`;
    RNFS.exists(path)
      .then((exist) => {
        if (exist) {
          console.log("voiceExist");
        } else {
          RNFetchBlob.fs
            .mkdir(path)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  console.log({ videoUrlbala: state.videoUrl });

  const videoDownload = async (data) => {
    if (data) setSectionData(data);
    console.log({ sectionData });

    if (state.videoUrl) {
      const videoDirectoryPath = `/AndisheRoshan/Courses/${parameter.title}/Videos`;
      const { config, fs } = RNFetchBlob;
      const url = state.videoUrl.toString();
      console.log({ videoUrl: url });

      const videoDirectory = `/storage/emulated/0/${videoDirectoryPath}`;
      const videoName = `${sectionData.title}.mp4`;
      console.log({ videoName });

      RNFS.exists(videoDirectory + `/${videoName}`)
        .then((exist) => {
          if (exist) {
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
            downloadEnd("video");
          } else {
            let options = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: videoDirectory + `/${videoName}`,
                description: "Downloading",
              },
            };

            config(options)
              .fetch("GET", url)
              .then((res) => downloadEnd("video"))
              .catch((r) => {
                Alert.alert("erroe in download");
                downloadEnd("video");
              });
          }
        })
        .catch((e) => {
          return Alert.alert("error in check in exist ");
        });
    }
  };
  const voiceDownload = async (data) => {
    if (data) setSectionData(data);
    if (state.voiceUrl) {
      const voiceDirectoryPath = `/AndisheRoshan/Courses/${parameter.title}/Voices`;
      const { config, fs } = RNFetchBlob;
      const url = state.voiceUrl.toString();
      const voiceDirectory = `/storage/emulated/0/${voiceDirectoryPath}`;
      const voiceName = `${sectionData.title}.mp3`;

      RNFS.exists(voiceDirectory + `/${voiceName}`)
        .then((exist) => {
          if (exist) {
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
            downloadEnd("voice");
          } else {
            let options = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: voiceDirectory + `/${voiceName}`,
                description: "Downloading",
              },
            };

            config(options)
              .fetch("GET", url)
              .then((res) => downloadEnd("voice"))
              .catch((r) => {
                downloadEnd("voice");
              });
          }
        })
        .catch((e) => {
          return Alert.alert("error in check in exist ");
        });
    }
  };

  //=================================DOWNLOAD =============================

  let IconDown = <Icon style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon style={Styles.Icon} name="chevron-left" color={"black"} />;

  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };

  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text style={Styles.textCotent}>{item.content}</Text>
      </View>
    );
  };
  const onPayments = async () => {
    setPayLoadin(true);
    const resPayment = await onPayment("Course", parameter._id);
    console.log({ resPayment });
    if (typeof resPayment === "string") {
      Linking.canOpenURL(resPayment).then((supported) => {
        if (supported) {
          setPayLoadin(false);
          console.log({ supported });
          Linking.openURL(resPayment);
        } else {
          setPayLoadin(false);
          console.log("Don't know how to open URI: " + resPayment);
        }
      });
    }
  };
  if (!parameter) {
    return <Indicator />;
  }

  return (
    <View style={Styles.Container}>
      <ScrollView>
        <View style={Styles.TopView}>
          <Header navigation={props.navigation} title={parameter.title} />
          <Image resizeMode= 'stretch' style={Styles.img} source={{ uri: parameter.cover }} />

          {isPaid ? null : (
            <TouchableOpacity TouchableOpacity={0.5} style={Styles.buttonBank} onPress={onPayments}>
              {payloadin ? (
                <Indicator size={20} color={GOLDEN_COLOR} />
              ) : (
                <View style={Styles.viewPrice}>
                  <Text style={Styles.priceBank}>{parameter.price}</Text>
                  <Text style={Styles.priceBank}> تومان</Text>
                </View>
              )}
              <Text style={Styles.priceBank}>پرداخت</Text>
            </TouchableOpacity>
          )}
          <Accordion
            style={Styles.Accordion}
            headerStyle={Styles.Header}
            dataArray={dataArray}
            renderHeader={header}
            renderContent={renderContent}
          />
        </View>
        <View style={Styles.TabsHolder}>
          <TouchableOpacity
            style={[Styles.TabLeft, active === LEFT ? { backgroundColor: MAIN_COLOR } : null]}
            onPress={() => onTabPressed(LEFT)}
          >
            <Text style={Styles.TabsText}>{TAB_VIDEO}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => onTabPressed(RIGHT)}
            style={[Styles.TabRight, active === RIGHT ? { backgroundColor: MAIN_COLOR } : null]}
          >
            <Text style={Styles.TabsText}>{TAB_VOICE}</Text>
          </TouchableOpacity>
        </View>
        <View>{underView}</View>
      </ScrollView>
      <View
        style={{
          position: "absolute",
          bottom: 0,
          width: screenWidth,
        }}
      >
        <FloatinMusicPlayer type={"course"} data={parameter} />
      </View>
    </View>
  );
};

export default PackScreen;
