import React, { useState, useEffect, useContext } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  ScrollView,
  BackHandler,
} from "react-native";
import Video, { OnLoadData } from "react-native-video";
import Orientation from "react-native-orientation-locker";

import PlayerControls from "../../../Components/VideoPlayers/Controls/PlayerControls";
import ProgressBar from "../../../Components/VideoPlayers/Controls/ProgressBar";
import Icon from "react-native-vector-icons/AntDesign";
import styles from "./styles";
import { Indicator } from "../../../Components/Indicator";
import { Context as DataContext } from "../../../Context/DataContext";
import { postFinishSec } from "../.././../Api/postFinishSec";
const LIKE_TEXT = "پسندیدم";
const VideoSection = ({ navigation }) => {
  //==============================CONTEXT =========================
  const context = useContext(DataContext);
  const { state, likeMechanism } = context;
  // console.log({ state });

  //==============================CONTEXT =========================
  const videoRef = React.createRef();

  const [fullScreen, setFullScreen] = useState(false);
  const [play, setPlay] = useState(true);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [showControls, setShowControls] = useState(true);
  const [parameter, setParameter] = useState(null);
  const [liked, setLiked] = useState(false);
  const [isFinisheDsec, setIsFinishedSec] = useState(false);
  const [dataParam, setDataParam] = useState({});
  console.log({ isFinisheDsec });
  useEffect(() => {
    navigation.addListener("didBlur", (path) => {
      if (path.type === "didBlur") {
        setIsFinishedSec(false);
      }
    });
    navigation.addListener("didFocus", (path) => {
      if (path.type === "didFocus") {
        setIsFinishedSec(false);
      }
    });
    const params = navigation.state.params.blog;
    if (params) {
      setParameter(params);
    }
  }, [navigation]);
  useEffect(() => {
    getSectionDone();
  }, [currentTime]);
  useEffect(() => {
    if (isFinisheDsec) {
      sendFinishedData();
    }
  }, [isFinisheDsec]);
  const sendFinishedData = async () => {
    if (dataParam.type === "course") {
      const resFinished = await postFinishSec(dataParam.type, dataParam.courseId, parameter._id);
      console.log({ resFinished });
    }
    if (dataParam.type === "seminar") {
      const courseId = null;
      const resFinishedSem = await postFinishSec(dataParam.type, courseId, parameter._id);
      console.log({ resFinishedSem });
    }
  };
  const getSectionDone = async () => {
    if (currentTime > 5) {
      if (currentTime > duration - 1) {
        setIsFinishedSec(true);
      }
    }
  };
  useEffect(() => {
    console.log({ OOOOO: navigation.state.params });

    const params = navigation.state.params.video;
    const courseId = navigation.state.params.idCourse;
    const type = navigation.state.params.type;

    const introParam = navigation.state.params.introVid;
    if (params) {
      setParameter(params);
      setDataParam({ courseId, type });
    }
    if (introParam) {
      setParameter(introParam);
    }
  }, [navigation]);
  console.log({ dataParam });

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backButtonHandler);

    return () => {
      BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
    };
  }, [fullScreen]);
  console.log({ parameter });

  const backButtonHandler = () => {
    if (fullScreen) {
      return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);

    return () => {
      Orientation.removeOrientationListener(handleOrientation);
    };
  }, []);

  const handleOrientation = (orientation) => {
    orientation === "LANDSCAPE-LEFT" || orientation === "LANDSCAPE-RIGHT"
      ? (setFullScreen(true),
        StatusBar.setHidden(true),
        setTimeout(() => setShowControls(false), 500))
      : (setFullScreen(false),
        StatusBar.setHidden(false),
        setTimeout(() => setShowControls(false), 500));
  };

  const handleFullscreen = () => {
    fullScreen ? Orientation.unlockAllOrientations() : Orientation.lockToLandscapeLeft();
  };

  const handlePlayPause = () => {
    // If playing, pause and show controls immediately.
    if (play) {
      setPlay(false);
      //   setShowControls(true);
      return;
    }

    setPlay(true);
    setTimeout(() => setShowControls(false), 500);
  };

  const skipBackward = () => {
    videoRef.current.seek(currentTime - 15);
    setCurrentTime(currentTime - 15);
  };

  const skipForward = () => {
    videoRef.current.seek(currentTime + 15);
    setCurrentTime(currentTime + 15);
  };

  const onSeek = (data) => {
    videoRef.current.seek(data.seekTime);
    setCurrentTime(data.seekTime);
  };

  const onLoadEnd = (data) => {
    console.log("LOADDDDDDDDDDDD");

    setDuration(data.duration);
    setCurrentTime(data.currentTime);
  };

  const onProgress = (data) => {
    console.log("jhhjhjk");

    setCurrentTime(data.currentTime);
  };
  const onEnd = () => {
    setPlay(false);
    videoRef.current.seek(0);
  };

  const showControlsF = () => {
    setShowControls(!showControls);
  };
  const onLikePress = () => {
    likeMechanism(parameter._id);
  };
  if (!parameter) {
    return <Indicator />;
  }
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={showControlsF}>
        <View>
          <Video
            poster={parameter.cover}
            posterResizeMode={"stretch"}
            ref={videoRef}
            source={{
              uri: parameter.video,
            }}
            style={fullScreen ? styles.fullscreenVideo : styles.video}
            controls={false}
            resizeMode={"contain"}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            onEnd={onEnd}
            paused={!play}
          />
          {showControls && (
            <View style={styles.controlOverlay}>
              <TouchableOpacity
                onPress={handleFullscreen}
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                style={styles.fullscreenButton}
              >
                {fullScreen ? (
                  <Icon name="shrink" size={25} color="red" />
                ) : (
                  <Icon name="arrowsalt" color="red" size={25} />
                )}
              </TouchableOpacity>
              <PlayerControls
                onPlay={handlePlayPause}
                onPause={handlePlayPause}
                playing={play}
                showPreviousAndNext={false}
                showSkip={true}
                skipBackwards={skipBackward}
                skipForwards={skipForward}
              />
              <ProgressBar
                currentTime={currentTime}
                duration={duration > 0 ? duration : 0}
                onSlideStart={handlePlayPause}
                onSlideComplete={handlePlayPause}
                onSlideCapture={onSeek}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
      <ScrollView style={styles.scroll}>
        <View style={styles.likeHolder}>
          <Icon
            name="heart"
            color="red"
            size={40}
            solid={liked}
            style={{ marginRight: 30 }}
            onPress={onLikePress}
          />
          <View>
            <Text style={styles.title}>{parameter.title}</Text>
            <Text style={styles.date}>{parameter.createdTime}</Text>
          </View>
        </View>
        <Text style={styles.text}>{parameter.aboutBlog}</Text>
      </ScrollView>
    </View>
  );
};

export default VideoSection;
