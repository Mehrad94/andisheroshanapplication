import React, { useContext, useEffect } from "react";
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import { Context as UserContext } from "../../Context/userDataContext";
import Styles from "./styles";
import Header from "../../Components/Header";
const MotionScreen = ({ navigation }) => {
  const context = useContext(UserContext);
  const { state, getFreshUserData } = context;
  useEffect(() => {
    getFreshUserData();
  }, []);

  const Category = [
    {
      img: require("../../Assets/Png/1.jpeg"),

      route: "Lists",
    },
    {
      img: require("../../Assets/Png/2.jpeg"),
      title: "کتاب های صوتی",
      route: "AudioBookList",
    },
    {
      img: require("../../Assets/Png/3.jpeg"),
      title: "سمینار ها",
      route: "SeminarList",
    },
    {
      img: require("../../Assets/Png/4.jpeg"),
      title: "آنالیز مشاغل",
      route: "IntroducingJobsList",
    },
  ];

  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} />
      <FlatList
        keyExtractor={(CategoryList) => CategoryList.img}
        data={Category}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={Styles.ViewTouch}
              onPress={() => navigation.navigate(item.route)}
            >
              <View>
                <Image style={Styles.Img} resizeMode="stretch" source={item.img} />
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default MotionScreen;
