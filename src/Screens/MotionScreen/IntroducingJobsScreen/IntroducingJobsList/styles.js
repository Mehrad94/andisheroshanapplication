import { StyleSheet,Dimensions } from "react-native";
import { mH16, mV8, posAbs, iranSans, mV16, padV64, r8 } from "../../../../common/Theme";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../../common/Colors";
const { height, width } = Dimensions.get("screen");

const Styles = StyleSheet.create({
  Container: {
    flex: 1
  },
  Text: {
    fontSize: 25,
    fontFamily: "iran_sans",
    marginRight: 25,
    marginVertical: 10
  },
  Pack: {
    backgroundColor: "white",
    borderRadius: 5,
    width: "45%",
    marginTop: 10,
    marginHorizontal: 9,
    borderWidth: 2,
    borderColor: MAIN_COLOR
  },
  img: {
    margin: 5,
    height:width/3.5
  },
  TextTitle: {
    fontFamily: "iran_sans",
    fontSize: 15,
    borderTopWidth: 2,
    width: "90%",
    textAlign: "center",
    borderColor: "#e8e8e8",
    color: "#d4af37"
  },
  TextDescription: {
    ...iranSans,
    padding: 5,
    fontSize: 10,
    flexDirection: "column",
    textAlign: "center"
  },

  
});
export default Styles;
