import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Styles from "./styles";
import Header from "../../../../Components/Header";
import { MAIN_COLOR } from "../../../../common/Colors";
import { Context as DataContext } from "../../../../Context/DataContext";
import { JOBS_TITLE } from "../../../../common/Strings";
import { TOMAN } from "../../../../common/Strings";
import { navigate } from "../../../../../navigationRef";
const irAmount = require("iramount");

const IntroducingJobsList = ({ navigation }) => {
  //===========================CONTEXT =============================
  const context = useContext(DataContext);
  const { state, getIntroducingJobItem } = context;
  console.log({ state });

  //===========================CONTEXT =============================

  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} title={JOBS_TITLE} />
      <FlatList
        numColumns={2}
        keyExtractor={(CategoryList, index) => Math.random(index) * 1.62}
        data={state.introducingjob}
        renderItem={({ item }) => {
          console.log({ IIII: item });

          return (
            <TouchableOpacity
              activeOpacity={0.9}
              style={Styles.Pack}
              onPress={() => {
                navigate("IntroducingJobsPack", { param: item });
              }}
            >
              <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: item.cover }} />
           
            
              <View style={{ alignItems: "center" }}>
                <Text style={Styles.TextTitle}>{item.title}</Text>
                <Text style={Styles.TextDescription}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default IntroducingJobsList;
