import React, { useState, useEffect, useContext } from "react";
import { TouchableOpacity, View, Text, Image, FlatList, ScrollView } from "react-native";
import { Accordion } from "native-base";
import Styles from "./styles";

import Header from "../../../Components/Header";
import { MAIN_COLOR } from "../../../common/Colors";
import { Indicator } from "../../../Components/Indicator";
import Icon1 from "react-native-vector-icons/FontAwesome5";
import Icon from "react-native-vector-icons/AntDesign";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import AsyncStorage from "@react-native-community/async-storage";
import { TAB_VOICE, RIGHT, TAB_VIDEO, LEFT, IS_EXIST_TOAST } from "../../../common/Strings";
import FloatinMusicPlayer from "../../../Components/FloatinMusicPlaye";
import { iranSans } from "../../../common/Theme";
import VoiseTab from "../VoiseTab";
import { Context as DataContext } from "../../../Context/DataContext";
import { Flex } from "../../../common/Theme";
import { screenWidth } from "../../../common/Constants";
import { mkDir } from "../../../HandlerFunc/mkDir";
import { navigate } from "../../../../navigationRef";
const IntroducingJobsScreen = (props) => {
  //=========================== CONTEXT  ====================

  //=========================== CONTEXT  ====================
  const [imageDownload, setImageDownload] = useState(false);
  let Data = props.navigation.state.params.param;
  const [data, setData] = useState(Data.sections);
  const dataArray = [{ title: "توضیحات دوره", content: Data ? Data.title : "" }];
  let IconDown = <Icon1 style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon1 style={Styles.Icon} name="chevron-left" color={"black"} />;

  const onDownload = async (item) => {
    console.log({ item });

    const resPermission = await mkDir();
    console.log({ resPermission });

    if (resPermission) {
      makeVideoDirectory();
      setImageDownload(true);
      videoDownload(item);
    }
  };
  useEffect(() => {
    checkIfDownLoaded();
  }, []);
  useEffect(() => {
    if (imageDownload) {
      makeImageDirectory();
    }
  }, [imageDownload]);
  const makeImageDirectory = () => {
    console.log("image");

    const { config, fs } = RNFetchBlob;
    const imageUrl = Data.cover.toString();
    const imageDirectoryPath = `/AndisheRoshan/IntroducingJobs/${Data.title}`;
    const path = `/storage/emulated/0/${imageDirectoryPath}`;
    const imageFileDirectory = path + `/image.png`;
    RNFS.exists(imageFileDirectory)
      .then((exist) => {
        if (exist) {
          console.log("image exist");
        } else {
          let imageDownloadoption = {
            fileCache: false,
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: false,
              path: imageFileDirectory,
              description: "Downloading"
            }
          };
          config(imageDownloadoption)
            .fetch("GET", imageUrl)
            .then((res) => {
              console.log(res);
              const textObject = {
                title: Data.title,
                desc: Data.description,
                about: Data.aboutJob
              };
              AsyncStorage.setItem(`${Data.title}`, JSON.stringify(textObject))
                .then((res) => {
                  console.log({ res });
                })
                .catch((e) => console.log({ e }));
            })
            .catch((e) => {
              console.log({ e });
            });
        }
      })
      .catch((e) => {
        console.log({ e });
      });
  };
  const videoDownload = async (data) => {
    if (data) {
      const voiceDirectoryPath = `/AndisheRoshan/IntroducingJobs/${Data.title}/Videoes`;
      const { config, fs } = RNFetchBlob;
      const url = data.video.toString();
      const voiceDirectory = `/storage/emulated/0/${voiceDirectoryPath}`;
      const voiceName = `${data.title}.mp4`;

      RNFS.exists(voiceDirectory + `/${voiceName}`)
        .then((exist) => {
          if (exist) {
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
          } else {
            let options = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: voiceDirectory + `/${voiceName}`,
                description: "Downloading"
              }
            };

            config(options)
              .fetch("GET", url)
              .then((res) => checkIfDownLoaded())
              .catch((r) => {});
          }
        })
        .catch((e) => {
          return Alert.alert("error in check in exist ");
        });
    }
  };
  const makeVideoDirectory = () => {
    const voiceDirectoryPath = `/AndisheRoshan/IntroducingJobs/${Data.title}/Videoes`;
    const path = `/storage/emulated/0/${voiceDirectoryPath}`;
    RNFS.exists(path)
      .then((exist) => {
        if (exist) {
          console.log("videoExist");
        } else {
          RNFetchBlob.fs
            .mkdir(path)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  const checkIfDownLoaded = () => {
    const checkDownloaded = Data.sections.map(async (item) => {
      const directoryPath = `/AndisheRoshan/IntroducingJobs/${Data.title}/Videoes`;
      const Path = `/storage/emulated/0/${directoryPath}`;
      const itemName = `${item.title}.mp4`;

      return RNFS.exists(Path + `/${itemName}`).then((exist) => {
        let newItem = {};
        if (exist) {
          newItem = { ...item, downloaded: true };
        } else {
          Object.assign(newItem, item);
        }
        return newItem;
      });
    });

    Promise.all(checkDownloaded).then((res) => {
      setData(res);
    });
  };

  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };
  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text style={Styles.textCotent}>{item.content}</Text>
      </View>
    );
  };

  const videoList = () => {
    return (
      <FlatList
       
        showsHorizontalScrollIndicator={false}
        keyExtractor={(index) => index._id}
        data={data}
        renderItem={({ item }) => {
          return (
            <View style={Styles.container}>
              <View style={Styles.view}>
                <Icon
                  name="playcircleo"
                  color={MAIN_COLOR}
                  size={40}
                  style={Styles.playIcon}
                  onPress={() => navigate("SecVideo", { introVid: item })}
                />
                <View style={Styles.TextView}>
                  <Text style={Styles.Title} numberOfLines={1}>{item.title}</Text>
                  <Text style={[iranSans]} numberOfLines={1}>{item.description}</Text>
                </View>
              </View>
              <Icon
                name="download"
                color={item.downloaded ? "#eee" : MAIN_COLOR}
                size={25}
                style={Styles.downloadIcon}
                onPress={() => onDownload(item)}
              />
            </View>
          );
        }}
      />
    );
  };
  return (
    <View style={Styles.Container}>
       <ScrollView>
      <View style={Styles.TopView}>
        <Header navigation={props.navigation} title={Data.title} />
        <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: Data.cover }} />
        <Accordion
          style={Styles.Accordion}
          headerStyle={Styles.Header}
          dataArray={dataArray}
          renderHeader={header}
          renderContent={renderContent}
        />
      </View>
      {videoList()}
      </ScrollView>
    </View>
  );
};

export default IntroducingJobsScreen;
