import { StyleSheet } from "react-native";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import { iranSans } from "../../../common/Theme";

const Styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bottom: {
    backgroundColor: MAIN_COLOR,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center"
  },
  textBottom: {
    ...iranSans,
    color: GOLDEN_COLOR,
    fontSize: 20
  }
});

export default Styles;
