import React, { useState, useEffect } from "react";
import { TouchableOpacity, View, Text, Image } from "react-native";
import Styles from "./styles";

import Header from "../../../Components/Header";

const unSoldPack = ({ navigation }) => {
  const Pack = {
    title: "دوره اول",
    img: "https://picsum.photos/200/300.jpg"
  };

  return (
    <View style={Styles.container}>
      <View style={Styles.TopView}>
        <Header navigation={navigation} title={Pack.title} />
        <Image style={Styles.img} source={{ uri: Pack.img }} />
        <TouchableOpacity style={Styles.bottom} onPress={() => navigation.navigate("Pack")}>
          <Text style={Styles.textBottom}> پرداخت کنید</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default unSoldPack;
