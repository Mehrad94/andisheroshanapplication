import { StyleSheet,Dimensions } from "react-native";
import { mH16, mV8, posAbs, iranSans,} from "../../../../common/Theme";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../../common/Colors";
const { height, width } = Dimensions.get("screen");

const Styles = StyleSheet.create({
  Container: {
    flex: 1
  },
  Text: {
    fontSize: 25,
    fontFamily: "iran_sans",
    marginRight: 25,
    marginVertical: 10
  },
  Pack: {
    backgroundColor: "white",
    borderRadius: 5,
    width: "45%",
    marginTop: 40,
    marginHorizontal: 9,
    borderWidth: 2,
    borderColor: MAIN_COLOR
  },
  img: {
    marginTop:15,
    marginBottom:10,
    margin: 5,
    height:width/3.5
  },
  TextTitle: {
    fontFamily: "iran_sans",
    fontSize: 15,
    borderTopWidth: 2,
    width: "90%",
    textAlign: "center",
    borderColor: "#e8e8e8",
    color: "#d4af37"
  },
  TextDescription: {
    ...iranSans,
    padding: 5,
    fontSize: 10,
    flexDirection: "column",
    textAlign: "center"
  },
  bookmarkImg: {
    ...posAbs,
    marginLeft: 5,
    top: -30
  },
  PriseView: {
    ...posAbs,
    width: "45%",
    top: -20
  },
  priseText: {
    ...iranSans,
    fontSize: 11,
    textAlign: "center",
    marginRight:10,
    color: GOLDEN_COLOR
  }
});
export default Styles;
