import React, { useState, useEffect, useContext } from "react";
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  FlatList,
  ToastAndroid,
  Alert,
  ScrollView,
  Linking,
} from "react-native";
import { Accordion } from "native-base";
import Styles from "./styles";

import Header from "../../../Components/Header";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import { Indicator } from "../../../Components/Indicator";
import Icon1 from "react-native-vector-icons/FontAwesome5";
import Icon from "react-native-vector-icons/AntDesign";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";
import AsyncStorage from "@react-native-community/async-storage";
import { PLEASE_PAY, IS_EXIST_TOAST } from "../../../common/Strings";
import FloatinMusicPlayer from "../../../Components/FloatinMusicPlaye";
import { iranSans } from "../../../common/Theme";
import VoiseTab from "../VoiseTab";
import { Context as DataContext } from "../../../Context/DataContext";
import { Flex } from "../../../common/Theme";
import { screenWidth } from "../../../common/Constants";
import { mkDir } from "../../../HandlerFunc/mkDir";
import TrackPlayer from "react-native-track-player";
import { Context as UserContext } from "../../../Context/userDataContext";
import { onPayment } from "../../../Api/onPayment";
const AudioBookScreen = (props) => {
  //=========================== CONTEXT  ====================
  const context = useContext(DataContext);
  const { state, musicPlayerAction, changeCurrentMusic, setCurrentMusicNull } = context;
  const { audioSection } = state;
  const { audioBooks } = audioSection;
  console.log({ Ali: state.audioSection });
  const [imageDownload, setImageDownload] = useState(false);
  const userContext = useContext(UserContext);
  const userState = userContext.state;
  //=========================== CONTEXT  ====================
  const [isPaid, setIsPaid] = useState(false);
  const [data, setData] = useState(audioBooks.sections);
  const [payloadin, setPayLoadin] = useState(false);
  console.log({ isPaid });
  const dataArray = [
    { title: "توضیحات دوره", content: audioBooks ? audioBooks.aboutAudioBook : "" },
  ];
  let IconDown = <Icon1 style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon1 style={Styles.Icon} name="chevron-left" color={"black"} />;
  useEffect(() => {
    console.log({ ALI: audioBooks });
    console.log({ MORI: userState.userDataState.audioBooks });

    if (audioBooks) {
      if (userState.userDataState.audioBooks.includes(audioBooks._id)) {
        setIsPaid(true);
      }
    }
  }, [audioBooks]);
  const onDownload = async (item) => {
    console.log({ item });

    if (isPaid) {
      const resPermission = await mkDir();
      if (resPermission) {
        console.log("hello");
        makeVoiceDirectory();
        setImageDownload(true);
        voiceDownload(item);
      }
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };
  useEffect(() => {
    checkIfDownLoaded();
  }, []);
  useEffect(() => {
    if (imageDownload) {
      makeImageDirectory();
    }
  }, [imageDownload]);
  const makeImageDirectory = () => {
    console.log("image");

    const { config, fs } = RNFetchBlob;
    const imageUrl = audioBooks.cover.toString();
    const imageDirectoryPath = `/AndisheRoshan/AudioBook/${audioBooks.title}`;
    const path = `/storage/emulated/0/${imageDirectoryPath}`;
    const imageFileDirectory = path + `/image.png`;
    RNFS.exists(imageFileDirectory)
      .then((exist) => {
        if (exist) {
          console.log("image exist");
        } else {
          let imageDownloadoption = {
            fileCache: false,
            addAndroidDownloads: {
              useDownloadManager: true,
              notification: false,
              path: imageFileDirectory,
              description: "Downloading",
            },
          };
          config(imageDownloadoption)
            .fetch("GET", imageUrl)
            .then((res) => {
              console.log(res);
              const textObject = {
                title: audioBooks.title,
                desc: audioBooks.description,
                about: audioBooks.aboutAudioBook,
              };
              AsyncStorage.setItem(`${audioBooks.title}`, JSON.stringify(textObject))
                .then((res) => {
                  console.log({ res });
                })
                .catch((e) => console.log({ e }));
            })
            .catch((e) => {
              console.log({ e });
            });
        }
      })
      .catch((e) => {
        console.log({ e });
      });
  };
  const voiceDownload = async (data) => {
    if (data) {
      const voiceDirectoryPath = `/AndisheRoshan/AudioBook/${audioBooks.title}/Voices`;
      const { config, fs } = RNFetchBlob;
      const url = data.voice.toString();
      const voiceDirectory = `/storage/emulated/0/${voiceDirectoryPath}`;
      const voiceName = `${data.title}.mp3`;

      RNFS.exists(voiceDirectory + `/${voiceName}`)
        .then((exist) => {
          if (exist) {
            ToastAndroid.show(IS_EXIST_TOAST, ToastAndroid.SHORT);
          } else {
            let options = {
              fileCache: true,
              addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                path: voiceDirectory + `/${voiceName}`,
                description: "Downloading",
              },
            };

            config(options)
              .fetch("GET", url)
              .then((res) => checkIfDownLoaded())
              .catch((r) => {});
          }
        })
        .catch((e) => {
          return Alert.alert("error in check in exist ");
        });
    }
  };
  const makeVoiceDirectory = () => {
    const voiceDirectoryPath = `/AndisheRoshan/AudioBook/${audioBooks.title}/Voices`;
    const path = `/storage/emulated/0/${voiceDirectoryPath}`;
    RNFS.exists(path)
      .then((exist) => {
        if (exist) {
        } else {
          RNFetchBlob.fs
            .mkdir(path)
            .then((res) => console.log({ res }))
            .catch((e) => console.log({ e }));
        }
      })
      .then((e) => console.log({ e }));
  };
  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };
  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text style={Styles.textCotent}>{item.content}</Text>
      </View>
    );
  };
  const checkPayment = () => {
    return isPaid ? null : (
      <TouchableOpacity TouchableOpacity={0.5} style={Styles.buttonBank} onPress={onPayments}>
        {payloadin ? (
          <Indicator size={20} color={GOLDEN_COLOR} />
        ) : (
          <View style={Styles.viewPrice}>
            <Text style={Styles.priceBank}>{audioBooks.price}</Text>
            <Text style={Styles.priceBank}> تومان</Text>
          </View>
        )}
        <Text style={Styles.priceBank}>پرداخت</Text>
      </TouchableOpacity>
    );
  };
  const checkIfDownLoaded = () => {
    const checkDownloaded = audioBooks.sections.map(async (item) => {
      const directoryPath = `/AndisheRoshan/AudioBook/${audioBooks.title}/Voices`;
      const Path = `/storage/emulated/0/${directoryPath}`;
      const itemName = `${item.title}.mp3`;

      return RNFS.exists(Path + `/${itemName}`).then((exist) => {
        let newItem = {};
        if (exist) {
          newItem = { ...item, downloaded: true };
        } else {
          Object.assign(newItem, item);
        }
        return newItem;
      });
    });

    Promise.all(checkDownloaded).then((res) => {
      setData(res);
    });
  };
  const onPressPlay = async (item) => {
    if (isPaid) {
      if (state.currentMusic === item) {
        return;
      } else {
        await TrackPlayer.reset();
        setCurrentMusicNull();
        changeCurrentMusic(item);
        musicPlayerAction(true);
      }
    } else {
      ToastAndroid.show(PLEASE_PAY, ToastAndroid.SHORT);
    }
  };
  console.log({ MMMM: data });

  const onPayments = async () => {
    setPayLoadin(true);
    const resPayment = await onPayment("AudioBook", audioBooks._id);
    console.log({ resPayment });
    if (typeof resPayment === "string") {
      Linking.canOpenURL(resPayment).then((supported) => {
        if (supported) {
          setPayLoadin(false);
          console.log({ supported });
          Linking.openURL(resPayment);
        } else {
          setPayLoadin(false);
          console.log("Don't know how to open URI: " + resPayment);
        }
      });
    }
  };

  const voiceList = () => {
    return (
      <FlatList
        style={Styles.flat}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(index) => index._id}
        data={data}
        renderItem={({ item }) => {
          return (
            <View style={Styles.container}>
              <View style={Styles.view}>
                <Icon
                  name="playcircleo"
                  color={MAIN_COLOR}
                  size={40}
                  style={Styles.playIcon}
                  onPress={() => onPressPlay(item)}
                />
                <View style={Styles.TextView}>
                  <Text style={Styles.Title} numberOfLines={1}>
                    {item.title}
                  </Text>
                  <Text style={[iranSans]} numberOfLines={1}>
                    {item.description}
                  </Text>
                </View>
              </View>
              <Icon
                name="download"
                color={item.downloaded ? "#eee" : MAIN_COLOR}
                size={25}
                style={Styles.downloadIcon}
                onPress={() => onDownload(item)}
              />
            </View>
          );
        }}
      />
    );
  };
  return (
    <View style={Styles.Container}>
      <ScrollView>
        <View style={Styles.TopView}>
          <Header navigation={props.navigation} title={audioBooks.title} />
          <Image resizeMode="stretch" style={Styles.img} source={{ uri: audioBooks.cover }} />
          {checkPayment()}
          <Accordion
            style={Styles.Accordion}
            headerStyle={Styles.Header}
            dataArray={dataArray}
            renderHeader={header}
            renderContent={renderContent}
          />
        </View>

        {voiceList()}
      </ScrollView>
      <View style={{ position: "absolute", bottom: 0 }}>
        <FloatinMusicPlayer type={"audiobook"} />
      </View>
    </View>
  );
};

export default AudioBookScreen;
