import React, { useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Styles from "./style";
import Header from "../../../Components/Header";
import { MAIN_COLOR } from "../../../common/Colors";
import { Context as DataContext } from "../../../Context/DataContext";
import { COURSE_TITLE } from "../../../common/Strings";
import { TOMAN } from "../../../common/Strings";
import { Context as UserContext } from "../../../Context/userDataContext";
const irAmount = require("iramount");

const ListScreen = ({ navigation }) => {
  //===========================CONTEXT =============================
  const context = useContext(DataContext);
  const userContext = useContext(UserContext);
  const { state } = context;
  const userState = userContext.state;
  //===========================CONTEXT =============================

  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} title={COURSE_TITLE} />
      <FlatList
        numColumns={2}
        keyExtractor={(CategoryList, index) => Math.random(index) * 1.62}
        data={state.allCorses}
        renderItem={({ item }) => {
          let isPaid = false;
          if (userState.userDataState.courses.includes(item._id)) {
            console.log("morijoooon");
            isPaid = true;
          }
          console.log({ ALLLIJ: item });

          return (
            <TouchableOpacity
              activeOpacity={0.9}
              style={Styles.Pack}
              onPress={() =>
                navigation.navigate("Pack", {
                  course: item
                })
              }
            >
              <Image resizeMode= 'stretch' style={Styles.img} source={{ uri: item.cover }} />
              <Icon
                name="bookmark"
                size={70}
                solid
                color={MAIN_COLOR}
                style={Styles.bookmarkImg}
              />
              <View style={Styles.PriseView}>
                <Text style={Styles.priseText} numberOfLines={1}>
                  {isPaid ? "خریداری" : new irAmount(parseInt(item.price)).digitGrouped()}
                </Text>
                <Text style={Styles.priseText}>{isPaid ? "شده" : TOMAN}</Text>
              </View>
              <View style={{ alignItems: "center" }}>
                <Text style={Styles.TextTitle}>{item.title}</Text>
                <Text style={Styles.TextDescription}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default ListScreen;
