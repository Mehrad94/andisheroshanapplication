import {StyleSheet, Dimensions} from 'react-native';
import {MAIN_COLOR} from '../../common/Colors';
import { posAbs } from '../../common/Theme';
const {height, width} = Dimensions.get('screen');

const Styles = StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#ebebeb',
  },
  videoPlayer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').width * (9 / 16),
    backgroundColor: 'black',
  },
  text: {
    marginTop: 30,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: 'justify',
  },
});
export default Styles;
