import React from 'react';
import {View,Text, ScrollView} from 'react-native';
import Styles from './styles';
import VideoPlayer from '../../Components/VideoPlayer';

const VideoScreen = () => {
   const Category = {
       title : 'ویدیو 1',
       description: 'توضیحات خیلی مهمی باید باشد ولی  نیست و بهمین شکل خواهد بود و ادامه دار می رود تا کجا نمی دانمی'
   }
   
    return (
        <View style={Styles.Container} >
            <VideoPlayer />
           
                <ScrollView>
                <Text>{Category.title}</Text>
    <Text>{Category.description}</Text>
                </ScrollView>
  
           
         
        </View>
    )


};



export default VideoScreen;