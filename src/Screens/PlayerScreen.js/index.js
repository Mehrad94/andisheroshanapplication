import React, { useEffect, useContext, useState, useRef } from "react";
import { StyleSheet, Text, View, Dimensions, Animated } from "react-native";
import TrackPlayer from "react-native-track-player";
import Player from "./PlayListScreen";
import { Context as DataContext } from "../../Context/DataContext";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import * as Animatable from "react-native-animatable";
import { postFinishSec } from "../../Api/postFinishSec";
export default function LandingScreen({ navigation, data, type }) {
  console.log({ GGGG: data });

  const playbackState = TrackPlayer.usePlaybackState();
  //========================== CONTEXT====================
  const context = useContext(DataContext);
  const { state, musicPlayerAction, setCurrentMusicNull } = context;
  //========================== CONTEXT====================
  const onFinishSec = async () => {
    if (data) {
      const idCourse = data._id;
      const idSection = state.currentMusic._id;
      const resFinish = await postFinishSec(type, idCourse, idSection);
      console.log({ resFinish });
    } else {
      const idCourse = null;
      const idSection = state.currentMusic._id;
      const resBookFinish = await postFinishSec(type, idCourse, idSection);
      console.log({ resBookFinish });
    }
  };
  const scrollY = new Animated.Value(200);
  useEffect(() => {
    stuff();
  }, []);
  useEffect(() => {
    play();
  }, [state.currentMusic]);

  const stuff = async () => {
    const currentTrack = await TrackPlayer.getCurrentTrack();

    if (!currentTrack) {
      await TrackPlayer.setupPlayer();

      TrackPlayer.updateOptions({
        stopWithApp: false,
        capabilities: [
          TrackPlayer.CAPABILITY_PLAY,
          TrackPlayer.CAPABILITY_PAUSE,
          TrackPlayer.CAPABILITY_STOP,
        ],
        compactCapabilities: [TrackPlayer.CAPABILITY_PLAY, TrackPlayer.CAPABILITY_PAUSE],
      });
    }
  };
  // const courceId = [
  //     { sectionId: sectionId, isDone: true },
  //     { sectionId: sectionId, isDone: true },
  //     { sectionId: sectionId, isDone: true },
  // ];
  async function play() {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack == null) {
      await TrackPlayer.reset();
      await TrackPlayer.add({
        id: "local-track",
        url: state.currentMusic.voice,
        title: state.currentMusic.title,
        artist: "اندیشه روشن",
        // artwork: "https://picsum.photos/200"
      });
      await TrackPlayer.play();
    } else {
      return;
    }
  }
  async function togglePlayback() {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack == null) {
      console.log("if");

      await TrackPlayer.reset();
      // await TrackPlayer.add(playlistData);
      await TrackPlayer.add({
        id: "local-track",
        url: state.currentMusic.voice,
        title: state.currentMusic.title,
        artist: "اندیشه روشن",

        // artwork: "https://picsum.photos/200"
      });
      await TrackPlayer.play();
    } else {
      console.log("elseCurent");

      if (playbackState === TrackPlayer.STATE_PAUSED) {
        await TrackPlayer.play();
      } else {
        await TrackPlayer.pause();
      }
    }
  }
  async function onStop() {
    await TrackPlayer.reset();
    if (state.musicPlayer) {
      musicPlayerAction(false);
      setCurrentMusicNull();
    }
  }
  // if (!state.musicPlayer) {
  //   return <View></View>;
  // }
  return (
    <View style={[styles.container]}>
      <Player
        onFinishSec={onFinishSec}
        onNext={skipToNext}
        style={styles.player}
        onPrevious={skipToPrevious}
        onTogglePlayback={togglePlayback}
        onStop={onStop}
      />
    </View>
  );
}

LandingScreen.navigationOptions = {
  title: "Playlist Example",
};

function getStateName(state) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return "None";
    case TrackPlayer.STATE_PLAYING:
      return "Playing";
    case TrackPlayer.STATE_PAUSED:
      return "Paused";
    case TrackPlayer.STATE_STOPPED:
      return "Stopped";
    case TrackPlayer.STATE_BUFFERING:
      return "Buffering";
  }
}

async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
 borderTopLeftRadius: 10,
 borderTopRightRadius: 10,
    alignItems: "center",
    backgroundColor: GOLDEN_COLOR,
  },

  player: {},
  state: {},
});

// const fadeIn = {
//   from: {
//     translateY: 200
//   },
//   to: {
//     translateY: 0
//   }
// };
// const fadeOut = {
//   from: {
//     translateY: 0
//   },
//   to: {
//     translateY: 200
//   }
// };
