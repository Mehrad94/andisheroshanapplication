import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import TrackPlayer from "react-native-track-player";
import { Slider, StyleSheet, Text, TouchableOpacity, View, ViewPropTypes } from "react-native";
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import {useTrackPlayerProgress} from 'react-native-track-player/lib/hooks';
import { iranSans, centerAll } from "../../common/Theme";
import Icon from "react-native-vector-icons/FontAwesome5";
import { MAIN_COLOR } from "../../common/Colors";
var moment = require("moment");
var momentDurationFormatSetup = require("moment-duration-format");

function ControlButton({ title, onPress }) {
  // let videoPlayer = null
  // const [playerState,setPlayerState] = useState(PLAYER_STATES.PLAYING)
  // const [currentTime,setCurrentTime] = useState(0)
  // const [duration,setDuration] = useState(0)
  // const onSeeking = currentTime => {setCurrentTime(currentTime)};
  // const onSeek = seek => {
  //   //Handler for change in seekbar
  //   videoPlayer.seek(seek);
  // };
  return (
    <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
      <Text style={styles.controlButtonText}>{title}</Text>
    
    </TouchableOpacity>
  );
}

ControlButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

const Player = (props) => {
  const onProgress = TrackPlayer.useTrackPlayerProgress();

  const [isFinishedSec, setIsFinishSec] = useState(false);
  const [visiable, setVisiable] = useState(false);
  const [sliderVal,setSliderVal]=useState(0);
  const [isSeeking, setIsSeeking] = useState(false);
  const [seek, setSeek] = useState(0);

  const progress = useTrackPlayerProgress();
  const {duration, position} = progress;
  useEffect(() => {
    if (isFinishedSec) {
      postDataFinishedSec();
    }
  }, [isFinishedSec]);

  useEffect(() => {
    mori();
    getSectionDone();
  });
  
  const postDataFinishedSec = () => {
    props.onFinishSec();
  };
  
  const getSectionDone = () => {
    if (onProgress.position > 22) {
      if (onProgress.position > onProgress.duration - 30) {
        setIsFinishSec(true);
      }
    }
  };
  async function mori() {
    const mm = await TrackPlayer.getPosition();
    if (mm >= 0) {
      setVisiable(true);
    } else {
      if (visiable) setVisiable(false);
    }
  }
  const onValChange=(val)=>{
console.log({val});
// setSliderVal(val)

// setSliderVal(val)\value
onSliderSeek(val)

  }
  
  function onSliderSeek(val){
    // setSliderVal(val)

  setTimeout(()=>{
  TrackPlayer.seekTo(val)
    setSliderVal(val)
    
  })

     }
  const ProgressBar = () => {
    if (visiable) {
      return (
        <Slider 
        // minimumValue          = {0}
        // maximumValue          = {onProgress.duration}
        // thumbTintColor        = "white"
        // minimumTrackTintColor = {MAIN_COLOR}
        // maximumTrackTintColor = "rgba(255,255,255,.8)"
        // step                  = {5}
        // // onValueChange ={ onValChange}
       
        // onSlidingComplete={ 
          
        //   onValChange
       
        // }
        // value={isSeeking ? seek : position}
        minimumValue={0}
        maximumValue={duration}
        minimumTrackTintColor="#FFFFFF"
        thumbTintColor = "white"
        minimumTrackTintColor = {MAIN_COLOR}
        
        onValueChange={(val) => {
          TrackPlayer.play();
          setIsSeeking(true);
          setSeek(val);
          console.log({val});
          TrackPlayer.seekTo(val);
          
        }}
        // onSlidingComplete={(val) => {
        //   console.log('aliiii');
          
        //   TrackPlayer.seekTo(val);
        //   TrackPlayer.play();
        // }}
        value={isSeeking ? seek : position}
        style={{width: '80%'}}
      />
         
      );
    } else {
      return <View></View>;
    }
  };
  const playbackState = TrackPlayer.usePlaybackState();

  const { style, onZeropposition, onPrevious, onTogglePlayback, onStop, stateName } = props;

  var middleButtonText = "Play";

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    middleButtonText = "Pause";
  }

  function onForward() {
    return TrackPlayer.seekTo(onProgress.position + 15);
  }
  function onBackWard() {
    return TrackPlayer.seekTo(onProgress.position - 15);
  }
  const durations = Math.round(onProgress.duration);
  const positions = Math.round(onProgress.position);

  momentDurationFormatSetup(moment);
  let positionII = moment.duration(positions, "seconds").format("mm:ss", { trim: false });
  let durationII = moment.duration(durations, "seconds").format("mm:ss", { trim: false });
  return (
    <View style={[styles.card, style]}>
      {visiable ? (
        <View style={styles.propgress}>
          <Text>{positionII}</Text>
          <Text>{durationII}</Text>
        </View>
      ) : null}
      <ProgressBar />
      {/* <MediaControls
          // duration={duration}
         
          mainColor="red"
         
         
          // onSeek={onSeek}
          // onSeeking={onSeeking}
          // playerState={playerState}
          // progress={currentTime}
        
          style={{height:100}}
        /> */}
      <View style={styles.controls}>
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="backward" size={25} color={MAIN_COLOR} onPress={onBackWard} />
        </TouchableOpacity>
        {playbackState === TrackPlayer.STATE_PLAYING ? (
          <TouchableOpacity style={{ width: 30, height: 30 }}>
            <Icon name="pause" size={25} color={MAIN_COLOR} onPress={onTogglePlayback} />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity style={{ width: 30, height: 30 }}>
            <Icon name="play" size={25} color={MAIN_COLOR} onPress={onTogglePlayback} />
          </TouchableOpacity>
        )}
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="stop" size={25} color={MAIN_COLOR} onPress={onStop} />
        </TouchableOpacity>
        <TouchableOpacity style={{ width: 30, height: 30 }}>
          <Icon name="forward" size={25} color={MAIN_COLOR} onPress={onForward} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default Player;
Player.propTypes = {
  style: ViewPropTypes.style,
  onNext: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
  onTogglePlayback: PropTypes.func.isRequired,
};

Player.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  card: {
    width: "100%",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  progress: {
    height: 3,
    width: "90%",
    marginTop: 8,
    marginBottom: 16,
    flexDirection: "row",
  },
  title: {
    ...iranSans,
    textAlign: "center",
  },
  artist: {
    fontWeight: "bold",
  },
  controls: {
    width: "100%",
    justifyContent: "space-around",
    flexDirection: "row",
  },
  controlButtonContainer: {
    // width: "100%",
    flex: 1,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
  },
  controlButtonText: {
    fontSize: 18,
    textAlign: "center",
  },
  propgress: {
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "90%",
  },
});
