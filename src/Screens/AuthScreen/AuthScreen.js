import React, { useContext, useEffect } from "react";
import { CODE_TITLE, LOGIN_TITLE, SIGN_TITLE } from "../../common/Strings";
import { Context as AuthContext } from "../../Context/AuthContext";
import { Context as DataContext } from "../../Context/DataContext";
import LoginScreen from "./LoginScreen";
import { Text } from "react-native";
import RNFS from "react-native-fs";
import RNFetchBlob from "rn-fetch-blob";
import { check, PERMISSIONS, RESULTS, request } from "react-native-permissions";

const AuthScreen = ({ navigation }) => {
  //=======================CONTEXT ====================
  const context = useContext(DataContext);
  const { fetchAllCourses, fetchSplashData, fetchAllBlogs } = context;
  //=======================CONTEXT ====================
  const { state } = useContext(AuthContext);
  const deleteAndishe = async () => {
    check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
      .then((result) => {
        switch (result) {
          case RESULTS.UNAVAILABLE:
            break;
          case RESULTS.DENIED:
            request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE).then((result) => {
              if (result === "granted") {
                deleteFunc();
              }
            });
            break;
          case RESULTS.GRANTED:
            deleteFunc();

            break;
          case RESULTS.BLOCKED:
            break;
        }
      })
      .catch((error) => {
        // …
      });
  };
  const deleteFunc = () => {
    const checkRoot = "/storage/emulated/0//AndisheRoshan";

    RNFS.exists(checkRoot)
      .then((exist) => {
        console.log({ exist });

        if (exist) {
          RNFS.unlink(checkRoot)
            .then((resDelet) => {
              console.log({ resDelet });
            })
            .catch((edelet) => {
              console.log({ edelet });
            });
        }
      })
      .catch((e) => {
        console.log({ e });
      });
  };
  const login = true;
  const enterToken = false;
  useEffect(() => {
    deleteAndishe();
  }, []);
  switch (state.loginType) {
    case "Login":
      return <LoginScreen navigation={navigation} H1={LOGIN_TITLE} type="Login" />;
    case "Code":
      return <LoginScreen navigation={navigation} H1={CODE_TITLE} type="Token" />;
    case "SignUp":
      return <LoginScreen navigation={navigation} H1={SIGN_TITLE} type="SignUp" />;
    default:
      return <Text>kljlkjl</Text>;
  }
};
AuthScreen.navigationOptions = () => {
  return {
    header: null,
  };
};
export default AuthScreen;
