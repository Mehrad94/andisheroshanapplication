import React, { useState } from "react";
import {
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  ToastAndroid,
  Dimensions
} from "react-native";
import {
  bgMainColor,
  centerAll,
  Flex,
  fWhite,
  h3,
  h4,
  iranSans,
  mH32,
  Tac,
  mV4,
  bgWhite,
  mH8,
  mV8,
  mV16,
  pad16,
  h5
} from "../../common/Theme";
import { MAIN_COLOR } from "../../common/Colors";
import { screenHeight, screenWidth } from "../../common/Constants";
import { SIGN_TITLE, NUMBER_IS_WRONG, I_AM_REGIRTER_LOGIN, INTER_TO_APP_TITLE } from "../../common/Strings";
import evvelope from "../../Assets/Png/envelope.png";
const entertoapptext = ;

const SignupScreen = ({ navigation }) => {
  const [rightLable, setRightLable] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const _changeMobileInput = (text) => {
    let newText = "";
    let numbers = "0123456789";
    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        if (text.charAt(0) != "0") return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);
        if (text.length > 1 && text.charAt(1) != "9")
          return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);

        newText = newText + text[i];
      } else return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);
    }
    setInputValue(newText);
  };
  return (
    <View style={[Flex, bgMainColor]}>
      <KeyboardAvoidingView style={[Flex]} behavior="position" enabled>
        <View style={[{ width: "100%", height: screenHeight / 2 }, bgMainColor, centerAll]}>
          <Image
            source={evvelope}
            style={[{ width: screenWidth / 2.7, height: screenWidth / 3 }]}
          />
          <Text style={[iranSans, h3, fWhite, Tac, mH32]}>{SIGN_TITLE}</Text>
        </View>
        <View
          style={[
            bgWhite,
            {
              // width: '100%',
              height: screenHeight / 2,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40
            },
            ,
            centerAll
          ]}
        >
          <TextInput
            placeholder={"09 - -  - - -  - - - -"}
            maxLength={11}
            value={inputValue}
            onChangeText={_changeMobileInput}
            keyboardType={"numeric"}
            textAlignVertical={"center"}
            onBlur={() => setRightLable(false)}
            onFocus={() => setRightLable(true)}
            style={[
              pad16,
              iranSans,
              {
                height: screenHeight / 12,
                width: screenWidth / 1.5,
                borderBottomWidth: 0,
                borderRadius: 20,
                direction: "rtl",
                textAlign: "center",

                backgroundColor: "rgba(64,199,133,0.2)"
              }
            ]}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate("MainFlow")}
            style={[
              centerAll,
              {
                borderRadius: 20,
                marginTop: 50,
                width: 200,
                height: 50,
                backgroundColor: MAIN_COLOR
              }
            ]}
          >
            <Text style={[iranSans, fWhite, h4]}>{INTER_TO_APP_TITLE}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[centerAll]}
            onPress={() => {
              navigation.navigate("SignIn");
            }}
          >
            <Text style={[iranSans, h5, { marginTop: 16 }]}>{I_AM_REGIRTER_LOGIN}</Text>

            <View
              style={{
                width: Dimensions.get("screen").width - 100,
                height: 1,
                backgroundColor: "red"
              }}
            />
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};
SignupScreen.navigationOptions = () => {
  return {
    header: null
  };
};
export default SignupScreen;
