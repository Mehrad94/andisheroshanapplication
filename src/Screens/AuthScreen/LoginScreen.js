import React, { useContext, useState } from "react";
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Text,
  TextInput,
  ToastAndroid,
  TouchableOpacity,
  View
} from "react-native";
import evvelope from "../../Assets/1.png";
import { MAIN_COLOR } from "../../common/Colors";
import { screenHeight, screenWidth } from "../../common/Constants";
import {
  bgMainColor,
  bgWhite,
  centerAll,
  Flex,
  fWhite,
  h3,
  h4,
  h5,
  iranSans,
  mH32,
  mV8,
  pad16,
  Tac
} from "../../common/Theme";
import { Context as AuthContext } from "../../Context/AuthContext";
import TokenScreen from "./TokenScreen/TokenScreen";
import { NUMBER_IS_WRONG } from "../../common/Strings";
const ChangeLoginToSignUpText = "ایجاد حساب کاربری";
const mobilenumberlable = "شماره همراه";
const entertoapptext = "ورود به برنامه ";
const ChangeSignUpToLogin = "حساب کاربری دارم ";
const LodinScreen = ({ navigation, H1, type }) => {
  const { state, signup, editNumber, clearErrorMessage, signin, changeTypeToSignUp } = useContext(
    AuthContext
  );
  const { loading, errorMessage } = state;
  const [rightLable, setRightLable] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [nameValue, setNameValue] = useState("");
  const _changeNameText = (text) => {
    setNameValue(text);
  };
  const Inputs = () => {
    switch (type) {
      case "SignUp":
        return (
          <View>
            <TextInput
              placeholder={"نام و نام خانوادگی"}
              maxLength={40}
              value={nameValue}
              onChangeText={_changeNameText}
              keyboardType={"default"}
              textAlignVertical={"center"}
              onBlur={() => setRightLable(false)}
              onFocus={() => setRightLable(true)}
              style={[
                pad16,
                iranSans,
                {
                  height: screenHeight / 12,
                  width: screenWidth / 1.5,
                  borderBottomWidth: 0,
                  borderRadius: 20,
                  direction: "rtl",
                  textAlign: "center",

                  backgroundColor: "rgba(64,199,133,0.2)"
                }
              ]}
            />
            <TextInput
              placeholder={"09 - -  - - -  - - - -"}
              maxLength={11}
              value={inputValue}
              onChangeText={_changeMobileInput}
              keyboardType={"numeric"}
              textAlignVertical={"center"}
              onBlur={() => setRightLable(false)}
              onFocus={() => setRightLable(true)}
              style={[
                pad16,
                iranSans,
                {
                  height: screenHeight / 12,
                  width: screenWidth / 1.5,
                  borderBottomWidth: 0,
                  borderRadius: 20,
                  direction: "rtl",
                  textAlign: "center",
                  marginTop: 10,
                  backgroundColor: "rgba(64,199,133,0.2)"
                }
              ]}
            />
            <TouchableOpacity
              onPress={() => signup(inputValue, nameValue)}
              style={[
                centerAll,
                {
                  alignSelf: "center",
                  borderRadius: 20,
                  marginTop: 50,
                  width: 200,
                  height: 50,
                  backgroundColor: MAIN_COLOR
                }
              ]}
            >
              {loading ? (
                <ActivityIndicator size={40} color="white" />
              ) : (
                <Text style={[iranSans, fWhite, h4]}>{entertoapptext}</Text>
              )}
            </TouchableOpacity>

            {errorMessage ? (
              <Text style={[iranSans, h4, { marginTop: 4, color: "red", textAlign: "center" }]}>
                {errorMessage}
              </Text>
            ) : null}
            <TouchableOpacity onPress={() => changeTypeToSignUp("Login")} style={{ marginTop: 4 }}>
              <Text style={[iranSans, Tac, { color: "blue", textDecorationLine: "underline" }]}>
                {ChangeSignUpToLogin}
              </Text>
            </TouchableOpacity>
          </View>
        );
      case "Token":
        return <TokenScreen mobileNumber={inputValue} navigation={navigation} />;
      case "Login":
        return (
          <>
            <TextInput
              placeholder={"09 - -  - - -  - - - -"}
              maxLength={11}
              value={inputValue}
              onChangeText={_changeMobileInput}
              keyboardType={"numeric"}
              textAlignVertical={"center"}
              onBlur={() => setRightLable(false)}
              onFocus={() => setRightLable(true)}
              style={[
                pad16,
                iranSans,
                {
                  height: screenHeight / 12,
                  width: screenWidth / 1.5,
                  borderBottomWidth: 0,
                  borderRadius: 20,
                  direction: "rtl",
                  textAlign: "center",
                  marginTop: 10,
                  backgroundColor: "rgba(64,199,133,0.2)"
                }
              ]}
            />
            <TouchableOpacity
              onPress={() => signin(inputValue)}
              style={[
                centerAll,
                {
                  alignSelf: "center",
                  borderRadius: 20,
                  marginTop: 50,
                  width: 200,
                  height: 50,
                  backgroundColor: MAIN_COLOR
                }
              ]}
            >
              {loading ? (
                <ActivityIndicator size={40} color="white" />
              ) : (
                <Text style={[iranSans, fWhite, h4]}>{entertoapptext}</Text>
              )}
            </TouchableOpacity>
            {errorMessage ? (
              <Text style={[iranSans, h4, { marginTop: 4, color: "red", textAlign: "center" }]}>
                {errorMessage}
              </Text>
            ) : null}
            <TouchableOpacity onPress={() => changeTypeToSignUp("SignUp")} style={{ marginTop: 8 }}>
              <Text style={[iranSans, Tac, { color: "blue", textDecorationLine: "underline" }]}>
                {ChangeLoginToSignUpText}
              </Text>
            </TouchableOpacity>
          </>
        );
      default:
        return null;
    }
  };
  const _changeMobileInput = (text) => {
    let newText = "";
    let numbers = "0123456789";
    for (let i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        if (text.charAt(0) != "0") return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);
        if (text.length > 1 && text.charAt(1) != "9")
          return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);

        newText = newText + text[i];
      } else return ToastAndroid.show(NUMBER_IS_WRONG, ToastAndroid.SHORT);
    }
    setInputValue(newText);
  };

  return (
    <View style={[Flex, bgMainColor]}>
      <KeyboardAvoidingView style={[Flex]} behavior="position" enabled>
        <View style={[{ width: "100%", height: screenHeight / 2 }, bgMainColor, centerAll]}>
          <Image
            source={evvelope}
            style={[{ width: screenWidth / 2.7, height: screenWidth / 1.7 }]}
          />
          <Text style={[iranSans, h3, fWhite, Tac, mH32]}>{H1}</Text>
          {type === "Token" ? <Text style={[iranSans, mV8, fWhite]}>{inputValue}</Text> : null}
          {type === "Token" ? (
            <TouchableOpacity
              onPress={() => {
                editNumber(), clearErrorMessage();
              }}
            >
              <Text style={[iranSans, h5, fWhite, { textDecorationLine: "underline" }]}>
                ویرایش شماره
              </Text>
            </TouchableOpacity>
          ) : null}
        </View>
        <View
          style={[
            bgWhite,
            centerAll,
            {
              width: "100%",
              height: screenHeight / 2,
              borderTopLeftRadius: 40,
              borderTopRightRadius: 40
            }
          ]}
        >
          {Inputs()}
        </View>
      </KeyboardAvoidingView>
    </View>
  );
};
LodinScreen.navigationOptions = () => {
  return {
    header: null
  };
};
export default LodinScreen;
