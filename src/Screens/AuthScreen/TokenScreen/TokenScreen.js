import React, { useEffect, useState, useContext } from "react";
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator } from "react-native";
import { Context as AuthContext } from "../../../Context/AuthContext";
import { iranSans, h4, mV8, centerAll, fWhite, Flex, pad16, h5 } from "../../../common/Theme";
import { MAIN_COLOR, GOLDEN_COLOR } from "../../../common/Colors";
import { screenWidth, screenHeight } from "../../../common/Constants";
import AsyncStorage from "@react-native-community/async-storage";
import { Context as DataContext } from "../../../Context/DataContext";
import Axios from "../../../Api/AxiosConfigue";
import CountDown from "react-native-countdown-component";
const TokenScreen = (props) => {
  //=======================CONTEXT ====================
  const context = useContext(DataContext);
  const {
    fetchAllCourses,
    fetchSplashData,
    fetchAllBlogs,
    fetchAudioBooks,
    fetchSeminars,
    fetchIntroducingJobs
  } = context;
  //=======================CONTEXT ====================
  const [codeInputValue, setCodeInputValue] = useState("");
  const [retrySendMobile, setRetrySendMobile] = useState(false);
  const [time, setTimer] = useState("");

  const { state, signin, enterToken, clearErrorMessage } = useContext(AuthContext);
  const { loading, errorMessage } = state;
  console.log({ ff: state.loginType });

  useEffect(() => {
    // TimerContDown();
  }, []);
  const getTokenFromStorage = async () => {
    let token = await AsyncStorage.getItem("token");
    if (token) {
      fetchAllBlogs();
      fetchAllCourses();
      fetchAudioBooks();
      fetchSeminars();
      fetchIntroducingJobs();
      await Axios.get("/splashscreen")
        .then((res) => {
          fetchSplashData(res.data);
          if (res.data) {
            props.navigation.navigate("Drawer");
          }
        })
        .catch((e) => {
          if (e.response.data.CODE === 1098) {
            props.navigation.navigate("LoginFlow");
          }
        });
    } else {
      console.log("LOGON");
    }
  };
  const resendCode = () => {
    return (
      <TouchableOpacity
        disabled={!retrySendMobile}
        onPress={() => {
          signin(props.mobileNumber);
          setRetrySendMobile((retrySendMobile) => !retrySendMobile);
        }}
      >
        {retrySendMobile ? (
          <Text style={[iranSans, h5, { color: MAIN_COLOR }]}>ارسال مجدد کد </Text>
        ) : (
          <CountDown
            size={10}
            until={90}
            onFinish={() => setRetrySendMobile(true)}
            digitStyle={{ backgroundColor: "#FFF", borderWidth: 1.5, borderColor: MAIN_COLOR }}
            digitTxtStyle={{ color: GOLDEN_COLOR }}
            // timeLabelStyle={{ color: "red", fontWeight: "bold" }}
            separatorStyle={{ color: GOLDEN_COLOR }}
            timeToShow={["M", "S"]}
            timeLabels={{ m: null, s: null }}
            showSeparator
          />
        )}
      </TouchableOpacity>
    );
  };
  const onChangeCodeInput = (text) => {
    setCodeInputValue(text);
  };
  return (
    <View style={[centerAll, Flex]}>
      <TextInput
        placeholder={"------"}
        maxLength={6}
        value={codeInputValue}
        onChangeText={onChangeCodeInput}
        keyboardType={"numeric"}
        textAlignVertical={"center"}
        style={[
          pad16,
          iranSans,
          {
            height: screenHeight / 12,
            width: screenWidth / 1.5,
            borderBottomWidth: 0,
            borderRadius: 20,
            direction: "rtl",
            textAlign: "center",

            backgroundColor: "rgba(64,199,133,0.2)"
          }
        ]}
      />
      <TouchableOpacity
        onPress={() => enterToken(codeInputValue, props.mobileNumber, () => getTokenFromStorage())}
        style={[
          centerAll,
          {
            alignSelf: "center",
            borderRadius: 20,
            marginTop: 50,
            width: 200,
            height: 50,
            backgroundColor: MAIN_COLOR
          }
        ]}
      >
        {loading ? (
          <ActivityIndicator size={40} color="white" />
        ) : (
          <Text style={[iranSans, fWhite, h4]}>{"تایید کد ارسالی"}</Text>
        )}
      </TouchableOpacity>
      {errorMessage ? (
        <Text style={[iranSans, h4, mV8, { color: "red" }]}>{errorMessage}</Text>
      ) : null}
      <View style={[centerAll, mV8, { alignSelf: "center" }]}>{resendCode()}</View>
    </View>
  );
};
export default TokenScreen;
// const TimerContDown = async () => {
//   let date = new Date();
//   let time = date.getTime() + 122000;

//   let countDownDate = new Date(time).getTime();

//   let x = setInterval(async () => {
//     let now = new Date().getTime();

//     let distance = countDownDate - now;

//     let days = Math.floor(distance / (1000 * 60 * 60 * 24));
//     let hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
//     let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
//     let seconds = Math.floor((distance % (1000 * 60)) / 1000);

//     setTimer("  ارسال مجدد کد تا  " + minutes + ":" + seconds);

//     if (distance < 1000) {
//       console.log("tamam");

//       clearInterval(x);
//       setRetrySendMobile(true);
//     }
//   }, 1000);
// };
