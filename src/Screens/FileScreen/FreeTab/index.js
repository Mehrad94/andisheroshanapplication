import React, { useState, useContext } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import Styles from "./styles";
import { navigate } from "../../../../navigationRef";
import LandingScreen from "../../PlayerScreen.js";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
import { Flex, centerAll } from "../../../common/Theme";

const FreeTab = ({ data }) => {
  const onPress = (item) => {
    if (item.type === "VIDEO") {
      navigate("OfflineVideoPlayer", { video: item.viddeo });
    }
    if (item.type === "VOICE") {
      navigate("OffLineMusicPlayer", { item: item });
    }
  };
  if (data.length < 1) {
    return (
      <View style={[{ height: 200 }]}>
        <RenderEmptyList />
      </View>
    );
  }
  return (
    <View>
      <FlatList
        showsHorizontalScrollIndicator={false}
        keyExtractor={(CategoryList) => CategoryList.title}
        data={data}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity style={Styles.Container} onPress={() => onPress(item)}>
              <Image source={{ uri: item.image }} style={Styles.Img} />
              <View style={Styles.ViewText}>
                <Text style={Styles.TextTitle} numberOfLines={1}>
                  {item.title}
                </Text>
                <Text style={Styles.Text} numberOfLines={1}>
                  {item.desc}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default FreeTab;
