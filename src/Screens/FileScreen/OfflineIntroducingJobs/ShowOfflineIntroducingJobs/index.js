import React, { useState, useEffect } from "react";
import { View, Text, FlatList, TouchableOpacity, Image, ToastAndroid, ScrollView } from "react-native";
import Styles from "./styles";
import { MAIN_COLOR } from "../../../../common/Colors";
import { Indicator } from "../../../../Components/Indicator";
import VideoTab from "../../../ShowOfflineCourses/VideoOffline";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icon1 from 'react-native-vector-icons/AntDesign'
import Header from "../../../../Components/Header";
import { navigate } from "../../../../../navigationRef";
import { Accordion } from "native-base";
import {COURSE_DESC, LEFT, RIGHT } from "../../../../common/Strings";

const ShowOfflineIntroducingJobs = (props) => {
  const [underView, setUnderView] = useState(<VideoTab data={null} />);
  const [active, setActive] = useState(LEFT);
  const [parameter, setParameter] = useState(null);
  let Data = props.navigation.state.params.param;
  const onTabPressed = (direction) => {
    setActive(direction);
    const video = parameter.videoMap;
    setUnderView(<VideoTab data={video} />);
  };
  console.log({ parameter });

  const dataArray = [{ title: COURSE_DESC, content: parameter ? parameter.about : "" }];
  //=================================DOWNLOAD =============================
  useEffect(() => {
    let params = null;
    if (props.navigation.state.params.course) {
      params = props.navigation.state.params.course;
    }

    setParameter(params);
    if (parameter) {
      return onTabPressed(RIGHT);
    }
  }, [props, parameter]);

  let IconDown = <Icon style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon style={Styles.Icon} name="chevron-left" color={"black"} />;

  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };

  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text>{item.content}</Text>
      </View>
    );
  };

  if (!parameter) {
    return <Indicator />;
  }

  const onPressPlay = (item) => {
    console.log({ dataPlay: item });

    navigate("OfflineVideoPlayer", { video: item });
  };

  const videoList = () => {
    return (
      <FlatList
        
        showsHorizontalScrollIndicator={false}
        keyExtractor={(index) => index._id}
        data={parameter.videoMap}
        renderItem={({ item }) => {
          const str = item.substring(item.lastIndexOf("/") + 1);
        var ret = str.replace(".mp4", "");
          return (
            <View style={Styles.container}>
              <View style={Styles.view}>
                <Icon1
                  name="playcircleo"
                  color={MAIN_COLOR}
                  size={40}
                  style={Styles.playIcon}
                  onPress={() => onPressPlay(item)}
                />
                <View style={Styles.TextView}>
                  <Text style={Styles.Title} numberOfLines={1}>{ret}</Text>
                  
                </View>
              </View>
           
            </View>
          );
        }}
      />
    );
  };

  return (
<View style={Styles.Container}>
  <ScrollView>
      <View style={Styles.TopView}>
        <Header navigation={props.navigation} title={parameter.title} />
        <Image resizeMode= 'stretch' style={Styles.img} source={{ uri: parameter.image }} />
        <Accordion
          style={Styles.Accordion}
          headerStyle={Styles.Header}
          dataArray={dataArray}
          renderHeader={header}
          renderContent={renderContent}
        />
      </View>
      {videoList()}
      </ScrollView>
    </View>
  );
};

export default ShowOfflineIntroducingJobs;
