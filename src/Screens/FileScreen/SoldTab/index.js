import React, { useState, useEffect } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import Styles from "./styles";
import { navigate } from "../../../../navigationRef";
import RenderEmptyList from "../../../Components/RenderEmptyLists";

const SoldTab = ({ data }) => {
  if (data.length < 1) {
    return (
      <View style={[{ height: 200 }]}>
        <RenderEmptyList />
      </View>
    );
  }
  return (
    <View style={Styles.box}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        keyExtractor={(CategoryList) => CategoryList.title}
        data={data}
        renderItem={({ item }) => {
          console.log({ item });

          return (
            <TouchableOpacity
              style={Styles.Container}
              onPress={() => navigate("ShowOfflineCourses", { course: item })}
            >
              <Image source={{ uri: item.image }} style={Styles.Img} />
              <View style={Styles.ViewText}>
                <Text style={Styles.TextTitle} numberOfLines={1}>
                  {item.title}
                </Text>
                <Text style={Styles.Text} numberOfLines={1}>
                  {item.desc}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default SoldTab;
