import { StyleSheet } from "react-native";
import { mH16, mV8, posAbs, iranSans, mV16, padV64, r8, h4, h6 } from "../../../common/Theme";
import { MAIN_COLOR } from "../../../common/Colors";
const Styles = StyleSheet.create({
  Container: {
    margin: 10,
    flexDirection: "row-reverse",
    borderWidth: 2,
    borderRadius: 5,
    borderColor: MAIN_COLOR
  },
  TextTitle: {
    ...iranSans,
    ...h4,
    marginBottom: 5
  },

  Img: {
    width: 80,
    height: 80
  },
  ViewText: {
    marginRight: 5,
    justifyContent: "center"
  },
  Text: {
    ...iranSans,
    ...h6
  }
});
export default Styles;
