import React, { useState, useEffect } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import Styles from "./styles";
import { navigate } from "../../../../navigationRef";
import Header from "../../../Components/Header";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
import RNFetchBlob from "rn-fetch-blob";
import RNFS from "react-native-fs";
import AsyncStorage from "@react-native-community/async-storage";

const OfflineAudioBook = (navigation) => {
  const [seprateData, setSeprateData] = useState({ data: [] });
  const [corsesArray, setCoursesArray] = useState([]);
  const [courseSerate, setCourseSeprate] = useState([]);
  const title = "کتاب های صوتی";
  useEffect(() => {
    //=============================CHECKCOURSE=====================
    const courses = "/storage/emulated/0//AndisheRoshan/AudioBook";
    RNFetchBlob.fs
      .ls(courses)
      .then((course) => {
        console.log({ course });
        setCoursesArray(course);
      })
      .catch((error) => console.log(error));
    //=============================checkFree==
  }, []);
  useEffect(() => {
    if (corsesArray.length > 0) {
      seprateCourses();
    }
  }, [corsesArray]);
  console.log({ courseSerate });

  const seprateCourses = async () => {
    let corsesSeprate = corsesArray.map(async (item) => {
      const eachCourse = `/storage/emulated/0//AndisheRoshan/AudioBook/${item}`;
      return RNFS.exists(eachCourse).then(async (exist) => {
        if (exist) {
          return RNFetchBlob.fs.ls(eachCourse).then(async (res) => {
            console.log({ res });

            let courseObject = {};

            const voiceCoursesinArray = eachCourse + `/${res[0]}`;
            const image = `file://${eachCourse}/${res[1]}`;
            Object.assign(courseObject, { image });
            await AsyncStorage.getItem(`${item}`)
              .then((resStorage) => {
                const resposnse = JSON.parse(resStorage);
                Object.assign(courseObject, resposnse);
              })
              .catch((errorStorage) => {
                console.log({ errorStorage });
              });

            RNFetchBlob.fs
              .ls(voiceCoursesinArray)
              .then(async (voiceRes) => {
                const voiceMap = voiceRes.map((item) => {
                  return `file://${eachCourse}/${res[0]}/${item}`;
                });
                Object.assign(courseObject, { voiceMap });
              })
              .catch((errrorVideo) => {
                console.log({ errrorVideo });
              });
            console.log({ courseObject });
            return courseObject;
          });
        } else {
          console.log("not wxist");

          return null;
        }
      });
    });
    Promise.all(corsesSeprate).then((resProimis) => {
      return setCourseSeprate(resProimis);
    });
  };
  if (courseSerate.length < 1) {
    return (
      <View style={[{ height: 200 }]}>
        <Header navigation={navigation} title={title} />
        <RenderEmptyList />
        
      </View>
    );
  }
  return (
    <View style={Styles.box}>
      <Header navigation={navigation} title={title} />
      <FlatList
        showsHorizontalScrollIndicator={false}
        keyExtractor={(CategoryList) => CategoryList.title}
        data={courseSerate}
        renderItem={({ item }) => {
          console.log({ item });

          return (
            <TouchableOpacity
              style={Styles.Container}
              onPress={() => navigate("ShowofflineAudioBooks", { course: item })}
            >
              <Image source={{ uri: item.image }} resizeMode= 'stretch' style={Styles.Img} />
              <View style={Styles.ViewText}>
                <Text style={Styles.TextTitle} numberOfLines={1}>
                  {item.title}
                </Text>
                <Text style={Styles.Text} numberOfLines={1}>
                  {item.desc}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default OfflineAudioBook;
