import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, ScrollView, Image, ToastAndroid } from "react-native";
import Styles from "./styles";
import { MAIN_COLOR } from "../../../../common/Colors";
import { Indicator } from "../../../../Components/Indicator";
import Header from "../../../../Components/Header";
import VoiseTab from "../../../ShowOfflineCourses/VoiceOffline";
import Icon from "react-native-vector-icons/FontAwesome5";
import Icon1 from "react-native-vector-icons/AntDesign";
import { Accordion } from "native-base";
import FloatinMusicPlayer from "../../../../Components/FloatinMusicPlaye";
import { Context as DataContext } from "../../../../Context/DataContext";
import TrackPlayer from "react-native-track-player";
import { COURSE_DESC, LEFT, RIGHT } from "../../../../common/Strings";

const ShowofflineAudioBooks = (props) => {
  const context = useContext(DataContext);
  const { state, musicPlayerAction, changeCurrentMusic, setCurrentMusicNull } = context;

  const [underView, setUnderView] = useState(<VoiseTab data={null} />);
  const [active, setActive] = useState(LEFT);
  const [parameter, setParameter] = useState(null);

  const onTabPressed = (direction) => {
    setActive(direction);
    const voices = parameter.voiceMap;
    setUnderView(<VoiseTab data={voices} />);
  };
  console.log({ parameter });

  const dataArray = [{ title: COURSE_DESC, content: parameter ? parameter.about : "" }];
  //=================================DOWNLOAD =============================
  useEffect(() => {
    let params = null;
    if (props.navigation.state.params.course) {
      params = props.navigation.state.params.course;
    }

    setParameter(params);
    if (parameter) {
      return onTabPressed(RIGHT);
    }
  }, [props, parameter]);

  let IconDown = <Icon style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon style={Styles.Icon} name="chevron-left" color={"black"} />;

  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };

  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text>{item.content}</Text>
      </View>
    );
  };

  if (!parameter) {
    return <Indicator />;
  }

  const onPressPlay = async (item) => {
    if (state.currentMusic === item) {
      return;
    } else {
      const voice = item;
      await TrackPlayer.reset();
      setCurrentMusicNull();
      changeCurrentMusic({ voice });
      musicPlayerAction(true);
    }
  };

  const voiceList = () => {
    return (
      <FlatList
        showsHorizontalScrollIndicator={false}
        keyExtractor={(index) => index._id}
        data={parameter.voiceMap}
        renderItem={({ item }) => {
          const str = item.substring(item.lastIndexOf("/") + 1);
          var ret = str.replace(".mp3", "");
          return (
            <View style={Styles.container}>
              <View style={Styles.view}>
                <Icon1
                  name="playcircleo"
                  color={MAIN_COLOR}
                  size={40}
                  style={Styles.playIcon}
                  onPress={() => onPressPlay(item)}
                />
                <View style={Styles.TextView}>
                  <Text style={Styles.Title} numberOfLines={1}>
                    {ret}
                  </Text>
                </View>
              </View>
            </View>
          );
        }}
      />
    );
  };

  return (
    <View style={Styles.Container}>
      <ScrollView>
        <View style={Styles.TopView}>
          <Header navigation={props.navigation} title={parameter.title} />
          <Image style={Styles.img} resizeMode="stretch" source={{ uri: parameter.image }} />

          <Accordion
            style={Styles.Accordion}
            headerStyle={Styles.Header}
            dataArray={dataArray}
            renderHeader={header}
            renderContent={renderContent}
          />
        </View>

        {voiceList()}
      </ScrollView>
      <View style={{ position: "absolute", bottom: 0 }}>
        <FloatinMusicPlayer />
      </View>
    </View>
  );
};

export default ShowofflineAudioBooks;
