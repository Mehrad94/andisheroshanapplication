import { StyleSheet, Dimensions } from "react-native";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../../../common/Colors";
import { fRow, pad8, bgWhite, r8, h2, iranSans, h1, posAbs } from "../../../../common/Theme";

const { height, width } = Dimensions.get("screen");
const Styles = StyleSheet.create({
  TopView: {},
  Container: {
    flex: 1,
    height: "100%"
  },
  img: {
    width: "100%",
    height: height / 4,

    alignSelf: "center"
  },
  TabsHolder: {
    flexDirection: "row",
    marginHorizontal: 5,
    borderRadius: 5,
    backgroundColor: "#e8e8e8"
  },
  TabLeft: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 16,
    width: "50%",
    borderTopStartRadius: 5,
    borderBottomStartRadius: 5
  },
  TabRight: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
    alignItems: "center",
    width: "50%",
    borderTopEndRadius: 5,
    borderBottomEndRadius: 5
  },
  TabsText: {
    fontSize: 16,
    fontFamily: "iran_sans",
    color: GOLDEN_COLOR
  },
  Accordion: {
    backgroundColor: "transparent",
    marginVertical: 5,
    ...iranSans
  },
  Content: {
    marginHorizontal: 4,
    padding: 4,
    backgroundColor: "white"
  },
  Header: {
    ...fRow,
    ...pad8,
    justifyContent: "flex-end",
    alignItems: "center",

    ...bgWhite,
    ...r8,
    margin: 5
  },
  HeaderText: {
    ...h2,
    ...iranSans
  },
  Icon: {
    ...h1,
    ...posAbs,
    left: 10
  },
  textCotent: {
    ...iranSans
  },
  buttonBank:{
    backgroundColor:MAIN_COLOR,
    alignItems:'center',
    borderRadius:20,
    width:width/2,
    alignSelf:'center',
    marginTop:10,
   
  },
  priceBank:{
    ...iranSans,
    color:GOLDEN_COLOR,
  fontSize:18
  },
  viewPrice:{
    flexDirection: 'row-reverse'
  },
  container: {
    backgroundColor: "white",
    flexDirection: "row-reverse",
    borderBottomWidth: 2,
    marginHorizontal: 5,
    borderColor: MAIN_COLOR,
    borderRadius: 5,
    justifyContent: "space-between"
  },
  TextView: {
    justifyContent: "center",
    marginRight: 10
  },
  Title: {
    fontFamily: "iran_sans",
    fontSize: 18,
    marginVertical:10
  },
  playIcon: {
    // justifyContent:'center',
    marginRight: 10,
    alignSelf: "center"
  },
  downloadIcon: {
    alignSelf: "center",
    marginLeft: 5
  },
  view: {
    flexDirection: "row-reverse"
  },
  flat: {
    marginBottom: width / 10
  }
});
export default Styles;
