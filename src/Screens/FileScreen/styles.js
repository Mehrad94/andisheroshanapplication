import { StyleSheet, Dimensions } from "react-native";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../common/Colors";
import { iranSans } from "../../common/Theme";
const { height, width } = Dimensions.get("screen");
const Styles = StyleSheet.create({
  Container: {
    flex: 1
  },
  TabsHolder: {
   
    height: width /3.5,
    backgroundColor: MAIN_COLOR,
    margin: 10,
    justifyContent: "center",
    alignItems: "center",
    borderRadius:10
  },
  title:{
    ...iranSans,
    color:GOLDEN_COLOR,
    fontSize:20
  }
  
});
export default Styles;
