import React, { useState, useContext, useEffect } from "react";
import { View, Text, FlatList, Image, TouchableOpacity } from "react-native";
import Styles from "./styles";
import { navigate } from "../../../../navigationRef";
import Header from "../../../Components/Header";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
import RNFetchBlob from "rn-fetch-blob";
import RNFS from "react-native-fs";
import AsyncStorage from "@react-native-community/async-storage";
const FreeBlog = ({ navigation }) => {
  const [videoArray, setVideoArray] = useState([]);
  const [voiceArray, setVoiceArray] = useState([]);
  const [VIDEO, setVIDEO] = useState([]);
  const [VOICE, setVoice] = useState([]);
  const [seprateData, setSeprateData] = useState({ data: [] });
  const [emptyList, setEmptyList] = useState(false);
  const title = "فایل های رایگان";
  useEffect(() => {
    setSeprateData({ data: [...VIDEO, ...VOICE] });
  }, [VOICE, VIDEO]);
  useEffect(() => {
    if (videoArray.length > 0) {
      videoCutomizing();
    }
  }, [videoArray]);
  useEffect(() => {
    if (voiceArray.length > 0) {
      voiceCustomizing();
    }
  }, [voiceArray]);
  const videoCutomizing = async () => {
    let videoSeprate = videoArray.map(async (item) => {
      const VIDEOS = `/storage/emulated/0//AndisheRoshan/Free/Videos/${item}`;

      return RNFetchBlob.fs
        .ls(VIDEOS)
        .then(async (video) => {
          let newObject = {};
          const viddeo = `file://${VIDEOS}/${video[0]}`;
          const image = `file://${VIDEOS}/${video[1]}`;
          newObject = { viddeo, image, type: "VIDEO" };
          await AsyncStorage.getItem(`${video[0]}`)
            .then((res) => {
              const resposnse = JSON.parse(res);
              Object.assign(newObject, resposnse);
            })
            .catch((e) => console.log({ e }));
          return newObject;
        })
        .catch((error) => console.log(error));
    });
    Promise.all(videoSeprate).then((res) => {
      setVIDEO(res);
    });
  };
  const voiceCustomizing = async () => {
    let voiceSeprate = voiceArray.map(async (item) => {
      const VOICES = `/storage/emulated/0//AndisheRoshan/Free/Voices/${item}`;

      return RNFetchBlob.fs
        .ls(VOICES)
        .then(async (voice) => {
          let voiceObject = {};
          const voicce = `file://${VOICES}/${voice[0]}`;
          const image = `file://${VOICES}/${voice[1]}`;
          voiceObject = { voicce, image, type: "VOICE" };
          await AsyncStorage.getItem(`${voice[0]}`)
            .then((res) => {
              const resposnse = JSON.parse(res);
              Object.assign(voiceObject, resposnse);
            })
            .catch((e) => console.log({ e }));
          return voiceObject;
        })
        .catch((error) => console.log(error));
    });
    Promise.all(voiceSeprate).then((res) => {
      if (res.length === 0) {
        setEmptyList(true);
      }
      setVoice(res);
    });
  };
  useEffect(() => {
    const checkRoot = "/storage/emulated/0//AndisheRoshan";

    RNFS.exists(checkRoot).then((exist) => {
      if (exist) {
        //=============================checkFree=====================
        const VIDEOS = "/storage/emulated/0//AndisheRoshan/Free/Videos";
        const VOICES = "/storage/emulated/0//AndisheRoshan/Free/Voices";
        RNFetchBlob.fs
          .ls(VIDEOS)
          .then((video) => {
            setVideoArray(video);
          })
          .catch((error) => console.log(error));
        RNFetchBlob.fs
          .ls(VOICES)
          .then((voice) => {
            setVoiceArray(voice);
          })
          .catch((error) => console.log(error));
        //=============================checkFree=====================
      } else {
        ToastAndroid.show(NOTHING_TO_SHOW_TOAST, ToastAndroid.SHORT);
        setError(true);
      }
    });
  }, []);
  const onPress = (item) => {
    if (item.type === "VIDEO") {
      navigate("OfflineVideoPlayer", { video: item.viddeo });
    }
    if (item.type === "VOICE") {
      navigate("OffLineMusicPlayer", { item: item });
    }
  };
  if (seprateData.data.length < 1) {
    return (
      <View style={[{ height: 200 }]}>
        <Header navigation={navigation} title={title} />
        <RenderEmptyList />
      </View>
    );
  }
  return (
    <View style={{ flex: 1 }}>
      <Header navigation={navigation} title={title} />
      <FlatList
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item) => item.title}
        data={seprateData.data}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity style={Styles.Container} onPress={() => onPress(item)}>
              <Image source={{ uri: item.image }} resizeMode="stretch" style={Styles.Img} />
              <View style={Styles.ViewText}>
                <Text style={Styles.TextTitle} numberOfLines={1}>
                  {item.title}
                </Text>
                <Text style={Styles.Text} numberOfLines={1}>
                  {item.desc}
                </Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default FreeBlog;
