import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, TouchableOpacity, Image, ToastAndroid } from "react-native";
import Styles from "./styles";
import Header from "../../Components/Header";
import { navigate } from "../../../navigationRef";
const FilesScreen = ({ navigation }) => {
  const Data = [
    { title: "فایل های رایگان", route: "freeOflfineBlog" },
    { title: "دوره های آموزشی", route: "OfflineCourses" },
    { title: "کتاب های صوتی", route: "OfflineAudioBooks" },
    { title: "سمینار ها", route: "OfflineSemminars" },
    { title: "معرفی مشاغل", route: "OfflineIntroducingJobs" },
  ];
  const items = () => {
    return (
      <FlatList
        data={Data}
        keyExtractor={(item) => item.route}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity onPress={() => navigate(item.route)} style={Styles.TabsHolder}>
              <Text style={Styles.title}>{item.title}</Text>
            </TouchableOpacity>
          );
        }}
      />
    );
  };
  const title = "فایل ها";
  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} title={title} />
      {items()}
    </View>
  );
};

export default FilesScreen;
