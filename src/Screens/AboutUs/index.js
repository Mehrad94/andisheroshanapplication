import React, {useState, useEffect} from 'react';
import {View, Text, Image, TouchableOpacity, ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Styles from './styles';
import {GOLDEN_COLOR, MAIN_COLOR} from '../../common/Colors';
import Header from '../../Components/Header';

const AboutUs = ({navigation}) => {
  const Category = 'مجموعه آموزشی اندیشه روشن در آبان ماه سال ۱۳۹۸ با هدف ارتقای دانش و مهارت های کسب و کار، فعالیت خود را در بستر فضای مجازی و شبکه اینترنت آغاز نمود و رسالت خود را ارتقای هر چه بیشتر دانش عمومی برای صاحبان کسب و کار و کمک به کارآفرینی جوانان جویای کار قرار داده است.'
  const title = ' درباره ما'
  const titleText = 'مجموعه آموزشی مجازی اندیشه روشن'
  const endText = 'امید است روزی هر ایرانی صاحب کسب و کار شخصی خود باشد.'
  return (
    <View style={Styles.Container}>
       <Header navigation={navigation} title={title}/>
      <View style={Styles.TopBack}></View>
      <ScrollView style={Styles.ViewCenter}>
      <Text style={Styles.Text}>{titleText}</Text>
  <Text style={Styles.TextInput}>{Category}</Text>
  <Text style={Styles.Text}>{endText}</Text>
      </ScrollView>
    </View>
  );
};

export default AboutUs;
