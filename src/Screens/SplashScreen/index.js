import React, { useEffect, useContext, useState } from "react";
import { View, Image } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import Axios from "../../Api/AxiosConfigue";
import { Context as DataContext } from "../../Context/DataContext";
import splashScreenImage from "../../Assets/1.png";
import { MAIN_COLOR } from "../../common/Colors";
import { centerAll } from "../../common/Theme";
import NetInfo from "@react-native-community/netinfo";
import VersionNumber from "react-native-version-number";
const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    navigation.addListener("didBlur", (path) => {
      if (path.type === "didBlur") {
      }
    });
    navigation.addListener("didFocus", (path) => {
      if (path.type === "willFocus") {
        if (isConnected === true) {
          setAppVersion(VersionNumber.appVersion);
          getTokenFromStorage();
          fetchAllCourses();
        }
        if (isConnected === false) {
          navigation.navigate("Files", { offline: "true" });
        }
      }
    });
  }, [navigation]);
  //=======================CONTEXT ====================
  const context = useContext(DataContext);
  const {
    fetchAudioBooks,
    fetchAllCourses,
    fetchSplashData,
    fetchAllBlogs,
    setAppVersion,
    fetchSeminars,
    fetchIntroducingJobs,
    checkConection,
  } = context;
  //=======================CONTEXT ====================
  const [isConnected, setConnected] = useState(null);
  const checkConnectivity = (type) => {
    NetInfo.addEventListener((state) => {
      setConnected(state.isConnected);
    });
  };
  const getTokenFromStorage = async () => {
    let token = await AsyncStorage.getItem("token");
    const showIntro = await AsyncStorage.getItem("intro");
    if (token) {
      fetchAllBlogs();
      fetchAllCourses();
      fetchAudioBooks();
      fetchSeminars();
      fetchIntroducingJobs();
      await Axios.get("/splashscreen")
        .then((res) => {
          fetchSplashData(res.data);
          if (res.data) {
            if (!showIntro) {
              navigation.navigate("Intro", { param: "home" });
            } else {
              navigation.navigate("Drawer");
            }
          }
        })
        .catch((e) => {
          console.log({ e });
          if (e.response.data.CODE === 1098) {
            if (!showIntro) {
              navigation.navigate("Intro", { param: "login" });
            } else {
              navigation.navigate("LoginFlow");
            }
          }
        });
    } else {
      if (!showIntro) {
        navigation.navigate("Intro", { param: "login" });
      } else {
        navigation.navigate("LoginFlow");
      }
    }
  };
  console.log({ isConnected });
  useEffect(() => {
    checkConnectivity();
  });
  useEffect(() => {
    checkConection(isConnected);

    if (isConnected === true) {
      setAppVersion(VersionNumber.appVersion);
      getTokenFromStorage();
    }
    if (isConnected === false) {
      navigation.navigate("Files", { offline: "true" });
    }
  }, [isConnected]);
  return (
    <View
      style={[
        centerAll,
        {
          flex: 1,
          backgroundColor: MAIN_COLOR,
        },
      ]}
    >
      <Image source={splashScreenImage} style={{ width: 300, height: 300 }} />
    </View>
  );
};

export default SplashScreen;
