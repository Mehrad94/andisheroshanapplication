import { StyleSheet } from "react-native";
import { MAIN_COLOR } from "../../../common/Colors";

const Styles = StyleSheet.create({
  Container: {
    backgroundColor: "white",
    flexDirection: "row-reverse",
    borderBottomWidth: 2,
    marginHorizontal: 5,
    borderColor: MAIN_COLOR,
    borderRadius: 5,
    justifyContent: "space-between"
  },
  TextView: {
    justifyContent: "center",
    marginRight: 10
  },
  Title: {
    fontFamily: "iran_sans",
    fontSize: 18,
    marginVertical:10
  },
  playIcon: {
    // justifyContent:'center',
    marginRight: 10,
    alignSelf: "center"
  },
  downloadIcon: {
    alignSelf: "center",
    marginLeft: 5
  },
  view: {
    flexDirection: "row-reverse"
  }
});
export default Styles;
