import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, Image } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { posAbs, mH16, mV8, iranSans, Flex, centerAll, bgMainColor } from "../../../common/Theme";
import Styles from "./styles";
import { MAIN_COLOR } from "../../../common/Colors";
import { navigate } from "../../../../navigationRef";
import { Context as DataContext } from "../../../Context/DataContext";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
const VideoOffline = ({ data }) => {
  //=========================CONTEXT =================
  console.log({ data });
  //=========================CONTEXT =================

  const onPressPlay = (item) => {
    console.log({ dataPlay: item });

    navigate("OfflineVideoPlayer", { video: item });
  };
  if (data) {
    if (data.length < 1) {
      return <RenderEmptyList />;
    }
  }
  return (
    <FlatList
      showsHorizontalScrollIndicator={false}
      keyExtractor={(index) => index._id}
      data={data}
      renderItem={({ item }) => {
        const str = item.substring(item.lastIndexOf("/") + 1);
        var ret = str.replace(".mp4", "");

        return (
          <View style={Styles.Container}>
            <View style={Styles.view}>
              <Icon
                name="play-circle"
                color={MAIN_COLOR}
                size={40}
                style={Styles.playIcon}
                onPress={() => onPressPlay(item)}
              />
              <View style={Styles.TextView}>
                <Text style={Styles.Title}>{ret}</Text>
              </View>
            </View>
          </View>
        );
      }}
    />
  );
};

export default VideoOffline;
