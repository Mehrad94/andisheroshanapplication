import React, { useState, useEffect } from "react";
import { View, Text, FlatList, TouchableOpacity, Image, ToastAndroid, ScrollView } from "react-native";
import Styles from "./styles";
import { MAIN_COLOR } from "../../common/Colors";
import { Indicator } from "../../Components/Indicator";
import VideoTab from "./VideoOffline";
import VoiseTab from "./VoiceOffline";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Accordion } from "native-base";
import { TAB_VIDEO, TAB_VOICE, COURSE_DESC, LEFT, RIGHT } from "../../common/Strings";
import FloatinMusicPlayer from "../../Components/FloatinMusicPlaye";
import { screenWidth } from "../../common/Constants";

const ShowOfflineCourses = (props) => {
  const [underView, setUnderView] = useState(<VideoTab data={null} />);
  const [active, setActive] = useState(LEFT);
  const [parameter, setParameter] = useState(null);

  const onTabPressed = (direction) => {
    if (direction === LEFT) {
      setActive(direction);
      const videos = parameter.videosMap;

      setUnderView(<VideoTab data={videos} />);
    }
    if (direction === RIGHT) {
      setActive(direction);
      const voices = parameter.voiceMap;
      setUnderView(<VoiseTab data={voices} />);
    }
  };
  console.log({ parameter });

  const dataArray = [{ title: COURSE_DESC, content: parameter ? parameter.about : "" }];
  //=================================DOWNLOAD =============================
  useEffect(() => {
    let params = null;
    if (props.navigation.state.params.course) {
      params = props.navigation.state.params.course;
    }

    setParameter(params);
    if (parameter) {
      return onTabPressed(LEFT);
    }
  }, [props, parameter]);

  let IconDown = <Icon style={Styles.Icon} name="chevron-down" color={"black"} />;
  let IconLeft = <Icon style={Styles.Icon} name="chevron-left" color={"black"} />;

  const header = (item, expanded) => {
    return (
      <View style={Styles.Header}>
        <Text style={Styles.HeaderText}> {item.title}</Text>
        {expanded ? IconDown : IconLeft}
      </View>
    );
  };

  const renderContent = (item) => {
    return (
      <View style={Styles.Content}>
        <Text>{item.content}</Text>
      </View>
    );
  };

  if (!parameter) {
    return <Indicator />;
  }

  return (
    <View style={Styles.Container}>
      <ScrollView>
      <View style={Styles.TopView}>
        <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: parameter.image }} />
        <Accordion
          style={Styles.Accordion}
          headerStyle={Styles.Header}
          dataArray={dataArray}
          renderHeader={header}
          renderContent={renderContent}
        />
      </View>
      <View style={Styles.TabsHolder}>
        <TouchableOpacity
          style={[Styles.TabLeft, active === LEFT ? { backgroundColor: MAIN_COLOR } : null]}
          onPress={() => onTabPressed(LEFT)}
        >
          <Text style={Styles.TabsText}>{TAB_VIDEO}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onTabPressed(RIGHT)}
          style={[Styles.TabRight, active === RIGHT ? { backgroundColor: MAIN_COLOR } : null]}
        >
          <Text style={Styles.TabsText}>{TAB_VOICE}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ flex: 1 }}>{underView}</View>
      </ScrollView>
      <View
        style={{
          position: "absolute",
          bottom: 0,
          width: screenWidth,
        }}
      >
        <FloatinMusicPlayer />
      </View>
    </View>
  );
};

export default ShowOfflineCourses;
