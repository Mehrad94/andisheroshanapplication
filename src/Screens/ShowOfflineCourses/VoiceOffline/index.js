import React, { useState, useEffect, useContext } from "react";
import { View, Text, FlatList, Image, ScrollView } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import Styles from "./style";
import { MAIN_COLOR } from "../../../common/Colors";
import TrackPlayer from "react-native-track-player";
import { Context as DataContext } from "../../../Context/DataContext";
import RenderEmptyList from "../../../Components/RenderEmptyLists";
const VoiceOffline = ({ data }) => {
  console.log({ data });

  console.log("VOICETAB");
  //========================== CONTEXT====================
  const context = useContext(DataContext);
  const { state, musicPlayerAction, changeCurrentMusic,setCurrentMusicNull } = context;
  //========================== CONTEXT====================

  const onVoiceIconPress = async (item) => {
    if (state.currentMusic === item) {
      return;
    } else {
      const voice = item;
      await TrackPlayer.reset();
      setCurrentMusicNull();
      changeCurrentMusic({ voice });
      musicPlayerAction(true);
    }};
  
  if (data) {
    if (data.length < 1) {
      return <RenderEmptyList />;
    }
  }
  return (
    <FlatList
     
      showsHorizontalScrollIndicator={false}
      keyExtractor={(index) => index._id}
      data={data}
      renderItem={({ item }) => {
        const str = item.substring(item.lastIndexOf("/") + 1);
        var ret = str.replace(".mp3", "");
        return (
          <View style={Styles.Container}>
            <View style={Styles.view}>
              <Icon
                name="play-circle"
                color={MAIN_COLOR}
                size={40}
                style={Styles.playIcon}
                onPress={() => onVoiceIconPress(item, item)}
              />
              <View style={Styles.TextView}>
                <Text style={Styles.Title}>{ret}</Text>
            
              </View>
            </View>
          </View>
        );
      }}
    />
  );
};

export default VoiceOffline;
