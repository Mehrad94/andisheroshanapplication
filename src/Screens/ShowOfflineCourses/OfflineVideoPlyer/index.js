import React, { useState, useEffect, useContext } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  ScrollView,
  BackHandler,
} from "react-native";
import Video, { OnLoadData } from "react-native-video";
import Orientation from "react-native-orientation-locker";

import PlayerControls from "../../../Components/VideoPlayers/Controls/PlayerControls";
import ProgressBar from "../../../Components/VideoPlayers/Controls/ProgressBar";
import Icon from "react-native-vector-icons/AntDesign";
import styles from "./style";
import { Indicator } from "../../../Components/Indicator";
const OfflineVideoSection = ({ navigation }) => {
  //==============================CONTEXT =========================

  //==============================CONTEXT =========================
  const videoRef = React.createRef();

  const [fullScreen, setFullScreen] = useState(false);
  const [play, setPlay] = useState(true);
  const [currentTime, setCurrentTime] = useState(0);
  const [duration, setDuration] = useState(0);
  const [showControls, setShowControls] = useState(true);
  const [parameter, setParameter] = useState(null);
  const [liked, setLiked] = useState(false);
  useEffect(() => {
    const params = navigation.state.params.video;
    setParameter(params);
  }, [navigation]);
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backButtonHandler);

    return () => {
      BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
    };
  }, [fullScreen]);
  console.log({ parameter });

  const backButtonHandler = () => {
    if (fullScreen) {
      return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);

    return () => {
      Orientation.removeOrientationListener(handleOrientation);
    };
  }, []);

  const handleOrientation = (orientation) => {
    orientation === "LANDSCAPE-LEFT" || orientation === "LANDSCAPE-RIGHT"
      ? (setFullScreen(true),
        StatusBar.setHidden(true),
        setTimeout(() => setShowControls(false), 500))
      : (setFullScreen(false),
        StatusBar.setHidden(false),
        setTimeout(() => setShowControls(false), 500));
  };

  const handleFullscreen = () => {
    fullScreen ? Orientation.unlockAllOrientations() : Orientation.lockToLandscapeLeft();
  };

  const handlePlayPause = () => {
    // If playing, pause and show controls immediately.
    if (play) {
      setPlay(false);
      setShowControls(true);
      return;
    }

    setPlay(true);
    setTimeout(() => setShowControls(false), 500);
  };

  const skipBackward = () => {
    videoRef.current.seek(currentTime - 15);
    setCurrentTime(currentTime - 15);
  };

  const skipForward = () => {
    videoRef.current.seek(currentTime + 15);
    setCurrentTime(currentTime + 15);
  };

  const onSeek = (data) => {
    videoRef.current.seek(data.seekTime);
    setCurrentTime(data.seekTime);
  };

  const onLoadEnd = (data) => {
    console.log("LOADDDDDDDDDDDD");

    setDuration(data.duration);
    setCurrentTime(data.currentTime);
  };

  const onProgress = (data) => {
    console.log("jhhjhjk");

    setCurrentTime(data.currentTime);
  };
  const onEnd = () => {
    setPlay(false);
    videoRef.current.seek(0);
  };

  const showControlsF = () => {
    setShowControls(!showControls);
  };
  const onLikePress = () => {
    likeMechanism(parameter._id);
  };
  if (!parameter) {
    return <Indicator />;
  }
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={showControlsF}>
        <View>
          <Video
            ref={videoRef}
            source={{
              uri: parameter,
            }}
            style={fullScreen ? styles.fullscreenVideo : styles.video}
            controls={false}
            resizeMode={"contain"}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            onEnd={onEnd}
            paused={!play}
          />
          {showControls && (
            <View style={styles.controlOverlay}>
              <TouchableOpacity
                onPress={handleFullscreen}
                hitSlop={{ top: 10, bottom: 10, left: 10, right: 10 }}
                style={styles.fullscreenButton}
              >
                {fullScreen ? (
                  <Icon name="shrink" size={25} color="red" />
                ) : (
                  <Icon name="arrowsalt" color="red" size={25} />
                )}
              </TouchableOpacity>
              <PlayerControls
                onPlay={handlePlayPause}
                onPause={handlePlayPause}
                playing={play}
                showPreviousAndNext={false}
                showSkip={true}
                skipBackwards={skipBackward}
                skipForwards={skipForward}
              />
              <ProgressBar
                currentTime={currentTime}
                duration={duration > 0 ? duration : 0}
                onSlideStart={handlePlayPause}
                onSlideComplete={handlePlayPause}
                onSlideCapture={onSeek}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
      <ScrollView style={styles.scroll}>
        <View style={styles.likeHolder}>
          <Icon
            name="heart"
            color="red"
            size={40}
            solid={liked}
            style={{ marginRight: 30 }}
            onPress={onLikePress}
          />
          <View>
            <Text style={styles.title}>{parameter.title}</Text>
            <Text style={styles.date}>{parameter.createdTime}</Text>
          </View>
        </View>
        <Text style={styles.text}>{parameter.aboutBlog}</Text>
      </ScrollView>
    </View>
  );
};

export default OfflineVideoSection;
