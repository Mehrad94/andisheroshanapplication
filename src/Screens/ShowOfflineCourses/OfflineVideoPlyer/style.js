import { StyleSheet, Dimensions } from "react-native";
import { GOLDEN_COLOR, MAIN_COLOR } from "../../../common/Colors";
import { iranSans } from "../../../common/Theme";
import { screenHeight } from "../../../common/Constants";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e8e8e8"
  },
  video: {
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width
  },
  fullscreenVideo: {
    height: Dimensions.get("window").width,
    width: Dimensions.get("window").height,
    backgroundColor: "#e8e8e8"
  },
  text: {
    marginTop: 10,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: "right",
    ...iranSans
  },
  fullscreenButton: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "flex-end",
    alignItems: "center",
    paddingRight: 10
  },
  controlOverlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#00000040",
    justifyContent: "space-between"
    // alignItems: "center"
  },
  title: {
    marginHorizontal: 20,
    textAlign: "right",
    ...iranSans,
    fontSize: 20,
    color: GOLDEN_COLOR,
    borderRadius: 5
  },
  scroll: {
    margin: 10,
    borderRadius: 5,
    backgroundColor: "white",
    elevation: 2
  },
  likeHolder: {
    backgroundColor: MAIN_COLOR,
    flexDirection: "row",
    height: screenHeight / 13,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  date: {
    color: GOLDEN_COLOR,
    textAlign: "right",
    marginRight: 16
  }
});

export default styles;
