import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity, StyleSheet, Image } from "react-native";
import Styles from "./style";
import Header from "../../Components/Header";
import { useContext } from "react";
import { Context as DataContext } from "../../Context/DataContext";
import { getBookmark } from "../../Api/getBookMark";
import AsyncStorage from "@react-native-community/async-storage";
import { Indicator } from "../../Components/Indicator";
import RenderEmptyList from "../../Components/RenderEmptyLists";
import { Flex, centerAll } from "../../common/Theme";

const Bookmarks = ({ navigation }) => {
  //=================== CONTEXT ======================
  const context = useContext(DataContext);
  const { state } = context;
  const { bookState } = state;
  //=================== CONTEXT ======================
  const [bookArray, setBookArray] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    bookMarkManager();
  }, [state]);
  const bookMarkManager = () => {
    const bookLoop = bookState.map(async (item) => {
      const resIsExist = await checkInAsyncStorage(item);
      if (!resIsExist) {
        const bookRes = await getBookmark(item);
        if (typeof bookRes === "object") {
          let object = { ...bookRes[0] };
          AsyncStorage.setItem(`${item}`, JSON.stringify(object));
        }
      }
      const finalRes = await checkInAsyncStorage(item);
      return JSON.parse(finalRes);
    });
    Promise.all(bookLoop).then((res) => {
      setBookArray(res);
      setLoading(false);
    });
  };
  const checkInAsyncStorage = async (item) => {
    const check = await AsyncStorage.getItem(item);
    return check;
  };
  //=================== CONTEXT ======================
  const title = "علاقه مندی ها";
  if (loading) {
    return <Indicator />;
  }
  if (bookArray.length < 1) {
    return (
      <View style={[Flex, centerAll]}>
        <Header navigation={navigation} title={title} />

        <RenderEmptyList />
      </View>
    );
  }
  return (
    <View style={Styles.Container}>
      <Header navigation={navigation} title={title} />
      <FlatList
        initialNumToRender={4}
        numColumns={2}
        keyExtractor={(CategoryList) => CategoryList.list}
        data={bookArray}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={Styles.Pack}
              onPress={() => navigation.navigate("Video", { blog: item })}
            >
              <Image style={Styles.img} resizeMode= 'stretch' source={{ uri: item.cover }} />
              <View style={{ alignItems: "center" }}>
                <Text style={Styles.TextTitle}>{item.title}</Text>
                <Text style={Styles.TextDescription}>{item.description}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default Bookmarks;
// useEffect(() => {
//   navigation.addListener("didBlur", (path) => {
//     if (path.type === "didBlur") {
//     }
//   });
//   navigation.addListener("didFocus", (path) => {
//     if (path.type === "didFocus") {
//       console.log({ mori: state });

//       setLoading(true);
//       bookMarkManager();
//     }
//   });
// }, [navigation]);
