//====================== PROFILE STRING ==================
export const AVATAR_SELECT = "انتخاب آواتار";
export const FROM_CAMERA = "عکس از دوربین";
export const FROM_STORAGE = "انتخاب از گالری";
export const NAME_FAMILY = "نام و نام خانوادگی";
export const EMAIL = "ایمیل";
export const MOBILE_NUMBER = "شماره تلفن همراه";
export const ADDRESS = "آدرس";
export const JOB_TITLE = "شغل";
export const EXIT = "خروج";
export const YOUR_NAME = "نام شما";
export const YOUR_EMAIL = "ایمیل شما";
export const YOUR_NUMBER = "شماره همراه شما";
export const YOUR_ADDRESS = "آدرس شما";
export const YOUR_JOB = "شغل شما";
//===================== PERMISSIONS ===============================
export const DENIED = "denied";
export const UNDETERMINARED = "undetermined";
export const AUTORIZED = "authorized";
export const GRANTED = "granted";
//=================== COMMON STRINGS ===============================
export const TAB_VOICE = "صوتی";
export const TAB_VIDEO = "تصویری";
export const COURSE_DESC = "توضیحات دوره";
export const RIGHT = "right";
export const LEFT = "left";
export const COURSE_TITLE = "دوره های آموزشی";
export const AUDIOBOOK_TITLE = "کتاب های صوتی";
export const JOBS_TITLE = "معرفی مشاغل";
export const SEMINAR_TITLE = "سمینارها";
export const TOMAN = "تومان";
export const ARTIST = "اندیشه روشن";
export const WELLCOME_DEAR = "عزیز خوش آمدید";
export const HOME = "خانه";
export const BOOKMARKS = "علاقه مندی ها";
export const ABOUT_US = "درباره ما";
export const CONTACT_US = "تماس با ما";
export const VERSION = "version : ";
export const NOTHING_TO_SHOW = "چیزی برای نمایش وجود ندارد";
export const DRAWER_SCREEN_ANDISHE = "اندیشه روشن";
export const TRY_AGAIN = "تلاش مجدد";
//=============================== TOASTS ==========================
export const IS_EXIST_TOAST = "این بخش قبلا دانلود شده ";
export const NUMBER_IS_WRONG = "شماره همراه اشتباه است";
export const NOTHING_TO_SHOW_TOAST = "چیزی برای نمایش وجود ندارد";
export const PROPBLEM_IN_UPLOAD = "مشکلی در آپلود مجدد تلاش کنید";
export const PLEASE_PAY = "دوره خریداری نشده است";
//==================================== LOGIN ==================
export const LOGIN_TITLE = "شماره خود را برای ورود به برنامه وارد کنید";
export const SIGN_TITLE = "شماره خود را برای ثبت نام در برنامه وارد کنید";
export const CODE_TITLE = "کد ۶ رقمی ارسال شده را وارد کنید ";
export const I_AM_REGIRTER_LOGIN = "قبلا ثبت نام کرده ام , ورود";
export const INTER_TO_APP_TITLE = "ورود به برنامه ";

//==================================== Seriveces ==================
