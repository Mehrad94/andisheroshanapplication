export const PINK_HOT = '#F4474F';
export const PINK_PASTEL = '#E3A6AE';
export const CYAN = '#50C3C8';
export const ORANGE_CARROT = '#FFB200';

export const MAIN_COLOR = '#0B0B61';
export const GOLDEN_COLOR = '#d4af37';
export const DEACTIVE_COLOR = '#BDBDBD';
export const ROUGE = '#f87e83';
export const ORANGE_LAUGHING = '#ffc94d';
export const PINK_BRIGHT = '#e3b2ba';
export const CYAN_LIGHT = '#7ec5c8';
