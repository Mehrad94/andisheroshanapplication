import React from "react";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import { createDrawerNavigator } from "react-navigation-drawer";
import { Provider as AuthProvider } from "./src/Context/AuthContext";
import { createBottomTabNavigator } from "react-navigation-tabs";
// import RNRestart from "react-native-restart";
import { I18nManager } from "react-native";

import HomeScreen from "./src/Screens/HomeScreen";
import MotionScreen from "./src/Screens/MotionScreen";
import ProfileScreen from "./src/Screens/ProfileScreen";
import DrawerScreen from "./src/Components/DrawerScreen/DrawerScreen";
import VideoPlayers from "./src/Components/VideoPlayers";
import ListScreen from "./src/Screens/MotionScreen/ListScreen";
import Icon from "react-native-vector-icons/AntDesign";
import FilesScreen from "./src/Screens/FileScreen";
import PackScreen from "./src/Screens/MotionScreen/PackScreen/PackScreen";
import CategoryScreen from "./src/Screens/HomeScreen/CategoryScreen";
import EditProfile from "./src/Screens/ProfileScreen/EditProfile";
import { MAIN_COLOR, GOLDEN_COLOR } from "./src/common/Colors";
import AboutUs from "./src/Screens/AboutUs";
import ContactUs from "./src/Screens/ContactUs";
import SplashScreen from "./src/Screens/SplashScreen";
import AuthScreen from "./src/Screens/AuthScreen/AuthScreen";
import LodinScreen from "./src/Screens/AuthScreen/LoginScreen";
import unSoldPack from "./src/Screens/MotionScreen/unSoldPack";
import { setNavigator } from "./navigationRef";
import { Provider as DataProvider } from "./src/Context/DataContext";
import { Provider as UserDataProvider } from "./src/Context/userDataContext";
import VideoSection from "./src/Screens/MotionScreen/VideoSection";
import LandingScreen from "./src/Screens/PlayerScreen.js";
import Bookmarks from "./src/Screens/BookMarks";
import ShowOfflineCourses from "./src/Screens/ShowOfflineCourses";
import OfflineVideoPlayer from "./src/Screens/ShowOfflineCourses/OfflineVideoPlyer";
import OffLineMusicPlayer from "./src/Components/OfflineMusicPlayer";
import IntroSlider from "./src/Components/IntroSlider";
import AudioBookList from "./src/Screens/MotionScreen/AudioBookScreen/AudioBooksList";
import AudioPack from "./src/Screens/MotionScreen/AudioBookScreen/index";
import SeminarList from "./src/Screens/MotionScreen/SeminarScreen/SeminarList";
import SeminarPack from "./src/Screens/MotionScreen/SeminarScreen/index";
import IntroducingJobsList from "./src/Screens/MotionScreen/IntroducingJobsScreen/IntroducingJobsList";
import IntroducingJobsPack from "./src/Screens/MotionScreen/IntroducingJobsScreen/index";
import freeOflfineBlog from "./src/Screens/FileScreen/freeBlog";
import OfflineCourses from "./src/Screens/FileScreen/offlineCourses";
import OfflineAudioBooks from "./src/Screens/FileScreen/offlineAudioBook";
import ShowofflineAudioBooks from "./src/Screens/FileScreen/offlineAudioBook/showOfflineAudio";
import OfflineSemminars from "./src/Screens/FileScreen/OfflineSeminars";
import ShowofflineSeminars from "./src/Screens/FileScreen/OfflineSeminars/ShowOfflineSeminars";
import OfflineIntroducingJobs from "./src/Screens/FileScreen/OfflineIntroducingJobs";
import ShowOfflineIntroducingJobs from "./src/Screens/FileScreen/OfflineIntroducingJobs/ShowOfflineIntroducingJobs";
I18nManager.allowRTL(false);
const StackHome = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Category: { screen: CategoryScreen },
  },
  {
    headerMode: "none",
  }
);
const StackMotoin = createStackNavigator(
  {
    Home: { screen: MotionScreen },
    Lists: { screen: ListScreen },
    Pack: { screen: PackScreen },
    unSoldPack,
    AudioBookList,
    AudioPack,
    SeminarPack,
    SeminarList,
    IntroducingJobsList,
    IntroducingJobsPack,
  },

  {
    headerMode: "none",
  }
);
const OfflineStack = createStackNavigator(
  {
    Files: FilesScreen,
    ShowOfflineCourses,
    OffLineMusicPlayer,
    freeOflfineBlog,
    OfflineCourses,
    OfflineAudioBooks,
    ShowofflineAudioBooks,
    OfflineSemminars,
    ShowofflineSeminars,
    OfflineIntroducingJobs,
    ShowOfflineIntroducingJobs,
  },
  {
    headerMode: "none",
  }
);
const StackProfile = createStackNavigator(
  {
    Profile: { screen: ProfileScreen },
    Edit: { screen: EditProfile },
  },
  {
    headerMode: "none",
  }
);

const Tabs = createBottomTabNavigator(
  {
    Profile: {
      screen: StackProfile,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="user" color={tintColor} size={25} />,
      },
    },

    Files: {
      screen: OfflineStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="download" color={tintColor} size={25} />,
      },
    },
    Motion: {
      screen: StackMotoin,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="appstore-o" color={tintColor} size={25} />,
      },
    },

    Home: {
      screen: StackHome,

      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="home" size={25} color={tintColor} />,
      },
    },
  },

  {
    tabBarOptions: {
      showLabel: false,
      inactiveBackgroundColor: MAIN_COLOR,
      activeTintColor: GOLDEN_COLOR,
      inactiveTintColor: "#aaa",
      style: {
        backgroundColor: MAIN_COLOR,
      },
    },
    initialRouteName: "Home",
  }
);
const DrawerStack = createStackNavigator(
  {
    DrawerFlow: createDrawerNavigator(
      {
        Home: {
          screen: Tabs,
        },
        AboutUs: AboutUs,
        ContactUs: ContactUs,
        Video: VideoPlayers,
        SecVideo: VideoSection,
        Voice: LandingScreen,
        Bookmarks: Bookmarks,
        OfflineVideoPlayer,
      },
      {
        drawerPosition: "right",
        contentComponent: DrawerScreen,
        drawerType: "slide",
      }
    ),
  },
  { headerMode: "none", initialRouteName: "DrawerFlow" }
);
const SplashStack = createStackNavigator(
  {
    Splash: { screen: SplashScreen },
    Intro: { screen: IntroSlider },
  },
  {
    headerMode: "none",
  }
);
const SwitchNavigator = createSwitchNavigator({
  SplashStack,
  LoginFlow: createStackNavigator({
    Athentication: AuthScreen,
    SignIn: LodinScreen,
  }),
  Files: OfflineStack,
  Drawer: DrawerStack,
});

const App = createAppContainer(SwitchNavigator);

export default () => {
  return (
    <AuthProvider>
      <DataProvider>
        <UserDataProvider>
          <App ref={(navigator) => setNavigator(navigator)} />
        </UserDataProvider>
      </DataProvider>
    </AuthProvider>
  );
};
